<?php
	require("../config/config.default.php");
	require("../config/config.function.php");
	require("../config/functions.crud.php");
	require("../config/dis.php");
	
	error_reporting(0);
	$id_mapel = $_GET['m'];	//==> kode mapel di bank soal
	$id_kelas = $_GET['k'];	//==> kode id_kelas
	$pengawas = fetch($koneksi, 'pengawas',array('id_pengawas'=>$id_pengawas));
	
	echo "
	<link rel='stylesheet' href='$homeurl/dist/bootstrap/css/bootstrap.min.css'/>
	<link rel='stylesheet' href='$homeurl/dist/css/cetak.min.css'>";
	$sqg=mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT mapel.*, pengawas.nama,pengawas.nip FROM mapel INNER JOIN pengawas ON mapel.idguru=pengawas.id_pengawas WHERE id_mapel='$id_mapel'"));
	$mapel=mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT mapel.*, mata_pelajaran.nama_mapel FROM mapel INNER JOIN mata_pelajaran ON mapel.nama=mata_pelajaran.kode_mapel WHERE id_mapel='$id_mapel'"));
	if(date('m')>=7 AND date('m')<=12) {
		$ajaran = date('Y')."/".(date('Y')+1);
	}elseif(date('m')>=1 AND date('m')<=6) {
		$ajaran = (date('Y')-1)."/".date('Y');
	}

	$querysetting=mysqli_query($koneksi, "SELECT * FROM setting WHERE id_setting='1'");
	$setting=mysqli_fetch_assoc($querysetting);
	// 
	// $query=mysqli_query("SELECT * FROM siswa WHERE id_kelas='$id_kelas'");
	if($id_kelas=='semua'){
	$query=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE nilai.id_mapel='$id_mapel' group by nama");
	}else{
	$query=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE siswa.id_kelas='$id_kelas' AND nilai.id_mapel='$id_mapel' group by nama");
	}
	$jumlahData = mysqli_num_rows($query);
	$jumlahn = '24';	//jumlah baris yang ingin ditampilkan
	$n = ceil($jumlahData/$jumlahn);	//jumlah halaman
	$nomer = 1;
	$tglsekarang = buat_tanggal('d M Y');

	for($i=1;$i<=$n;$i++){
		$mulai = $i-1;
		$batas = ($mulai*$jumlahn);	//jumlah baris yang tidak ingin ditampilkan
		$startawal = $batas;
		$batasakhir = $batas+$jumlahn;
		if ($i==$n){
			echo "
			<div class='page'>
				<table width='100%'>
					<tr>
						<td width='100'><img src='$homeurl/dist/img/jabar.png' height='75'></td>
						<td>
							<CENTER>
								<strong class='f12'>
									REKAPITULASI NILAI <BR>
									$setting[nama_ujian]<BR>TAHUN PELAJARAN $ajaran
								</strong>
							</CENTER></td>
							<td width='100'><img src='$homeurl/$setting[logo]' height='75'></td>
					</tr>
				</table>
				
				<table class='detail'>
					<tr>
						<td>SEKOLAH/MADRASAH</td><td>:</td><td><span style='width:450px;'>&nbsp;$setting[sekolah]</span></td>
					</tr>
					<tr>
						<td>KELAS</td><td>:</td><td><span style='width:450px;'>&nbsp;$id_kelas</span></td>
					</tr>
					<tr>
						<td>MATA PELAJARAN</td><td>:</td><td colspan='4'><span style='width:450px;'>&nbsp;$mapel[nama_mapel]</span></td>
					</tr>
					<tr>
						<td>BOBOT PG</td><td>:</td><td colspan='4'><span style='width:100px;'>&nbsp;$mapel[bobot_pg] %</span></td>	
					</tr>
					<tr>
						<td>BOBOT ESSAY</td><td>:</td><td colspan='4'><span style='width:100px;'>&nbsp;$mapel[bobot_esai] %</span></td>
					</tr>
				</table>
				<hr/>
				<table class='it-grid it-cetak' width='100%'>
				<thead>
				<tr>
				<th style='text-align:center' rowspan='3'>#</th>
				<th style='text-align:center' rowspan='3'>No Peserta</th>
				<th style='text-align:center' rowspan='3'>Nama</th>
				<th style='text-align:center' colspan='3'>$mapel[nama]</th>
			</tr>";
			$kdu = mysqli_query($koneksi,"SELECT * FROM nilai group by kode_ujian");
				  while($kd_ujian = mysqli_fetch_array($kdu)) {
					  echo"
				<th style='text-align:center' colspan='3'>$kd_ujian[kode_ujian]</th>";
				  }
			echo"
			</tr>
			<tr>";
		$mapelQ = mysqli_query($koneksi,"SELECT * FROM mapel a inner join nilai b ON a.id_mapel=b.id_mapel group by b.kode_ujian ");
		while($mapel=mysqli_fetch_array($mapelQ)){
				echo "
					<th style='text-align:center'>B</th>
					<th style='text-align:center'>S</th>
					<th style='text-align:center'>SKOR</th>";
					}
					echo "
					</tr>
					</thead>";
					// $ckck=mysqli_query("SELECT * FROM siswa WHERE id_kelas='$id_kelas' limit $batas, $jumlahn");
					if($id_kelas=='semua'){
					$ckck=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE nilai.id_mapel='$id_mapel' group by nama limit $batas, $jumlahn ");
					}else{
					$ckck=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE siswa.id_kelas='$id_kelas' AND nilai.id_mapel='$id_mapel' group by nama limit $batas, $jumlahn");
					}
					while($siswa= mysqli_fetch_array($ckck)){
						$no++;
						$lama = $jawaban = $skor = $totalskor = $skoresai='--';
						// $kelas = fetch('kelas',array('id_kelas'=>$siswa['id_kelas']));
						$nilaiQ = mysqli_query($koneksi, "SELECT * FROM nilai WHERE id_mapel='$id_mapel' AND id_siswa='$siswa[id_siswa]'");
						$nilaiC = mysqli_num_rows($nilaiQ);
						$nilai = mysqli_fetch_array($nilaiQ);
						$avg = mysqli_fetch_array($ratarata);
						if($nilaiC<>0) {
							if($nilai['ujian_mulai']<>'' AND $nilai['ujian_selesai']<>'') {
								$jawaban = "$nilai[jml_benar] benar / $nilai[jml_salah] salah";
								$benar = "$nilai[jml_benar]";
								$salah = "$nilai[jml_salah]";
								$skor = number_format($nilai['skor'],2,'.','');
								$totalskor = number_format($nilai['total'],2,'.','');
								$skoresai=number_format($nilai['nilai_esai'],2,'.','');
							}
						}
						echo "
						<tr>
							<td align='center'>$no</td>
							
							<td align='center'>$siswa[no_peserta]</td>
							<td>$siswa[nama]</td>
							<td align='center'>$benar</td>
							<td align='center'>$salah</td>
							<td align='center'>$totalskor</td>
						</tr>";
					}
					echo "
				</table>
			
				<br><br>
				<table border=0 width='100%'>
				<tr>
				<td width='5%'>Mengatahui</td>
				<td width='5%'>Mengetahui</td>
				<td width='1.5%'>Mengatahui</td>
				</tr>
				<tr>
				<td>$setting[kota], $tglsekarang</td>
				<td>$setting[kota], $tglsekarang</td>
				<td width='0%'>$setting[kota], $tglsekarang</td>
				</tr>
				<tr>
				<td>Kepala Sekolah,</td>
				<td>Guru Mapel,</td>
				<td width='0%'>Proktor,</td>
				</tr>
				<tr>
				<td><br><br><br><br><br><strong>$setting[kepsek]</strong></td>
				<td><br><br><br><br><br><strong>$sqg[nama]</strong></td>
				<td width='2%'><br><br><br><br><br><strong>M.BAGUS IKHSAN,S.KOM</strong></td>
				</tr>
				<tr>
				<td>NIP. $setting[nip]</td>
				<td>NIP. $sqg[nip] </td>
				<td width='0%'>NIP. </td>
				</tr>
				</table>

			</div>";
			break;
		}
		echo "
		<div class='page'>
			<table width='100%'>
				<tr>
					<td width='100'><img src='$homeurl/dist/img/jabar.png' height='75'></td>
					<td>
						<CENTER>
							<strong class='f12'>
								REKAPITULASI NILAI <BR>
								USBN BERBASIS KOMPUTER SMA/SMK/MA<BR>TAHUN PELAJARAN $ajaran
							</strong>
						</CENTER></td>
						<td width='100'><img src='$homeurl/$setting[logo]' height='75'></td>
				</tr>
			</table>
			<hr/>
			<table class='detail'>
				<tr>
					<td>SEKOLAH/MADRASAH</td><td>:</td><td><span style='width:450px;'>&nbsp;$setting[sekolah]</span></td>
				</tr>
				<tr>
					<td>KELAS</td><td>:</td><td><span style='width:450px;'>&nbsp;$id_kelas</span></td>
				</tr>
				<tr>
					<td>MATA PELAJARAN</td><td>:</td><td colspan='4'><span style='width:450px;'>&nbsp;$mapel[nama_mapel]</span></td>
				</tr>
				<tr>
					<td>BOBOT PG</td><td>:</td><td colspan='4'><span style='width:100px;'>&nbsp;$mapel[bobot_pg] %</span></td>	
				</tr>
				<tr>
					<td>BOBOT ESSAY</td><td>:</td><td colspan='4'><span style='width:100px;'>&nbsp;$mapel[bobot_esai] %</span></td>
				</tr>
			</table>
			<table class='it-grid it-cetak' width='100%'>
				<thead>
				<tr>
				<th style='text-align:center' rowspan='3'>#</th>
				<th style='text-align:center' rowspan='3'>No Peserta</th>
				<th style='text-align:center' rowspan='3'>Nama</th>
				<th style='text-align:center' colspan='3'>$mapel[nama]</th>
			</tr>";
			$kdu = mysqli_query($koneksi,"SELECT * FROM nilai group by kode_ujian");
				  while($kd_ujian = mysqli_fetch_array($kdu)) {
					  echo"
				<th style='text-align:center' colspan='3'>$kd_ujian[kode_ujian]</th>";
				  }
			echo"
			</tr>
			<tr>";
		$mapelQ = mysqli_query($koneksi,"SELECT * FROM mapel a inner join nilai b ON a.id_mapel=b.id_mapel group by b.kode_ujian ");
		while($mapel=mysqli_fetch_array($mapelQ)){
				echo "
					<th style='text-align:center'>B</th>
					<th style='text-align:center'>S</th>
					<th style='text-align:center'>SKOR</th>";
					}
					echo "
					</tr>
					</thead>";
				// $ckck=mysqli_query("SELECT * FROM siswa WHERE id_kelas='$id_kelas' limit $batas, $jumlahn");
				if($id_kelas=='semua'){
				$ckck=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE nilai.id_mapel='$id_mapel' group by nama limit $batas, $jumlahn");
				}else{
				$ckck=mysqli_query($koneksi, "SELECT siswa.*, nilai.id_mapel, nilai.id_siswa FROM siswa INNER JOIN nilai ON siswa.id_siswa=nilai.id_siswa WHERE siswa.id_kelas='$id_kelas' AND nilai.id_mapel='$id_mapel' group by nama limit $batas, $jumlahn");
				}
				while($siswa= mysqli_fetch_array($ckck)){
					$no++;
					$lama = $jawaban = $skor = $totalskor = $skoresai='--';
					// $kelas = fetch('kelas',array('id_kelas'=>$siswa['id_kelas']));
					$nilaiQ = mysqli_query($koneksi, "SELECT * FROM nilai WHERE id_mapel='$id_mapel' AND id_siswa='$siswa[id_siswa]'");
					$nilaiC = mysqli_num_rows($nilaiQ);
					$nilai = mysqli_fetch_array($nilaiQ);
					if($nilaiC<>0) {
						if($nilai['ujian_mulai']<>'' AND $nilai['ujian_selesai']<>'') {
							$jawaban = "$nilai[jml_benar] benar / $nilai[jml_salah] salah";
							$benar = "$nilai[jml_benar]";
							$salah = "$nilai[jml_salah]";
							$skor = number_format($nilai['skor'],2,'.','');
							$totalskor = number_format($nilai['total'],2,'.','');
							$skoresai=number_format($nilai['nilai_esai'],2,'.','');
						}
					}
					echo "
					<tr>
						<td align='center'>$no</td>
						
						<td align='center'>$siswa[no_peserta]</td>
						<td>$siswa[nama]</td>
						<td align='center'>$benar</td>
						<td align='center'>$salah</td>
						<td align='center'>$totalskor</td>
					</tr>";
				}
				echo "
			</table>
		</div>";
	}
?>