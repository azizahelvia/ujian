<?php
	require("../config/config.default.php");
	require("../config/config.function.php");
	require("../config/functions.crud.php");
	require("../config/dis.php");
	(isset($_SESSION['id_pengawas'])) ? $id_pengawas = $_SESSION['id_pengawas'] : $id_pengawas = 0;
	($id_pengawas==0) ? header('location:login.php'):null;
	echo "<style> .str{ mso-number-format:\@; } </style>";
	$id_mapel = $_GET['m'];
	$id_kelas = $_GET['k'];
	$pengawas = fetch($koneksi,'pengawas',array('id_pengawas'=>$id_pengawas));
	$mapel = fetch($koneksi,'mapel',array('id_mapel'=>$id_mapel));
	$kelas = fetch($koneksi,'kelas',array('id_kelas'=>$id_kelas));
	if(date('m')>=7 AND date('m')<=12) {
		$ajaran = date('Y')."/".(date('Y')+1);
	}
	elseif(date('m')>=1 AND date('m')<=6) {
		$ajaran = (date('Y')-1)."/".date('Y');
	}
	$file = "INDIKATOR_SOAL_".$mapel['tgl_ujian']."_".$mapel['nama']."_".$kelas['nama'];
	$file = str_replace(" ","-",$file);
	$file = str_replace(":","",$file);
	header("Content-type: application/octet-stream");
	header("Pragma: no-cache");
	header("Expires: 0");
	header("Content-Disposition: attachment; filename=".$file.".xls");
	echo "
		Mata Pelajaran: $mapel[nama]<br/>
		Tanggal Ujian: ".buat_tanggal('D, d M Y - H:i',$mapel['tgl_ujian'])."<br/>
		Jumlah Soal: $mapel[jml_soal]<br/>
		
		Kelas: "; 
		$dataArray = unserialize($mapel['kelas']);
		foreach ($dataArray as $key => $value) {
		echo "<small class='label label-success'>$value</small>&nbsp;";
		}
		echo "<br/>
		<table border='1'>
											
															<tr>
																<th width='5px'>No</th>
																<th colspan='5'>Soal Pilihan Ganda</th>
																<th>Responden</th>
																<th>Benar</th>	
																<th>Salah</th>	
																<th>Indikator Soal</th>														
															</tr>";
														$isso = mysqli_query($koneksi,"SELECT soal.id_soal,soal.nomor,soal.soal,soal.jawaban,soal.pilA,soal.pilB,soal.pilC,soal.pilD,soal.pilE FROM mapel INNER JOIN soal ON mapel.id_mapel = soal.id_mapel WHERE mapel.id_mapel='$id_mapel' and soal.jenis = 1 order by soal.nomor ASC");
														while($soal=mysqli_fetch_array($isso)){
															echo"															
															<tr>
																<td>$soal[nomor]</td>
																<td colspan='5' width='500px'>$soal[soal]<br>";
																
																echo"
																</td>
																<td style='vertical-align:middle; text-align:center;'>";
																$jsis = mysqli_fetch_array(mysqli_query($koneksi,"SELECT COUNT(id_jawaban) AS jsiswa FROM hasil_jawaban WHERE id_soal= '$soal[id_soal]'"));
																echo"$jsis[jsiswa]
																</td>
																
																<td style='vertical-align:middle; text-align:center;'>";
																$jben = mysqli_fetch_array(mysqli_query($koneksi,"SELECT SUM(IF(soal.jawaban = hasil_jawaban.jawaban,1,0)) AS kunci FROM soal INNER JOIN hasil_jawaban ON soal.id_soal = hasil_jawaban.id_soal WHERE soal.id_soal = '$soal[id_soal]'"));
																echo"$jben[kunci]
																</td>
																
																<td style='vertical-align:middle; text-align:center;'>";
																$jsal = mysqli_fetch_array(mysqli_query($koneksi,"SELECT SUM(IF(soal.jawaban <> hasil_jawaban.jawaban,1,0)) AS kunci FROM soal INNER JOIN hasil_jawaban ON soal.id_soal = hasil_jawaban.id_soal WHERE soal.id_soal = '$soal[id_soal]'"));
																echo"$jsal[kunci]
																</td>
																
																<td style='vertical-align:middle' text-align:center;'>";$anali = round((($jben['kunci']/$jsis['jsiswa'])*100),2); 
																
																echo"$anali %</td>
																</td>
															</tr>
															";
														}
														echo"
				</table>
	";
?>