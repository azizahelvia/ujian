<?php
require("../config/config.default.php");
require("../config/config.function.php");
$cekdb = mysqli_query($koneksi, "select 1 from `pengawas` LIMIT 1");
if ($cekdb == false) {
    header("Location: ../install.php");
}

$soal = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM mapel"));
$siswa = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM siswa"));
$pengawas = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE level = 'guru'"));
$kelas = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM kelas"));
$mapel = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM mata_pelajaran"));
$ceks = mysqli_fetch_array(mysqli_query($koneksi, "select * from setting"));
$namaaplikasi = $ceks['aplikasi'];
$namasekolah = $ceks['sekolah'];

$prop = $setting['XProv'];
$kab  = $setting['XKab'];
$kec  = $setting['XKec'];

$sql1 = mysqli_query($koneksi, "select * from inf_lokasi where lokasi_kabupatenkota='$kab' and lokasi_propinsi='$prop' and lokasi_kecamatan='0000' and lokasi_kelurahan='0000'");
$xadm1 = mysqli_fetch_array($sql1);
$xkab = $xadm1['lokasi_nama'];

$sql2 = mysqli_query($koneksi, "select * from inf_lokasi where lokasi_kecamatan='$kec' and lokasi_kabupatenkota='$kab' and lokasi_propinsi='$prop' and lokasi_kelurahan='0000'");
$xadm2 = mysqli_fetch_array($sql2);
$xkec = $xadm2['lokasi_nama'];


$sql3 = mysqli_query($koneksi, "select * from inf_lokasi where lokasi_propinsi='$prop' and lokasi_kecamatan='00' and lokasi_kelurahan='0000' and lokasi_kabupatenkota='00'");
$xadm3 = mysqli_fetch_array($sql3);
$xprop = strtoupper($xadm3['lokasi_nama']);

if (isset($_POST['submit'])) {
    $info = '';
    $username = $_POST['username'];
    $password = $_POST['password'];
    $query = mysqli_query($koneksi, "SELECT * FROM pengawas WHERE username='$username'");

    $cek = mysqli_num_rows($query);
    $user = mysqli_fetch_array($query);


    if ($cek <> 0) {

        if ($user['level'] == 'admin') {

            if (!password_verify($password, $user['password'])) {
                $info = info("Password salah!", "NO");
            } else {
                $_SESSION['id_pengawas'] = $user['id_pengawas'];
                $_SESSION['level'] = 'admin';
                header('location:.');
            }
        } elseif ($user['level'] == 'guru') {

            if ($password == $user['password']) {
                $_SESSION['id_pengawas'] = $user['id_pengawas'];
                $_SESSION['level'] = 'guru';
                header('location:.');
            } else {
                $info = info("Password salah!", "NO");
            }
        }
    } elseif ($cek == 0 or $cekguru == 0) {
        $info = info("Sekolah tidak terdaftar!");
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <title>
        <?php echo    $namaaplikasi; ?>
    </title>

    <!-- Vendor -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../sans/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="../sans/vendor/wow/css/animate.min.css">
    <link rel="stylesheet" href="../sans/vendor/chart/Chart.min.css">

    <!--Assets-->
    <link rel="stylesheet" href="../sans/assets/css/front.min.css">
    <link href="<?php echo "$homeurl/$setting[logo]"; ?>" rel="shortcut icon">
    <style type="text/css">
        /* Chart.js */
        @keyframes chartjs-render-animation {
            from {
                opacity: .99
            }

            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            animation: chartjs-render-animation 1ms
        }

        .chartjs-size-monitor,
        .chartjs-size-monitor-expand,
        .chartjs-size-monitor-shrink {
            position: absolute;
            direction: ltr;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            overflow: hidden;
            pointer-events: none;
            visibility: hidden;
            z-index: -1
        }

        .chartjs-size-monitor-expand>div {
            position: absolute;
            width: 1000000px;
            height: 1000000px;
            left: 0;
            top: 0
        }

        .chartjs-size-monitor-shrink>div {
            position: absolute;
            width: 200%;
            height: 200%;
            left: 0;
            top: 0
        }
    </style>
</head>

<body data-spy="scroll" data-target="#menu" data-offset="100">
    <div class='modal fade' id='modalinfo' style='display: none;'>
        <div class='modal-dialog'>
            <div class='modal-content' style="background-color:lightskyblue; padding:10px; border-radius:15px;">
                <div class='modal-header'>
                    <h4 class='modal-title'><b>Informasi</b></h4>
                    <button class='close' data-dismiss='modal'><span aria-hidden='true'><i class='fa fa-remove'></i></span></button>
                </div>
                <div class='modal-body' style="background-color:white; font-size:14px;">
                    <p>Apa bila ditemukan kendala saat penginputan nilai atau ada soal yang tidak lengkap harap menghubungi admin ujian.</p>
                    <span><b>Kontak Admin</b></span><br>
                    <?php $pengawas1 = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE level = 'admin'")); ?>
                    <span>Nama</span><span style="margin-left:22px">: <?= $setting['proktor'] ?></span><br>
                    <span>No WA</span><span style="margin-left:20px;">: <a target="_blank" href="https://api.whatsapp.com/send?phone=62<?= $setting['no_proktor'] ?>&text=Assalamualaikum%20Admin%20ujian%20<?= $setting['sekolah'] ?>%20Saya%20ingin%20bertanya%20mengenai%20Ujian%20<?= $setting['sekolah'] ?>" style="text-decoration:none; color:black;"><?= $setting['telp'] ?></a></span><br>
                    <span>Email</span><span style="margin-left:27px">: <?= $setting['email'] ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="home-wrapper" id="home">
        <div class="home-header">
            <div class="container p-0">
                <nav class="navbar navbar-expand-lg navbar-light" id="navbar-header">
                    <a class="navbar-brand" href="javascript:;">
                        <img class='img img-responsive' style='max-width:200px;' src="<?php echo "$homeurl/$setting[logo]"; ?>" width='75'>
                        <div class="home-header-text d-none d-sm-block">
                            <h5><?php echo    $namaaplikasi; ?></h5>
                            <h4><?php echo     "$ceks[sekolah] - PROVINSI $ceks[provinsi]"; ?></h4>
                            <h6><?php echo    " $ceks[kota] - KECAMATAN $ceks[kecamatan]"; ?></h6>
                            <h6>&copy; <?= $namaaplikasi; ?> 2020</h6>
                        </div>
                        <span class="logo-mini-unbk d-block d-sm-none">CBT-APP</span>
                        <span class="logo-mini-tahun d-block d-sm-none">&nbsp;2020</span>
                    </a>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="menu">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#" id="link-home" data-toggle="modal" data-target="#modalinfo">Info</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="home-banner">
            <div class="home-banner-bg home-banner-bg-color"></div>
            <div class="home-banner-bg home-banner-bg-img"></div>
            <div class="container mt-5">
                <div class="row">
                    <div class="col-sm-8">
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                            <!-- <ol class="carousel-indicators">
                                    <li data-target="#carousel" data-slide-to="0" class=""></li>
                                    <li data-target="#carousel" data-slide-to="1" class=""></li>
                                    <li data-target="#carousel" data-slide-to="2" class=""></li>
                                    <li data-target="#carousel" data-slide-to="3" class=""></li>
                                    <li data-target="#carousel" data-slide-to="4" class="active"></li>
                                    <li data-target="#carousel" data-slide-to="5"></li>
                                </ol> -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div>
                                        <h5 data-animation="animated fadeInDownBig" style="-webkit-animation-delay:undefined;animation-delay:undefined" class="">
                                            Data Statistik Ujian <?php echo "$setting[sekolah]"; ?>
                                        </h5>
                                        <h5 data-animation="animated fadeInDownBig" style="-webkit-animation-delay:undefined;animation-delay:undefined" class="">
                                            Tahun Pelajaran 2020/2021
                                        </h5>
                                        <ul>
                                            <li data-animation="animated flipInX" data-delay="1s" style="-webkit-animation-delay:1s;animation-delay:1s" class="animated flipInX">
                                                <?php echo "$siswa"; ?> Jumlah Siswa
                                            </li>
                                            <li data-animation="animated flipInX" data-delay="2s" style="-webkit-animation-delay:2s;animation-delay:2s" class="animated flipInX">
                                                <?php echo "$kelas"; ?> Jumlah Kelas
                                            </li>
                                            <li data-animation="animated flipInX" data-delay="3s" style="-webkit-animation-delay:3s;animation-delay:3s" class="animated flipInX">
                                                <?php echo "$soal"; ?> Jumlah Soal
                                            </li>
                                            <li data-animation="animated flipInX" data-delay="4s" style="-webkit-animation-delay:4s;animation-delay:4s" class="animated flipInX">
                                                <?php echo "$mapel"; ?> Jumlah Mapel
                                            </li>
                                            <li data-animation="animated flipInX" data-delay="5s" style="-webkit-animation-delay:5s;animation-delay:5s" class="animated flipInX">
                                                <?php echo "$pengawas"; ?> Jumlah Guru
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- <div class="carousel-item">
                                    <div>
                                        <h5 data-animation="animated fadeInDownBig" style="-webkit-animation-delay:undefined;animation-delay:undefined" class="">
                                            Apa Itu CBT-APP?
                                        </h5>
                                        <br>
                                        <p data-animation="animated slideInRight" data-delay="1s" style="-webkit-animation-delay:1s;animation-delay:1s" class="">
                                            CBT-APP disebut juga Aplikasi Computer Based Test (CBT) adalah sistem pelaksanaan simulasi ujian PAS-PAT-USBN dengan menggunakan komputer sebagai media ujiannya.
                                        </p>
                                        <p data-animation="animated slideInRight" data-delay="2s" style="-webkit-animation-delay:2s;animation-delay:2s" class="">
                                            Dalam pelaksanaannya, CBT-APP berbeda dengan sistem ujian nasional berbasis kertas atau Paper Based Test (PBT) yang selama ini sudah berjalan.
                                        </p>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card card-login">
                            <div class="card-body"><span><span>
                                        <h6 align="center"><b>LOGIN</b></h6>
                                        <form action='' method='post' name='fmLogin' id='fmLogin' class="validate-form">

                                            <div class="form-group">
                                                <span class="fa fa-user"></span>
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                                            </div>
                                            <div class="form-group">
                                                <span class="fa fa-key"></span>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>

                                            <button type="submit" name='submit' class="btn btn-primary btn-block btn-login">
                                                Masuk
                                            </button>
                                            <?= $info ?>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="home-footer">
            <div class="container text-center">
                Copyright © 2020, SMK WISATA INDONESIA By Team TKJ
            </div>
        </div>
    </div>
    <!-- Vendor -->
    <script src="../sans/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="../sans/vendor/jquery/jquery.form.min.js"></script>
    <script src="../sans/vendor/bootstrap-4/js/bootstrap.min.js"></script>
    <script src="../sans/vendor/bootstrap-4/js/popper.min.js"></script>
    <script src="../sans/vendor/js/wow.min.js"></script>
    <script src="../sans/vendor/chart/Chart.min.js"></script>
    <!-- Assets -->
    <script src="../sans/assets/js/front.min.js"></script>

</body>

</html>