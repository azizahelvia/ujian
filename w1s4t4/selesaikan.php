<?php
require("../config/config.default.php");
require("../config/config.function.php");
require("../config/functions.crud.php");
$idnilai = $_POST['id'];
$nilai = fetch($koneksi, 'nilai', array('id_nilai' => $idnilai));
$idm = $nilai['id_mapel'];
$ids = $nilai['id_siswa'];
$idk = $nilai['id_kelas'];
$idu = $nilai['kode_ujian'];
$where = array(
	'id_mapel' => $idm,
	'id_siswa' => $ids,
	'kode_ujian' => $idu
);
$benar = $salah = 0;
$mapel = fetch($koneksi, 'mapel', array('id_mapel' => $idm));
$siswa = fetch($koneksi, 'siswa', array('id_siswa' => $ids));
$ceksoal = select($koneksi, 'soal', array('id_mapel' => $idm));
foreach ($ceksoal as $getsoal) {
	$w = array(
		'id_siswa' => $ids,
		'id_mapel' => $idm,
		'id_soal' => $getsoal['id_soal'],
		'jenis' => 1
	);
	$cekjwb = rowcount($koneksi, 'jawaban', $w, array('jenis' => 1));
	if ($cekjwb <> 0) {
		$getjwb = fetch($koneksi, 'jawaban', $w);
		($getjwb['jawaban'] == $getsoal['jawaban']) ? $benar++ : $salah++;
	} else {
		$salah++;
	}
}
$jumsalah = $mapel['tampil_pg'] - $benar;
$bagi = $mapel['tampil_pg'] / 100;
$bobot = $mapel['bobot_pg'] / 100;
$skorx = ($benar / $bagi) * $bobot;
$skor = number_format($skorx, 2, '.', '');
$data = array(
	'ujian_selesai' => $datetime,
	'jml_benar' => $benar,
	'jml_salah' => $jumsalah,
	'skor' => $skor,
	'total' => $skor
);
update($koneksi, 'nilai', $data, $where);
delete($koneksi, 'pengacak', $where);
delete($koneksi, 'pengacakopsi', $where);
