<style type="text/css">
    table {
        font-family: 'OCR A Extended';
        border-collapse: collapse;
        border: 0;
        width: 100%;
        box-shadow: 1px 2px 3px #ccc;
    }

    .center {
        text-align: center;
    }

    .center table {
        margin: 1em auto;
        text-align: left;
    }

    .center th {
        text-align: center !important;
    }

    td,
    th {
        border: 1px solid #666;
        font-size: 90%;
        vertical-align: baseline;
        padding: 4px 5px;
    }

    h1 {
        font-size: 150%;
    }

    h2 {
        font-size: 125%;
    }

    .p {
        text-align: left;
    }

    .e {
        background-color: #F5F5DC;
        width: 300px;
        font-weight: bold;
    }

    .h {
        background-color: #99c;
        font-weight: bold;
    }

    .v {
        background-color: #DCDCDC;
        max-width: 300px;
        overflow-x: auto;
        word-wrap: break-word;
    }

    .v i {
        color: #999;
    }
</style>
<div class="panel">
    <div class='box-header with-border' style='background-color:#ff8c00'>
        <h3 class='box-title'><img src='../dist/img/svg/statistics.svg' width='30'> Informasi System</h3>
        <div class='box-tools pull-right'>
        </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#prober" data-toggle="tab">Prober</a></li>
            <li><a href="#Informasi" data-toggle="tab">Info APP</a></li>
        </ul>
    </div>

    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane" id="Informasi">
                <div class="panel">
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td>Versi CBT </td>
                                <td><?= APLIKASI . " v" . 1 . " r" . 1.6 ?></td>
                            </tr>
                            <tr>
                                <td>Webserver </td>
                                <td><?= $_SERVER['SERVER_SOFTWARE']; ?></td>
                            </tr>
                            <tr>
                                <td>Versi PHP </td>
                                <td><?= phpversion(); ?></td>
                            </tr>
                            <tr>
                                <td>Database </td>
                                <td><?= mysqli_get_server_info($koneksi); ?></td>
                            </tr>
                            <tr>
                                <td>Sistem Operasi </td>
                                <td><?php echo php_uname('a'); ?></td>
                            </tr>
                            <tr>
                                <td>Modif</td>
                                <td>@Wistek</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane  active" id="prober">
                <iframe src="<?= $homeurl ?>/w1s4t4/prober.php" style="height:400px;width:100%; border:none"></iframe>
            </div>
        </div>
    </div>
</div>