<?php
require "../config/config.koneksipusat.php";
$pusat = mysqli_fetch_array(mysqli_query($koneksipusat,"SELECT * FROM setting WHERE id_setting='1'"));
$tokenapi = $pusat['db_token'];
$info1 = '';
if (isset($_POST['simpanserver'])) :
    $exec = mysqli_query($koneksi, "UPDATE setting SET id_server='$_POST[id_server]', db_folder='$_POST[db_folder]', db_host='$_POST[db_host]',db_name='$_POST[db_name]',db_user='$_POST[db_user]',db_pass='$_POST[db_pass]',db_url='$_POST[db_url]',db_token='$_POST[db_token]'  WHERE id_setting='1'");
    if ($exec) {
        $info1 = info('Berhasil menyimpan pengaturan!', 'OK');
    }
endif; ?>
<div class='row'>
    <div class='col-md-12'>
        <div class='box box-solid'>
            <div class='box-header with-border' style='background-color:#ff8c00'>
                <h3 class='box-title'><i class='fa fa-gear'></i> Setting Sinkronisasi</h3>
            </div><!-- /.box-header -->
            <div class='box-body'>
                <div class='box box-solid '>
                    <div class='box-header with-border' style='background-color:#ff8c00'>
                        <h3 class='box-title'><i class='fa fa-desktop'></i> Status Server</h3>					
                    </div><!-- /.box-header --><br>
						<button  class='btn btn-flat btn-danger' data-toggle='modal' data-target='#lihatapi'><i class='fa fa-globe'></i> Lihat Token API </button>
									<div class='modal fade' id='lihatapi' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header' style='background-color:#ff8c00'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'>Kode API</h3>
												</div>
												<div class='modal-body'>
														<input type='text' name='token' value="<?= $tokenapi ?>" class='form-control'/>									 
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
												</div>
											</div>					
										</div>											
									</div>
                    <div class='box-body'>
                        <center><img id='loading-image' src='../dist/img/ajax-loader.gif' style='display:none; width:50px;' />
                            <center>
                                <div id='statusserver'>
                                </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <form action='' method='post' enctype='multipart/form-data'>
                    <div class='box-body'>
                        <?= $info1 ?>
                        <div class='form-group'>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <label>ID SERVER</label>
                                    <input type='text' name='id_server' value="<?= $setting['id_server'] ?>" class='form-control' required='true' />
                                </div>
                                <div class='col-md-6'>
                                    <label>Folder CBT</label>
                                    <input placeholder="Default Kosongkan" type='text' name='db_folder' value="<?= $setting['db_folder'] ?>" class='form-control' />
                                    <span style="font-size: 12px; color: red;">Contoh: http://192.168.0.200/nama_folder_cbtcandy</span>
                                </div>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <label>Domain / IP Addres CBT</label>
                                    <input placeholder="http://192.168.0.200/candycbt" type='text' name='db_url' value="<?= $setting['db_url'] ?>" class='form-control' required='true' />
                                    <span style="font-size: 12px; color: red;">Contoh IP Address: http://192.168.0.200/candycbt</span><br>
                                    <span style="font-size: 12px; color: red;">Contoh Domain: http://www.domain.com/candycbt</span>
                                </div>
                                <div class='col-md-6'>
                                    <label>db_host/ip</label>
                                    <input type='text' name='db_host' value="<?= $setting['db_host'] ?>" class='form-control' required='true' />
                                </div>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='row'>
                                
                                <div class='col-md-6'>
                                    <label>db_user</label>
                                    <input type='text' name='db_user' value="<?= $setting['db_user'] ?>" class='form-control' required='true' />
                                </div>
                                <div class='col-md-6'>
                                    <label>db_password</label>
                                    <input type='password' name='db_pass' value="<?= $setting['db_pass'] ?>" class='form-control' />
                                </div>
                            </div>
                        </div>
                       
                        <div class='form-group'>
                            <div class='row'>
                                
                                <div class='col-md-6'>
                                    <label>db_name</label>
                                    <input type='text' name='db_name' value="<?= $setting['db_name'] ?>" class='form-control' />
                                </div>
                                <div class='col-md-6'>
                                    <label>Token API</label>
                                    <input type='text' name='db_token' placeholder="Masukan Token API" value="<?= $setting['db_token'] ?>" class='form-control' />
                                </div>
                            </div>
                        </div>
                       
                        <div class='col-md-6'>
                            <button type='submit' name='simpanserver' class='btn btn-flat pull-right btn-success' style='margin-bottom:5px'><i class='fa fa-check'></i> Simpan</button>
                        </div>
                    </div><!-- /.box-body -->

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajax({
        type: 'POST',
        url: 'statusserver.php',
        beforeSend: function() {
            $('#loading-image').show();
        },
        success: function(response) {
			$('#statusserver').html(response);
            $('#loading-image').hide();
        }
    });
</script>