<div class='modal fade' id='tambahjadwal' style='display: none;'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' style='background-color:#a9a9a9'>
				<button class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
				<h4 class='modal-title'><i class="fas fa-business-time fa-fw"></i> Tambah Jadwal Ujian</h4>
			</div>
			<div class='modal-body'>
				<form id="formtambahujian" method='post'>
					<div class='form-group'>
						<label>Nama Bank Soal</label>
						<select name='idmapel' class='form-control' required='true'>
							<?php
							if ($pengawas['level'] == 'admin') {
								$namamapelx = mysqli_query($koneksi,"SELECT * FROM mapel where status='1' order by nama ASC");
							} else {
								$namamapelx = mysqli_query($koneksi,"SELECT * FROM mapel where status='1' and idguru='$id_pengawas' order by nama ASC");
							}
							while ($namamapel = mysqli_fetch_array($namamapelx)) {
								$dataArray = unserialize($namamapel['kelas']);
								echo "<option value='$namamapel[id_mapel]'>$namamapel[nama] - $namamapel[level] - ";
								foreach ($dataArray as $key => $value) {
									echo "$value ";
								}
								echo "</option>";
							}
							?>
						</select>
					</div>
					<div class='form-group'>
						<label>Nama Jenis Ujian</label>
						<select name='kode_ujian' class='form-control' required='true'>
							<option value=''>Pilih Jenis Ujian </option>
							<?php
							$namaujianx = mysqli_query($koneksi,"SELECT * FROM jenis where status='aktif' order by nama ASC");
							while ($ujian = mysqli_fetch_array($namaujianx)) {
								echo "<option value='$ujian[id_jenis]'>$ujian[id_jenis] - $ujian[nama] </option>";
							}
							?>
						</select>
					</div>
					<div class='form-group'>
						<div class='row'>
							<div class='col-md-6'>
								<label>Tanggal Mulai Ujian</label>
								<input type='text' name='tgl_ujian' class='tgl form-control' autocomplete='off' required='true' />
							</div>
							<div class='col-md-6'>
								<label>Tanggal Waktu Expired</label>
								<input type='text' name='tgl_selesai' class='tgl form-control' autocomplete='off' required='true' />
							</div>
						</div>
					</div>
					<div class='form-group'>
					<div class='row'>
					<div class='col-md-6'>
						<label>Sesi</label>
						<select name='sesi' class='form-control' required='true'>
							<?php
							$sesix = mysqli_query($koneksi,"SELECT * from sesi");
							while ($sesi = mysqli_fetch_array($sesix)) {
								echo "<option value='$sesi[kode_sesi]'>$sesi[kode_sesi]</option>";
							}
							?>
						</select>
						</div>
					<div class='col-md-6'>
								<label>Lama ujian (menit)</label>
								<input type='number' name='lama_ujian' class='form-control' required='true' />
					</div>
						</div>
					</div>
				
					<div class='form-group'>
						<label></label><br>
						<label>
							<input type='checkbox' class='icheckbox_square-green' name='acak' value='1' $acak /> Acak Soal
						</label>
						<label>
							<input type='checkbox' class='icheckbox_square-green' name='acak_opsi' value='1' $acak_opsi /> Acak Opsi PG
						</label>
						<label>
							<input type='checkbox' class='icheckbox_square-green' name='token' value='1' $token /> Token Soal
						</label>
						<label>
							<input type='checkbox' class='icheckbox_square-green' name='hasil' value='1' $hasil /> Hasil Tampil
						</label>
					</div>
					<div class='modal-footer'>
						<button name='tambahjadwal' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan Semua</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class='row'>
	<div class='col-md-12'>
		<div class='box box-solid'>
			<div class='box-header with-border ' style='background-color:#a9a9a9'>
				<h3 class='box-title'><i class="fas fa-envelope-open-text    "></i> Aktifasi Ujian</h3>
				<div class='box-tools pull-right '>
						<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-backdrop='static' data-target='#tambahjadwal'><i class='glyphicon glyphicon-plus'></i> <span class='hidden-xs'>Tambah Jadwal</span></button>
				</div>
			</div><!-- /.box-header -->
			<div class='box-body'>
				<div class="col-md-1">
				</div>
				<div class="col-md-6">
					<form id='formaktivasi' action="">
						<div class="form-group">
							<label for="">Pilih Jadwal Ujian</label>
							<select class="form-control select2" name="ujian[]" style="width:100%" multiple='true' required>
								<?php $jadwal = mysqli_query($koneksi,"select * from ujian"); ?>
								<?php while ($ujian = mysqli_fetch_array($jadwal)) : ?>

									<option value="<?= $ujian['id_ujian'] ?>"><?= $ujian['nama'] . "-" . $ujian['id_pk'] ?></option>
								<?php endwhile; ?>
							</select>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label for="">Pilih Sesi</label>
									<select class="form-control select2" name="sesi" id="">
										<?php $sesi = mysqli_query($koneksi,"select * from siswa group by sesi"); ?>
										<?php while ($ses = mysqli_fetch_array($sesi)) : ?>
											<option value="<?= $ses['sesi'] ?>"><?= $ses['sesi'] ?></option>
										<?php endwhile; ?>
									</select>
								</div>
								<div class="col-md-4">
									<label for="">Pilih Aksi</label>
									<select class="form-control select2" name="aksi" required>

										<option value=""></option>
										<option value="1">Aktif</option>
										<option value="0">Non Aktif</option>
										<option value="hapus">Hapus</option>
									</select>
								</div>
							<div class="col-md-4">
									<label for="">Rerset Login<i class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="bottom" title="Jika Reset Login Aktif Maka Setiap Siswa login akan di simpan, Non Aktifkan jika tidak ingin di simpan (Siswa Bebas Masuk Keluar)"></i></label>

									<?php $query = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM setting ")); 
										if($query['reset_login'] == 0){ $a = 'selected'; }
										if($query['reset_login'] == 1){ $b = 'selected'; }
										?>
									<select data-id="<?= $query['id_setting'] ?>" id="login" class="form-control select2" name="reset_login">
										<option <?= $b; ?> value="1" >Aktif</option>
										<option <?= $a; ?> value="0">Non Aktif</option>
									</select>
								</div>
							</div>
						</div>
						<button name="simpan" class="btn btn-success">Simpan Semua</button>
					</form>
				</div>
				<div class="col-md-4">
					<div class="box-body">
						<div class='small-box bg-aqua'>
							<div class='inner'>
								<?php $token = mysqli_fetch_array(mysqli_query($koneksi,"select token from token")) ?>
								<h3><span name='token' id='isi_token'><?= $token['token'] ?></span></h3>
								<p>Token Tes</p>
							</div>
							<div class='icon'>
								<i class='fa fa-barcode'></i>
							</div>
						</div>
						<a id="btntoken" href="#"><i class='fa fa-spin fa-refresh'></i> Refreh Sekarang</a>
						<p>Token akan refresh setiap 15 menit
					</div>
				</div>
			</div><!-- /.box -->

</div>
	<div id="bodyreset">
	<div id='tablereset' class='table-responsive'>
	<table id='example1' class='table table-bordered table-striped'>
	<thead>
		<tr style='background-color:#a9a9a9; color:white;'>
			<th width='100px'>Mapel</th>
			<th width='100px'>Level/Jur/Kelas</th>
			<th>Durasi</th>
			<th >Tgl Waktu Ujian</th>
			<th>Ack.Soal/Ack.Opsi</th>
			<th width='250px'>Status Ujian</th>
			<th width='450px'>Aksi</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$mapelQ = mysqli_query($koneksi,"SELECT * FROM ujian ORDER BY status DESC, tgl_ujian ASC, waktu_ujian ASC");
		?>
		<?php while ($mapel = mysqli_fetch_array($mapelQ)) : 
			$datakelas2 = unserialize($mapel['kelas']);
			$dataidsiswa2 = unserialize($mapel['siswa']);
			if(in_array('khusus', $datakelas2) and  !empty($dataidsiswa2)){
				$warna = 'bg-red';
			}
			elseif(in_array('semua', $datakelas2)){
				$warna = 'bg-blue';
			}
			elseif(!empty($datakelas2) and  !empty($dataidsiswa2)){
				$warna = 'bg-purple';
			}
			elseif(!empty($datakelas2) and  empty($dataidsiswa2)){
				$warna = 'bg-green';
			}
			else{
				$warna = 'bg-blue';
			}
		?>
			<?php
			if ($mapel['id_pk'] == '0') {
				$jur = 'Semua';
			} else {
				$jur = $mapel['id_pk'];
			}
			?>
			 <tr>
			<td>
			<b><small class='label bg-purple'><?= $mapel['nama'] ?></small></b>
			</td>													
			<td><i class="fa fa-tag"></i> <?= $mapel['kode_ujian'] ?> &nbsp;
			<i class="fa fa-user"></i> <?= $mapel['level'] ?> &nbsp;
			<i class="fa fa-wrench"></i> <?= $jur ?>
			</td>
			<td><span class="badge bg-purple"><?= $mapel['tampil_pg'] ?> Soal / <?= $mapel['lama_ujian'] ?> menit</span></td>
			<td><small class='label bg-green'><i class='fa fa-clock-o'></i> <?= $mapel['tgl_ujian'] ?></small> <small class='label bg-red'><i class='fa fa-clock-o'></i> <?= $mapel['tgl_selesai'] ?></small></td>
			<td><center><span >
			<?php 
			if($mapel['acak']==1){ echo"<label class='badge bg-blue'>YA</label>"; }
			else{ echo "<label class='badge'>Tidak</label>"; } 
			?>
			<?php 
			if($mapel['acak_opsi']==1){ echo"<label class='badge bg-blue'>YA</label>"; }
			else{ echo "<label class='badge'>Tidak</label>"; } 
			?>
			</span>	</center>
			</td>
			<td>	
			<?php
			if ($mapel['status'] == 1) {
			echo "<i class='fa fa-spinner fa-spin'></i> <label class='badge bg-green'>Sedang Aktif</label> <label class='badge bg-red'>Sesi $mapel[sesi]</label>";
			} elseif ($mapel['status'] == 0) {
			echo "<label class='badge bg-red'>Tidak Aktif</label>";
			}
			?>
			</td>
			<td>
			<a class="badge btn-primary " href="?pg=nilai&id=<?= $mapel['id_mapel'] ?>"><i class="fa fa-bar-chart-o "></i> Nilai</a>
			<a class="badge btn-primary " href="?pg=status&id=<?= $mapel['id_mapel'] ?>"><i class="fa fa-binoculars "></i> Status</a>
			<a class="badge btn-primary " href="?pg=banksoal&ac=lihat&id=<?= $mapel['id_mapel'] ?>"><i class="fa fa-search "></i> Soal</a>
			<!-- Button trigger modal -->
			<a type="button" class="badge btn-primary" data-toggle="modal" data-target="#modelId<?=$mapel['id_ujian'] ?>">
				<i class="fa fa-pencil-square-o"></i> edit
			</a>
			</td>
			</tr> 
			<div class='modal fade' id='modelId<?= $mapel['id_ujian'] ?>' style='display: none;'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header bg-blue'>
							<h5 class='modal-title'>Edit Waktu Ujian</h5>

						</div>
						<form id="formedit<?= $mapel['id_ujian'] ?>">
							<div class='modal-body'>
								<input type='hidden' name='idm' value="<?= $mapel['id_ujian'] ?>" />
								<div class="form-group">
									<label for="mulaiujian">Waktu Mulai Ujian</label>
									<input type="text" class="tgl form-control" name="mulaiujian" value="<?= $mapel['tgl_ujian'] ?>" aria-describedby="helpId" placeholder="">
									<small id="helpId" class="form-text text-muted">Tanggal dan waktu ujian dibuka</small>
								</div>
								<div class="form-group">
									<label for="selesaiujian">Waktu Ujian Ditutup</label>
									<input type="text" class="tgl form-control" name="selesaiujian" value="<?= $mapel['tgl_selesai'] ?>" aria-describedby="helpId" placeholder="">
									<small id="helpId" class="form-text text-muted">Tanggal dan waktu ujian ditutup</small>
								</div>
								<div class='form-group'>
									<div class='row'>
										<div class='col-md-6'>
											<label>Lama Ujian</label>
											<input type='number' name='lama_ujian' value="<?= $mapel['lama_ujian'] ?>" class='form-control' required='true' />
										</div>
										<div class='col-md-6'>
											<label>Sesi</label>
											<input type='number' name='sesi' value="<?= $mapel['sesi'] ?>" class='form-control' required='true' />
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>
												<input type='checkbox' class='icheckbox_square-green' name='hasil' value='1' $hasil /> Hasil Tampil Ujian
											</label>
										</div>
										<div class="col-md-6">
											<label>
												<input type='checkbox' class='icheckbox_square-green' name='hasil2' value='1' $hasil /> Hasil Tampil Draft
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class='modal-footer'>

								<center>
									<button type="submit" class='btn btn-primary' name='simpan'><i class='fa fa-save'></i> Ganti Waktu Ujian</button>
								</center>

							</div>
						</form>
					</div>
				</div>
			</div>

			<script>
				$("#formedit<?= $mapel['id_ujian'] ?>").submit(function(e) {
					e.preventDefault();
					$.ajax({
						type: 'POST',
						url: 'jadwal/edit_jadwal.php',
						data: $(this).serialize(),
						success: function(data) {
						if (data == 'ok') {
						toastr.success('jadwal ujian berhasil diperbarui');
						}
							location.reload();
						}
					});
					return false;
				});
			</script>
		<?php endwhile; ?>
</tbody>
</table>
</div>
</div>


<script>
	$(document).ready(function() {
		$("#btntoken").click(function() {
			$.ajax({
				url: "_load.php?pg=token",
				type: "POST",
				success: function(respon) {
					$('#isi_token').html(respon);
					toastr.success('token berhasil diperbarui');
				}
			});
			return false;
		})
		$('#formaktivasi').submit(function(e) {
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: 'jadwalaktivasi.php?key=1616161616',
				data: $(this).serialize(),
				success: function(data) {
					if (data == 'hapus') {
						toastr.success('jadwal ujian berhasil di hapus');
					}
					if (data == 'update') {
						toastr.success('jadwal ujian berhasil diperbarui');
					}
					$('#bodyreset').load(location.href + ' #bodyreset');

				}
			});
			return false;
		});
		
		$("#login").change(function() {
		var a = $(this).val();
		var b = $(this).data('id');
		if(a==1){ var ca = 'Di Aktifkan';}
		else{ var ca =" Di Non Aktifkan"; }
		$.ajax({
			data :{data:a,id:b},
			url: 'jadwalaktivasi.php?setting=ganti',
			type: "POST",
			success: function(respon) {
				if(respon==1){
					toastr.success('Catat Login Berhasil '+ca);
				}
				else{
					toastr.error('Gagal');
				}
											//alert(respon);
				}
		});
		return false;

	});

	});
</script>
<script>
	$('#formtambahujian').submit(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'jadwal/tambah_jadwal.php',
			data: $(this).serialize(),
			success: function(data) {
				console.log(data);
				if (data == "OK") {
					toastr.success("jadwal berhasil dibuat");
				} else (data == "NO") {
					toastr.error("jadwal gagal tersimpan");
				}
				$('#tambahjadwal').modal('hide');
				$('#bodyreset').load(location.href + ' #bodyreset');
				window.setTimeout(function(){location.reload()},1000);
				//jump("$pg=jadwal");
			}
		});
		return false;
	});
	
</script>