<?php
	require("../config/config.default.php");
	require("../config/config.function.php");
	require("../config/functions.crud.php");
	require("../config/dis.php");
	include "phpqrcode/qrlib.php";
	(isset($_SESSION['id_pengawas'])) ? $id_pengawas = $_SESSION['id_pengawas'] : $id_pengawas = 0;
	($id_pengawas==0) ? header('location:index.php'):null;
	$id_kelas = @$_GET['id_kelas'];

	
	
	if(date('m')>=7 AND date('m')<=12) {
		$ajaran = date('Y')."/".(date('Y')+1);
	}
	elseif(date('m')>=1 AND date('m')<=6) {
		$ajaran = (date('Y')-1)."/".date('Y');
	}
	$kelas = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM kelas WHERE id_kelas='$id_kelas'"));
	echo "
		<style>
			* { font-size:x-small; }
			.box { border:1px solid #000; width:100%; height:150px; }
		</style>
		
		<table border='0' width='100px' align='center' cellpadding='2'>
			<tr>";
				$siswaQ = mysqli_query($koneksi,"SELECT * FROM siswa WHERE id_kelas='$id_kelas' ORDER BY nama ASC");
				while($siswa = mysqli_fetch_array($siswaQ)) {
					$nopeserta=$siswa['no_peserta'];
					$no++;
					
					echo "
						<td width='50%'>
							<div  style='width:20cm;border:1px solid #666;'>
								<table border='0' width='100%' align='center'>
									<tr>
										<td align='left' valign='top'>";
											$tempdir='../foto/qrcode/';

											$tempatpng=$tempdir.$nopeserta;
											$tempatip=$tempdir.'qrcodeip.png';
											
											if(!file_exists($tempatpng)){
											QRcode::png($nopeserta,$tempatpng,'L',1);
											}
											if(!file_exists($tempatip)){
											QRcode::png($setting['ip_server'],$tempatip,'M',1);
											}
										echo "
										<img src='../dist/img/jabar.png' height='40px'/>
										
										</td>
										<td align='center'>
											<b>
											PEMERINTAH PROVINSI JAWA BARAT<BR>
											DINAS PENDIDIKAN<BR>
											SEKOLAH MENENGAH ATAS SMAN 1 CIGALONTANG<BR>
											JL.Raya Cigalontang, Lengkongjaya Kec. Cigalontang 46463 Tasikmalaya<BR>
											Email: smancigalontang@gmail.com
											</b>
										</td>
										<td align='right' valign='top'>
											<img src='../$setting[logo]' height='40px'/>

										</td>
									</tr>
								</table>
								<hr/>";
									$nm_ujian=mysqli_query($koneksi,"SELECT * FROM ujian");
											$tampil=mysqli_fetch_array($nm_ujian);
											echo "
											<center>KARTU LEGITIMASI<br/>
											$setting[nama_ujian]<br/>
												BERBABSIS KOMPUTER<br/>
												TAHUN PELAJARAN $ajaran</center><BR>
								<table border='0' width='100%' align='center'>
									<tr>";
									
									echo"
									<tr>
										<td width='10%'>No Peserta</td>
										<td width='1%'>:</td>
										<td valign='top'>$siswa[no_peserta]</td>
									</tr>
									<tr>
										<td width='10%'>Nama</td>
										<td valign='top'>:</td>
										<td valign='top'> $siswa[nama]</td>
									</tr>
									<tr>
										<td width='10%'>Kelas / Ruangan</td>
										<td valign='top'>:</td>
										<td valign='top'> $kelas[nama]</td>
									</tr>
							
													
									<tr>
										<td colspan='3' valign='top'>
										<table border='1' width='100%'>
										<thead>
										<th bgcolor='#ffe4c4'>Hari</th>
										<th bgcolor='#ffe4c4'>Jam Ke</th>
										<th bgcolor='#ffe4c4'>Mata Pelajaran</th>
										<th bgcolor='#ffe4c4'>Sesi</th>
										<th bgcolor='#ffe4c4'>Mulai Ujian</th>
										</thead>
										<tbody>";
										$sesi=$siswa['sesi'];
										$idpk=$siswa['idpk'];
										$level=$siswa['level'];
										$id_kelas=$siswa['id_kelas'];
										$jadwalx=mysqli_query($koneksi,"select * from jadwal where id_kelas='$id_kelas'");
										$cekjadwal=mysqli_num_rows($jadwalx);
										if($cekjadwal > 0){
										while($jadwal=mysqli_fetch_array($jadwalx)){
										$no++;
										echo "
										<tr>
										<td align='center'>$jadwal[hari] - $jadwal[tanggal]</td>
										<td align='center'>$jadwal[jam]</td>
										<td align='center'>$jadwal[mapel]</td>
										<td align='center'>$jadwal[sesi]</td>
										<td align='center'>$jadwal[waktu] - $jadwal[habis]</td>
										</tr>
											";
										}
										}else{
											echo "
										<tr>
										<td colspan='5'>Tidak ada jadwal ujian</td>
										
										</tr>
											";
										}
										
										echo "</tbody>
										</table>
									
										</td>
										
									</tr>										
								</table>
								<BR><BR>
									<table border='0' width='100%'>	
									<tr>
										<td valign='top' align='left'>
										   Kepala Sekolah<br><br>
										   <b><img src='$homeurl/dist/img/ttdkepsek.png' style='max-width:60px'>
									   </b><br><br>
									   <b>$setting[kepsek]</b>
										
										</td
									</tr>
								</table>
							</div>";
							if(($no%3)==0) { echo "<div style='page-break-before:always;'></div>"; }
							
							echo "
						</td>
					";
					if(($no%0)==0) { echo "</tr><tr>"; } 
					
				}
				echo "
			</tr>
		</table>
	";
?>