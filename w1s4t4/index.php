	<?php
	require("../config/config.default.php");
	require("../config/config.function.php");
	require("../config/functions.crud.php");
	require("../config/excel_reader.php");
	require("../config/chaceng.php");
	require("../config/footer_chaceng.php");
	(isset($_SESSION['id_pengawas'])) ? $id_pengawas = $_SESSION['id_pengawas'] : $id_pengawas = 0;
	($id_pengawas == 0) ? header('location:login.php') : null;
	$pengawas = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM pengawas  WHERE id_pengawas='$id_pengawas'"));

	(isset($_GET['pg'])) ? $pg = $_GET['pg'] : $pg = '';
	(isset($_GET['ac'])) ? $ac = $_GET['ac'] : $ac = '';
	($pg == 'banksoal' && $ac == 'input') ? $sidebar = 'sidebar-collapse' : $sidebar = '';
	($pg == 'nilai' && $ac == 'lihat') ? $sidebar = 'sidebar-collapse' : $sidebar = '';
	$nilai = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM nilai"));
	$draf_nilai = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM hasil_nilai"));
	$jenis = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM jenis"));
	$pk = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM pk"));
	$soal = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM mapel"));
	$siswa = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM siswa"));
	$online = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM log"));
	$ruang = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM ruang"));
	$kelas = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM kelas"));
	$mapel = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM mata_pelajaran"));
	echo "
		<!DOCTYPE html>
		<html>
			<head>
  				<meta charset='utf-8'>
 				<meta http-equiv='X-UA-Compatible' content='IE=edge'>
  				<title>Administrator | $setting[aplikasi]</title>
  				<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  				<link href='$homeurl/$setting[logo]' rel='shortcut icon'>
				<link rel='stylesheet' href='$homeurl/dist/bootstrap/css/bootstrap.min.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/fileinput/css/fileinput.min.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/fontawesome/css/all.css' />
				<link rel='stylesheet' href='$homeurl/plugins/font-awesome/css/font-awesome.min.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/select2/select2.min.css'/>
				<link rel='stylesheet' href='$homeurl/dist/css/AdminLTE.min.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'>
				<link rel='stylesheet' href='$homeurl/dist/css/skins/skin-purple.min.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/jQueryUI/jquery-ui.css'>
  				<link rel='stylesheet' href='$homeurl/plugins/iCheck/square/green.css'/>
  				<link rel='stylesheet' href='$homeurl/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'>
				<link rel='stylesheet' href='$homeurl/plugins/datatables/dataTables.bootstrap.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/datatables/extensions/Select/css/select.bootstrap.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/animate/animate.min.css'>
				<link rel='stylesheet' href='$homeurl/plugins/datetimepicker/jquery.datetimepicker.css'/>
				<link rel='stylesheet' href='$homeurl/plugins/notify/css/notify-flat.css'/>
  				<link rel='stylesheet' href='$homeurl/plugins/sweetalert2/dist/sweetalert2.min.css'>
				<link rel='stylesheet' href='$homeurl/plugins/toastr/toastr.min.css'>
				<link href='$homeurl/assets/back/vendors/izitoast/css/iziToast.min.css' rel='stylesheet' />
				<script src='$homeurl/plugins/tinymce/tinymce.min.js'></script>
				<script src='$homeurl/plugins/jQuery/jquery-3.1.1.min.js'></script>

				
				
<style>
@font-face {
        font-family: tulisan_keren;
        src: url(Allura-Regular.ttf);
}
body {
    font-family: 'tulisan_keren';
    font-size: 14px;
    line-height: 1.42857143;
    color: #000;
}
.sidebar-menu>li>a {
    padding: 5px 5px 5px 10px;
    display: block;
}
  .menu-header ul {
   list-style: none;
   background: #A9A9A9;
   text-align: center;
   padding: 0;
   margin: 0;
   bottom: 0;
   left: 0;
   right: 0;
   }
.menu-header ul li a {
display: inline-block;
text-align: center;
padding: 5px 5px;
color: #fff;
margin-top: 0;
}
.menu-header ul li {
display: inline-block;
}

.skin-green .main-header .navbar {
    background-color: #02948d;
}
.bg-green{
    background-color: #02948d;
}
  .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../dist/img/loading.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
.skin-red .sidebar a {
    color: #b1c9f5;
}
.form-control {
    border-radius: 0;
    box-shadow: none;
    border-color: #afc2e8;
}
.box-header {
   
    background: #d81b60;
}
.box-header .box-title {
    color:#fff;
}
.box-header {
    color: #fff;
}
thead {
	background-color: #4cf5a7;
  
}
thead:hover {
  background-color: #92fbca;
}
.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #ddfdeb;
}
.info-box-number {
    display: block;
    font-weight: bold;
    font-size: 30px;
	color:#fff;
}
</style>
<style type='text/css' media='print'>
    .page
    {
     -webkit-transform: rotate(-90deg); 
     -moz-transform:rotate(-90deg);
     filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
	
</style>
			</head>
			<body class='hold-transition skin-purple sidebar-mini fixed $sidebar'>
			<div id='pesan'></div>
			<div class='loader'></div>
				<div class='wrapper'>
					<header class=' main-header'>
						<a href='?' class='logo' style='background-color:#00008B'>
							<span class='animated bounce logo-mini'><image src='$homeurl/$setting[logo]' height='30px'></span>
							<span class='animated bounce logo-lg'><image src='$homeurl/$setting[logo]' height='40px'></span>
						</a>
						<nav  class='navbar navbar-static-top' style='background-color:#00008B;box-shadow: 0px 10px 10px 0px rgba(0,0,0,0.1)' role='navigation''>
							<a href='#' class='sidebar-toggle' data-toggle='offcanvas' role='button'>
								<span class='sr-only' >Toggle navigation</span>
								&nbsp;&nbsp;<span class=''>$setting[sekolah]</span>
							</a>
							<div class='navbar-custom-menu'>
								<ul class='nav navbar-nav'>";
	if ($pengawas['level'] == 'admin') {
		echo "
							<li class='dropdown notifications-menu'>
								<a href='#' class='dropdown-toggle' data-toggle='dropdown' style='color:yellow'>
									<i class='fas fa-desktop fa-lg fa-fw'></i> <span style='font-size:14px'>" . strtoupper($setting['server']) . "</span>
								</a>
								<ul class='dropdown-menu' style='height:80px'>
									<li class='header'>Status Server</li>
									<li>
										<!-- inner menu: contains the actual data -->
										<ul class='menu'>";
		if ($setting['server'] == 'lokal') {
			echo "
												<li>
													<a id='btnserver' href='#'>
														<i class='fa fa-users text-aqua'></i> Server Pusat
													</a>
												</li>";
		} else {
			echo "
												<li>
													<a id='btnserver' href='#'>
														<i class='fa fa-users text-aqua'></i> Server Lokal
													</a>
												</li>";
		}
		echo "
										</ul>
									</li>
								</ul>
							</li>"; //stop
	}
	echo "
							
							<li class='dropdown notifications-menu'>
							<li><a style='color:yellow' href='?pg=informasi'><i class='fa fa-cogs fa-lg fa-fw'></i></a></li>
							<li><a style='color:yellow' href='clear.php'><i class='fa fa-trash-o'></i> CC</a></li>
									<li class='dropdown user user-menu'>
										<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
	if ($pengawas['level'] == 'admin') {
		echo "
											<img src='$homeurl/foto/fotoguru/foto1.png' class='user-image' alt='+' style='background-color:white'>
											<span class='hidden-xs'>$pengawas[nama]  &nbsp; <i class='fa fa-caret-down'></i></span>";
	} elseif ($pengawas['level'] == 'guru') {
		echo "
											<img src='$homeurl/foto/fotoguru/$pengawas[nama].jpg' class='user-image' alt='+'>
											<span class='hidden-xs'>$pengawas[nama]  &nbsp; <i class='fa fa-caret-down'></i></span>";
	}
	echo "
										</a>
										
						
										<ul class='dropdown-menu'>
											<li class='user-header'>";
	if ($pengawas['level'] == 'admin') {
		echo "<img src='$homeurl/foto/fotoguru/foto1.png' class='img-circle' alt='User Image' style='background-color:white'>";
	} elseif ($pengawas['level'] == 'guru') {
		if ($pengawas['fotoguru'] <> '') {
			echo "<img src='$homeurl/foto/fotoguru/$pengawas[nama].jpg' class='img-circle' alt='User Image'>";
		} else {
			echo "<img src='$homeurl/foto/fotoguru/$pengawas[nama].jpg' class='img-circle' alt='User Image'>";
		}
	}
	echo "<p>
													$pengawas[nama]
													<small>NIP. $pengawas[nip]</small>
												</p>
											</li>
											<li class='user-footer'>
												<div class='pull-left'>";
	if ($pengawas['level'] == 'admin') {
		echo "
													<a href='?pg=pengaturan' class='btn btn-sm btn-default btn-flat'><i class='fa fa-gear'></i> Pengaturan</a>
													";
	} elseif ($pengawas['level'] == 'guru') {
		echo "
													<a href='?pg=editguru' class='btn btn-sm btn-default btn-flat'><i class='fa fa-gear'></i> Edit Profil</a>
													";
	}
	echo "
												</div>
												<div class='pull-right'>
													<a href='logout.php' class='btn btn-sm btn-default btn-flat'><i class='fa fa-sign-out'></i> Keluar</a>
												</div>
											</li>
										</ul>	
									</li>
							</ul>		
							</div>
						</nav>
					</header>
					
					<aside class='main-sidebar'>
						<section class='sidebar'>
							<div class='user-panel' >
								<div class='pull-left image' >";
	if ($pengawas['level'] == 'admin') {
		echo "	<img src='$homeurl/dist/img/website.png'   style='max-width:60px' alt='+'>";
	} elseif ($pengawas['level'] == 'guru') {
		if ($pengawas['fotoguru'] <> '') {
			echo "	<img src='$homeurl/foto/fotoguru/$pengawas[nama].jpg' class='img-circle'  style='border:2px solid #4169E1; max-width:60px; max-height:60px' alt='+'>";
		} else {
			echo "	<img src='$homeurl/dist/img/website.png'  style=' max-width:60px' alt='+'>";
		}
	}
	echo "
								</div>";
	if ($setting['server'] == 'pusat') {
		echo "
									<div class='pull-left info' style='left:70px; bottom:10px;'>
									<p><b><font size='5pt'>CBT-APP</font></b></p>
								</div>";
	} else {
		echo "
								<div class='pull-left info' style='left:70px; top:-10px;'>
									<p id='statusserver'></p>
								</div>";
	}
	echo "
									<script>
											$.ajax({
												type: 'POST',
												url: 'statusserver.php',
												beforeSend: function() {
													$('#loading-image').show();
												},
												success: function(response) {
													$('#statusserver').html(response);
													$('#loading-image').hide();

												}
											});
										</script>
							</div>	
						<div class='menu-header' >
							<ul style='background-color:#A9A9A9;'>
                        <li>
                            <a href='index.php' class='btn-logout'  style='color:white;'>
                                <span class='fa fa-home fa-2x'></span> Home<br>
                            </a>
                        </li>
                        <li>
							<a href='?pg=pengaturan' class='btn-logout' style='color:white;'>
                                <span class='fa fa-gear fa-2x'></span> Setting<br>
                            </a>
                        </li>
                        <li>
                            <a href='logout.php' class='btn-logout' style='color:white;'>
								<span class='fa fa-sign-out fa-2x'></span> Exit<br>
							</a>
                        </li>
                    </ul>
					
                    </div>
							<ul class=' sidebar-menu tree data-widget='tree' >
								
								<li ><a href='?'><img src='$homeurl/dist/img/icon/home.svg' width='25'> <span>Dashboard</span></a></li>
								";
	if ($pengawas['level'] == 'admin') {
		if ($setting['server'] == 'lokal') {

			echo "
						<li class=' treeview'>
							<a href='#'>
								<img src='$homeurl/dist/img/icon/loading.svg' width='25'>
								<span>Sinkron Data</span>
								<span class='pull-right-container'>
									<i class='fa fa-angle-down pull-right'></i>
								</span>
							</a>
							<ul class='treeview-menu'>
								<li><a href='?pg=sinkron'><i class='fa  fa-circle-o text-teal'></i> <span> Sinkron Pusat</span></a></li>
								<li><a href='?pg=sinkronset'><i class='fa  fa-circle-o text-teal'></i> <span> Sinkron Setting</span></a></li>

							</ul>
						</li>";
		}

		if ($setting['server'] == 'pusat') {
			echo "
									<li class=' treeview'>
										<a href='#'>
										<img src='$homeurl/dist/img/icon/interface.svg' width='25'>
										<span>Data Master</span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span>
										</a>
										
										 <ul class='treeview-menu'>
										<li><a href='?pg=importmaster'><i class='fa fa-upload'></i> <span>Import Data Master</span><span class='pull-right-container'><small class='label pull-right bg-green'>new</small></span></a></li>
										<li><a href='?pg=matapelajaran'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Data Mata Pelajaran</span></a></li>
										<li><a href='?pg=jenisujian'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Data Jenis Ujian</span></a></li>";
			if ($setting['jenjang'] == 'SMK') {
				echo "<li><a href='?pg=pk'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Data Jurusan</span></a></li>";
			}
			echo "
										<li><a href='?pg=kelas'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'><span> Data Kelas</span></a></li>
										<li><a href='?pg=ruang'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Data Ruangan</span></a></li>
										<li><a href='?pg=level'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Data Level</span></a></li>
										<li><a href='?pg=sesi'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Data Sesi</span></a></li>
										<li><a href='?pg=dataserver'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Data Server</span></a></li>
										</ul>
										</li>";
		}
		echo "
										<li class='treeview'><a href='?pg=siswa'><img src='$homeurl/dist/img/icon/men.svg' width='25'> <span>Peserta Ujian</span></a></li>
										
										<li><a href='?pg=banksoal'><img src='$homeurl/dist/img/icon/flat.svg' width='25'> <span> Bank Soal</span></a></li>
										<li><a href='?pg=jadwal'><img src='$homeurl/dist/img/icon/clock.svg' width='25'> <span> Jadwal Ujian</span></a></li>
										
										<li class='treeview'>
										<a href='#'><img src='$homeurl/dist/img/icon/multimedia.svg' width='25'><span> UBK</span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span></a>
										<ul class='treeview-menu'>
										<li><a href='?pg=status'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Status Peserta</span></a></li>
										<li><a href='?pg=reset_soal'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span>Reset Soal Blank</span></a></li>
										<li><a href='?pg=susulan'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Belum Ujian</span></a></li>
										<li><a href='?pg=daftar_pengumpulan_soal'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Daftar Soal</span></a></li>
										<!-- <li><a href='?pg=filemanager'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> File manager</span></a></li>-->
										</ul>
										</li>
										
										<li class='treeview'>
										<a href='#'><img src='$homeurl/dist/img/icon/file.svg' width='25'><span> Nilai </span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span></a>
										<ul class='treeview-menu'>
										<li><a href='?pg=nilai'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Ambil Nilai</span></a></li>
										<li><a href='?pg=nilai_xsans'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Hasil Nilai</span></a></li> 		
										<li><a href='?pg=semuanilai'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span>Semua Nilai</span></a></li>
										";
		if ($setting['server'] == 'lokal') {
			echo "
										<li><a href='?pg=kirimnilai'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span>Kirim Nilai</span></a></li> 
										";
		}
		echo "
										</ul>
										</li>
										<li class='treeview'>
										<a href='#'><img src='$homeurl/dist/img/icon/print.svg' width='25'><span> Cetak </span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span></a>
										<ul class='treeview-menu'>
										<li><a href='?pg=absen'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Daftar Hadir</span></a></li> 
										<li><a href='?pg=kartu'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Cetak Kartu</span></a></li>
										<li><a href='?pg=berita'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Berita Acara</span></a></li>
										<!-- <li><a href='?pg=laporan'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span>Cetak Format Nilai</span></a></li>-->
										</ul>
										</li>

										";
		if ($setting['server'] == 'pusat') {
			echo "
										<!--<li class='treeview'><a href='?pg=dokumentasi'><img src='../dist/img/icon/download.svg' width='25'> <span> Dokumentasi</span></a></li>-->
										<li class='treeview'><a href='?pg=pengumuman'><img src='../dist/img/icon/customer.svg' width='25'> <span> Pengumuman</span></a></li>
										";
		}
		echo "
										<li class='treeview'>
										<a href='#'><img src='$homeurl/dist/img/icon/list.svg' width='25'> <span>Manajemen User</span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span></a>
										 <ul class='treeview-menu'>
										<li><a href='?pg=pengawas'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span>Data Administrator</span></a></li>
										<li><a href='?pg=guru'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span>Data Guru</span></a></li>
										<!-- <li><a href='?pg=walikelas'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span>Wali Kelas</span></a></li>-->
										</ul>
										</li>					  
											
									";
	}
	if ($pengawas['level'] == 'guru') { //Menu Guru
		echo "
										<li class='treeview'><a href='?pg=siswa'><img src='$homeurl/dist/img/icon/men.svg' width='25'> <span>Peserta Ujian</span></a></li>
										<li ><a href='?pg=editguru'><img src='$homeurl/dist/img/icon/man.svg' width='25'> <span>Profil Saya</span></a></li>
                    <li ><a href='?pg=banksoal'><img src='$homeurl/dist/img/icon/briefcase.svg' width='25'> <span>Bank Soal</span></a></li>
										<li><a href='?pg=jadwal'><img src='$homeurl/dist/img/icon/clock.svg' width='25'> <span> Jadwal Ujian</span></a></li>
										<li class='treeview'>
										<a href='#'><img src='$homeurl/dist/img/icon/cloud-computing.svg' width='25'><span> UBK</span><span class='pull-right-container'> <i class='glyphicon glyphicon-plus pull-right'></i> </span></a>
										<ul class='treeview-menu'>
										<li><a href='?pg=status'><img src='$homeurl/dist/img/icon/electronics.svg' width='15'> <span> Status Peserta</span></a></li>
										<li><a href='?pg=reset'><img src='$homeurl/dist/img/icon/electronics-1.svg' width='15'> <span> Reset Login</span></a></li> 
										
										</ul>
										</li>
										<li><a href='?pg=nilai'><img src='../dist/img/icon/download-1.svg' width='25'> <span>Ambil Nilai</span></a></li>
										<li><a href='?pg=nilai_xsans'><img src='$homeurl/dist/img/icon/upload-1.svg' width='25'> <span>Hasil Nilai</span></a></li>
										
									";
	}
	if ($setting['jenjang'] == 'SMK') {
		$jenjang = 'SMK/SMA/MA';
	} elseif ($setting['jenjang'] == 'SMP') {
		$jenjang = 'SMP/MTS';
	} else {
		$jenjang = 'SD/MI';
	}
	echo "
							 <!--	<li><a class='btn btn-block' style='background-color:#02948d' href='logout.php'><b><img src='../dist/img/svg/cancel.svg' width='30'> Keluar</b></a></li> --!>
							</ul><!-- /.sidebar-menu -->
						</section>
					</aside>
					
					<div class='content-wrapper' style='background-image: url(backgroun.jpg);background-size: cover;'>
					<section class='content-header' style='height:102px;z-index:0;background:#1e90ff'>
								<h1 style='text-shadow: 2px 2px 4px #827e7e;color:#fff'>
								&nbsp;<span class='hidden-xs'>$setting[aplikasi] - $jenjang</span>
								
								</h1>
								<div style='float:right; margin-top:-37px'>
								
								<button class='btn  btn-flat  bg-purple'><i class='fa fa-calendar'></i> " . buat_tanggal('D, d M Y') . "</button>
								<button class='btn  btn-flat  bg-maroon' ><span id='waktu'>$waktu </span></button>
								
								</div>
								<div class='breadcrumb'>
								
								
											</div>
								</section>
						<section class='content' style='margin-top:-65px'>";
	if ($pg == '') {


		$testongoing = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM nilai WHERE ujian_mulai!='' AND ujian_selesai=''"));
		$testdone = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM nilai WHERE ujian_mulai!='' AND ujian_selesai!=''"));

		if ($siswa <> 0) {
			$testongoing_per = (1000 / $siswa) * $testongoing;
			$testongoing_per = number_format($testongoing_per, 2, '.', '');
			$testongoing_per = str_replace('.00', '', $testongoing_per);
			$testdone_per = (1000 / $siswa) * $testdone;
			$testdone_per = number_format($testdone_per, 2, '.', '');
			$testdone_per = str_replace('.00', '', $testdone_per);
		} else {
			$testongoing_per = $testdone_per = 0;
		}
		if ($pengawas['level'] == 'admin') {
			echo "
								
								<div class=' row'>
									
								<div class='col-lg-3'>
								<div class='small-box' style='background-color:#9ACD32; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$siswa</h3>
										<p>Jumlah Siswa</p>
									</div>
									<div class='icon'>
										<i class='fa fa-users'></i>
									</div>
									<a href='?pg=siswa' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							<div class='col-lg-3'>
								<div class='small-box' style='background-color:#FFA07A; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$kelas</h3>
										<p>Jumlah Kelas</p>
									</div>
									<div class='icon'>
										<i class='fa fa-institution'></i>
									</div>
									<a href='?pg=kelas' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
								<div class='col-lg-3'>
								<div class='small-box' style='background-color:#DC143C; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$ruang</h3>
										<p>Ruangan</p>
									</div>
									<div class='icon'>
										<i class='fa fa-building'></i>
									</div>
									<a href='?pg=ruang' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							<div class='col-lg-3'>
								<div class='small-box' style='background-color:#B8860B; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$pk</h3>
										<p>Jurusan</p>
									</div>
									<div class='icon'>
										<i class='fa fa-mortar-board'></i>
									</div>
									<a href='?pg=pk' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							
							<div class='col-lg-3 '>
								<div class='small-box' style='background-color:#FF1493; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$nilai</h3>
										<p>Nilai Masuk</p>
									</div>
									<div class='icon'>
										<i class='fa fa-envelope'></i>
									</div>
									<a href='?pg=nilai' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							<div class='col-lg-3'>
								<div class='small-box' style='background-color:#FFD700; border-radius:1px 25px;'>
									<div class='inner'>
										<h3> $soal</h3>
										<p>Jumlah Soal</p>
									</div>
									<div class='icon'>
										<i class='fa fa-file-pdf-o'></i>
									</div>
									<a href='?pg=banksoal' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							
							<div class='col-lg-3'>
								<div class='small-box' style='background-color:#ADFF2F; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$draf_nilai</h3>
										<p>Draft Nilai</p>
									</div>
									<div class='icon'>
										<i class='fa fa-cloud'></i>
									</div>
									<a href='?pg=nilai_xsans' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>

							<div class='col-lg-3'>
								<div class='small-box' style='background-color:#FF6347; border-radius:1px 25px;'>
									<div class='inner'>
										<h3>$online</h3>
										<p>Aktifitas Siswa</p>
									</div>
									<div class='icon'>
										<i class='fa fa-user'></i>
									</div>
									<a href='#' class='small-box-footer' style='border-radius:1px 25px;'>More info <i class='fa fa-arrow-circle-right'></i></a>
								</div>
							</div>
							
							<div class='animated flipInX col-md-8'>
							<div class='row'>
							<div class='col-md-12'>
									<div class='box box-solid direct-chat direct-chat-warning'>
											<div class='box-header with-border' style='background-color:#A9A9A9;'>
												<h3 class='box-title' ><i class='fa fa-bullhorn'></i> Pengumuman
												</h3>
											<div class='box-tools pull-right'>
													<a href='?pg=$pg&ac=clearpengumuman' class='btn btn-sm bg-dark-gray' title='Bersihkan Pengumuman' style='color:white;'><i class='fa fa-trash'></i></a>
										</div>
										</div><!-- /.box-header -->
										<div class='box-body'>
											
											<div id='pengumuman'>
													<p class='text-center'>
														<br/><i class='fa fa-spin fa-circle-o-notch'></i> Loading....
													</p>
											</div>
										</div><!-- /.box-body -->
									</div><!-- /.box -->
							</div>
							</div>
							</div>
									
										
									<div class='animated flipInX col-md-4'>
										<div class='box box-solid direct-chat direct-chat-warning'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'><i class='fa fa-history'></i> Log Aktifitas</h3>
												<div class='box-tools pull-right'>
													<a href='?pg=$pg&ac=clearlog' class='btn btn-sm btn-flat bg-dark-gray'' title='Bersihkan Log' style='color:white;'><i class='fa fa-trash'></i></a>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<div id='log-list'>
													<p class='text-center'>
														<br/><i class='fa fa-spin fa-circle-o-notch'></i> Loading....
													</p>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>

							";
			if ($ac == 'clearlog') {
				mysqli_query($koneksi, "TRUNCATE log");
				jump("index.php");
			}
			if ($ac == 'clearpengumuman') {
				mysqli_query($koneksi, "TRUNCATE pengumuman");
				jump("index.php");
			}
		}
		if ($pengawas['level'] == 'guru') {
			echo "
								
								<div class='row'>	
								<div class='col-md-8'>
										<div class='box box-solid direct-chat direct-chat-warning' >
											<div class='box-header with-border' style='background-color:#A9A9A9' >
												<h3 class='box-title'><i class='fa fa-bullhorn'></i> Pengumuman
												</h3>
												<div class='box-tools pull-right'>
													
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<div id='pengumuman'>
													<p class='text-center'>
														<br/><i class='fa fa-spin fa-circle-o-notch'></i> Loading....
													</p>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									<div class='col-md-4'>
										<div class='box box-solid '>
											
											<div class='box-body'>
												<strong><i class='fa fa-building-o'></i> $setting[sekolah]</strong><br/>
												$setting[alamat]<br/><br/>
												<strong><i class='fa fa-phone'></i> Telepon</strong><br/>
												$setting[telp]<br/><br/>
												<strong><i class='fa fa-fax'></i> Fax</strong><br/>
												$setting[fax]<br/><br/>
												<strong><i class='fa fa-globe'></i> Website</strong><br/>
												$setting[web]<br/><br/>
												<strong><i class='fa fa-at'></i> E-mail</strong><br/>
												$setting[email]<br/>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
	
								</div>
							";
		}
	} elseif ($pg == 'dataserver') {
		include 'serverlokal.php';
	} elseif ($pg == 'filemanager') {

		echo "
							<iframe  width='100%' height='550' frameborder='0'
								src='ifm.php'>
							</iframe>
							";
	}
	// mata pelajaran
	elseif ($pg == 'matapelajaran') {
		cek_session_admin();
		$pesan = '';
		if (isset($_POST['simpanmapel'])) {
			$kode = str_replace(' ', '', $_POST['kodemapel']);
			$nama = addslashes($_POST['namamapel']);
			$cek = mysqli_num_rows(mysqli_query($koneksi, "select * from mata_pelajaran where kode_mapel='$kode'"));
			if ($cek == 0) {
				$exec = mysqli_query($koneksi, "INSERT INTO mata_pelajaran (kode_mapel,nama_mapel)value('$kode','$nama')");
				$pesan = "<div class='alert alert-success alert-dismissible'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
										<i class='icon fa fa-info'></i>
										Data Berhasil ditambahkan ..
										</div>";
			} else {
				$pesan = "<div class='alert alert-warning alert-dismissible'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
										<i class='icon fa fa-info'></i>
										Maaf Kode Mapel Sudah ada !
										</div>";
			}
		}
		if (isset($_POST['importmapel'])) {
			$file = $_FILES['file']['name'];
			$temp = $_FILES['file']['tmp_name'];
			$ext = explode('.', $file);
			$ext = end($ext);
			if ($ext <> 'xls') {
				$info = info('Gunakan file Ms. Excel 93-2007 Workbook (.xls)', 'NO');
			} else {

				$data = new Spreadsheet_Excel_Reader($temp);
				$hasildata = $data->rowcount($sheet_index = 0);
				$sukses = $gagal = 0;
				for ($i = 2; $i <= $hasildata; $i++) {

					$kode = addslashes($data->val($i, 2));
					$nama = addslashes($data->val($i, 3));
					$kode = str_replace(' ', '', $kode);
					$nama = addslashes($nama);
					$cek = mysqli_num_rows(mysqli_query($koneksi, "select * from mata_pelajaran where kode_mapel='$kode'"));
					if ($kode <> '' and $nama <> '') {
						if ($cek == 0) {
							$exec = mysqli_query($koneksi, "INSERT INTO mata_pelajaran (kode_mapel,nama_mapel) VALUES ('$kode','$nama')");
							($exec) ? $sukses++ : $gagal++;
						}
					} else {
						$gagal++;
					}
				}
				$total = $hasildata - 1;
				$info = info("Berhasil: $sukses | Gagal: $gagal | Dari: $total", 'OK');
			}
		}
		echo "
								<div class='row'><div class='col-md-12'>$pesan</div>
									<div class='col-md-12'>
										<div class='box box-solid'>
											 <div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Mata Pelajaran</h3>
												<div class='box-tools pull-right btn-group'>
													<button class='btn btn-sm btn-flat btn-info' data-toggle='modal' data-target='#tambahmapel'><i class='fa fa-check'></i> Tambah Mapel</button>
													<button class='btn btn-sm btn-flat btn-primary' data-toggle='modal' data-target='#importmapel'><i class='fa fa-upload'></i> Import Mapel</button>
												</div>
									
											</div><!-- /.box-header -->

											<div class='box-body'>
											<div class='table-responsive'>
												<table id='tablemapel' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>Kode Mapel</th>
															<th>Mata Pelajaran</th>
															
														</tr>
													</thead>
													<tbody>";

		$mapelQ = mysqli_query($koneksi, "SELECT * FROM mata_pelajaran ORDER BY nama_mapel ASC");

		while ($mapel = mysqli_fetch_array($mapelQ)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$mapel[kode_mapel]</td>
																<td>$mapel[nama_mapel]</td>
																
																
															</tr>";
		}
		echo "
													</tbody>
												</table>
												</div>
												
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
															<div class='modal fade' id='tambahmapel' style='display: none;'>
															<div class='modal-dialog'>
															<div class='modal-content'>
															<div class='modal-header' style='background-color:#A9A9A9'>
															<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Tambah Mata Pelajaran</h3>
															</div>
															<div class='modal-body'>
															<form action='' method='post'>
															<div class='form-group'>
																<label>Kode Mapel</label>
																<input type='text' name='kodemapel' class='form-control'  required='true'/>
															</div>
															<div class='form-group'>
																<label>Nama Pelajaran</label>
																<input type='text' name='namamapel'  class='form-control' required='true'/>
															</div>
															<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='simpanmapel' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
															</div>
															</form>
															</div>
								
															</div>
															<!-- /.modal-content -->
															</div>
															<!-- /.modal-dialog -->
															</div>
															
															<div class='modal fade' id='importmapel' style='display: none;'>
															<div class='modal-dialog'>
															<div class='modal-content'>
															<div class='modal-header' style='background-color:#A9A9A9'>
															<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Tambah Mata Pelajaran</h3>
															</div>
															<div class='modal-body'>
															<form action='' method='post' enctype='multipart/form-data'>
															<div class='form-group'>
																<label>Pilih File</label>
																<input type='file' name='file' class='form-control' required='true'/>
															</div>
															<p>
																Sebelum meng-import pastikan file yang akan anda import sudah dalam bentuk Ms. Excel 97-2003 Workbook (.xls) dan format penulisan harus sesuai dengan yang telah ditentukan. <br/>
															</p>
														
															<a href='importdatamapel.xls'><i class='fa fa-file-excel-o'></i> Download Format</a>
													   
															<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='importmapel' class='btn btn-sm btn-flat btn-success'><i class='fa fa-upload'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
															</div>
															</form>
															</div>
								
															</div>
															<!-- /.modal-content -->
															</div>
															<!-- /.modal-dialog -->
															</div>
								
						";
	}

	//Membuat token
	elseif ($pg == 'token') {


		if (isset($_POST['generate'])) {
			function create_random($length)
			{
				$data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$string = '';
				for ($i = 0; $i < $length; $i++) {
					$pos = rand(0, strlen($data) - 1);
					$string .= $data{
						$pos};
				}
				return $string;
			}
			$token = create_random(6);
			$now = date('Y-m-d H:i:s');
			$cek = mysqli_num_rows(mysqli_query($koneksi, "select * from token"));
			if ($cek <> 0) {
				$query = mysqli_fetch_array(mysqli_query($koneksi, "select time from token"));
				$time = $query['time'];
				$tgl = buat_tanggal('H:i:s', $time);
				$exec = mysqli_query($koneksi, "update token set token='$token', time='$now' where  id_token='1'");
			} else {
				$exec = mysqli_query($koneksi, "INSERT INTO token (token,masa_berlaku) VALUES ('$token','00:15:00')");
			}
		}
		$token = mysqli_fetch_array(mysqli_query($koneksi, "select token from token"));
		echo "
						<div class='row'>
						<form action='' method='post'>
						<div class='col-md-6'>
						<div class='box box-solid'>
							<div class='box-header with-border' >
								<h3 class='box-title'> Generate</h3>
								<div class='box-tools pull-right'>
													
								</div>
							</div><!-- /.box-header -->
						<div class='box-body'>
											 
						<div class='col-xs-12'>
							
                            <div class='small-box bg-aqua'>
                                <div class='inner'>
									
                                    <h3><span name='token' id='isi_token'>$token[token]</span></h3>
                                    <p>Token Tes</p>
                                </div>
                                <div class='icon'>
                                    <i class='fa fa-barcode'></i>
                                </div>
                            </div>   
                           
							<button name='generate' class='btn btn-block btn-flat bg-maroon'>Generate</button>
                        </div>
						</div><!-- /.box-body -->
						</div><!-- /.box -->
						</div>
						</form>
									<div class='col-md-6'>
										<div class='box box-solid'>
											<div class='box-header with-border'>
												<h3 class='box-title'> Data Token</h3>
												
											</div><!-- /.box-header -->
											<div class='box-body'>
											<div class='table-responsive'>
												<table  class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'></th>
															<th>Token</th>
															<th>Waktu Generate</th>
															<th>Masa Berlaku</th>
															
															
														</tr>
													</thead>
													<tbody>";
		$tokenku = mysqli_query($koneksi, "SELECT * FROM token ");
		while ($token = mysqli_fetch_array($tokenku)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$token[token]</td>
																<td>$token[time]</td>
																<td>$token[masa_berlaku]</td>
																
																
															</tr>
															
														";
		}
		echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
										</div>				
										
						</div>";
	} elseif ($pg == 'pengumuman') {
		cek_session_admin();
		if (isset($_POST['simpanpengumuman'])) {
			$exec = mysqli_query($koneksi, "INSERT INTO pengumuman (judul,text,user,type) VALUES ('$_POST[judul]','$_POST[pengumuman]','$pengawas[id_pengawas]','$_POST[tipe]')");
			if (!$exec) {
				$info = info("Gagal menyimpan!", "NO");
			} else {
				jump("?pg=$pg");
			}
		}
		echo "
								<div class='row'>
								<form action='' method='post'>
										<div class='col-md-6'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'> Tulis Pengumuman</h3>
												<div class='box-tools pull-right'>
													<button type='submit' name='simpanpengumuman' class='btn btn-sm btn-flat btn-success' ><i class='fa fa-pencil-square-o'></i> Simpan</button>
													<a href='?pg=$pg' class='btn btn-sm bg-maroon' ><i class='fa fa-times'></i></a>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<div class='col-sm-12'>
												<div class='form-group'>
												<label >Judul </label>
												<input type='text' class='form-control' name='judul' placeholder='Judul' required>
												</div>
												</div>
												<div class='col-sm-12'>
												<div class='form-group'>
												<label >Jenis Pengumuman </label><br>
												<input type='radio' name='tipe' value='internal' checked> <span class='text-green'><b>guru</b></span> &nbsp; &nbsp;&nbsp;<input type='radio' name='tipe' value='eksternal'> <span class='text-blue'><b>siswa</b></span>
												</div>
												</div>
												<div class='col-sm-12'>
												<div class='form-group'>
												<label >Informasi Pengumuman </label>
												<textarea id='txtpengumuman' name='pengumuman' class='form-control'></textarea>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
										</div>
										</form>
										
										
								</div>
								<div class='col-md-6'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'> Pengumuman</h3>
												
											</div><!-- /.box-header -->
											<div class='box-body'>
											<div class='table-responsive'>
												<table id='example1' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'></th>
															<th>Pengumuman</th>
															
															<th>Untuk</th>
															
															<th width=60px></th>
														</tr>
													</thead>
													<tbody>";
		$pengumumanq = mysqli_query($koneksi, "SELECT * FROM pengumuman ORDER BY date desc");
		while ($pengumuman = mysqli_fetch_array($pengumumanq)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$pengumuman[judul]</td>
																
																<td>";
			if ($pengumuman['type'] == 'eksternal') {
				echo "<small class='label bg-blue'>siswa</label>";
			} else {
				echo "<small class='label bg-green'>guru</label>";
			}
			echo "</td>
																
																<td align='center'>
																<div class='btn-group'>
																	<!--<a href='?pg=$pg&ac=edit&id=$pengumuman[id_pengumuman]'> <button class='btn btn-flat btn-xs btn-warning'><i class='fa fa-pencil-square-o'></i></button></a>-->
																	<a><button class='btn bg-maroon btn-flat btn-xs' data-toggle='modal' data-target='#hapus$pengumuman[id_pengumuman]'><i class='fa fa-trash-o'></i></button></a>
																</div>
																</td>
															</tr>
															
														";
			$info = info("Anda yakin akan menghapus pengumuman ini  ?");
			if (isset($_POST['hapus'])) {
				$exec = mysqli_query($koneksi, "DELETE  FROM pengumuman WHERE id_pengumuman = '$_REQUEST[idu]'");
				(!$exec) ? info("Gagal menyimpan", "NO") : jump("?pg=$pg");
			}
			echo "
													<div class='modal fade' id='hapus$pengumuman[id_pengumuman]' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header bg-maroon'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Hapus Pengumuman</h3>
															</div>
													<div class='modal-body'>
													<form action='' method='post'>
													<input type='hidden' id='idu' name='idu' value='$pengumuman[id_pengumuman]'/>
													<div class='callout '>
															<h4>$info</h4>
													</div>
													<div class='modal-footer'>
													<div class='box-tools pull-right btn-group'>
																<button type='submit' name='hapus' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Hapus</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
													</div>	
													</div>
													</form>
													</div>
								
													</div>
														<!-- /.modal-content -->
													</div>
														<!-- /.modal-dialog -->
													</div>
														";
		}
		echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
										</div>
									<script>
									tinymce.init({
										selector: '#txtpengumuman',
										plugins: [
											'advlist autolink lists link image charmap print preview hr anchor pagebreak',
											'searchreplace wordcount visualblocks visualchars code fullscreen',
											'insertdatetime media nonbreaking save table contextmenu directionality',
											'emoticons template paste textcolor colorpicker textpattern imagetools uploadimage paste'
										],
										
										toolbar: 'bold italic fontselect fontsizeselect | alignleft aligncenter alignright bullist numlist  backcolor forecolor | emoticons code | imagetools link image paste ',
										fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
										paste_data_images: true,
										paste_as_text: true,
										images_upload_handler: function (blobInfo, success, failure) {
											success('data:' + blobInfo.blob().type + ';base64,' + blobInfo.base64());
										},
										image_class_list: [
										{title: 'Responsive', value: 'img-responsive'}
										],									
										});
									</script>";
	} elseif ($pg == 'guru') {
		cek_session_admin();
		echo "
								<div class='row'>
									<div class='col-md-8'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Manajemen Guru</h3>
												<div class='box-tools pull-right btn-group'>
													<a href='?pg=importguru' class='btn btn-sm btn-flat btn-success'><i class='fa fa-upload'></i> Import Guru</a>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
											<div class='table-responsive'>
												<table id='example1' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>NIP</th>
															<th>Nama</th>
															<th>Username</th>
															<th>Password</th>
															<th>Level</th>
															<th width=60px></th>
														</tr>
													</thead>
													<tbody>";
		$guruku = mysqli_query($koneksi, "SELECT * FROM pengawas where level='guru'  ORDER BY nama ASC");
		while ($pengawas = mysqli_fetch_array($guruku)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$pengawas[nip]</td>
																<td>$pengawas[nama]</td>
																<td><small class='label bg-purple'>$pengawas[username]</small></td>
																<td><small class='label bg-blue'>$pengawas[password]</small></td>
																<td>$pengawas[level]</td>
																<td align='center'>
																<div class='btn-group'>
																	<a href='?pg=$pg&ac=edit&id=$pengawas[id_pengawas]'> <button class='btn btn-flat btn-xs btn-warning'><i class='fa fa-pencil-square-o'></i></button></a>
																	<a href='?pg=$pg&ac=hapus&id=$pengawas[id_pengawas]'> <button class='btn btn-flat btn-xs bg-maroon'><i class='fa fa-trash-o'></i></button></a>
																</div>
																</td>
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									<div class='col-md-4'>";
		if ($ac == '') {
			if (isset($_POST['submit'])) {
				$nip = $_POST['nip'];
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				$username = $_POST['username'];
				$pass1 = $_POST['pass1'];
				$pass2 = $_POST['pass2'];

				$cekuser = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE username='$username'"));
				if ($cekuser > 0) {
					$info = info("Username $username sudah ada!", "NO");
				} else {
					if ($pass1 <> $pass2) {
						$info = info("Password tidak cocok!", "NO");
					} else {
						$password = $pass1;
						$exec = mysqli_query($koneksi, "INSERT INTO pengawas (nip,nama,username,password,level) VALUES ('$nip','$nama','$username','$password','guru')");
						(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
					}
				}
			}
			echo "
												<form action='' method='post'>
													<div class='box box-solid'>
														<div class='box-header with-border' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Tambah</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
															<div class='form-group'>
																<label>NIP</label>
																<input type='text' name='nip' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Nama</label>
																<input type='text' name='nama' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Username</label>
																<input type='text' name='username' class='form-control' required='true'/>
															</div>
															
															<div class='form-group'>
																<div class='row'>
																	<div class='col-md-6'>
																		<label>Password</label>
																		<input type='password' name='pass1' class='form-control' required='true'/>
																	</div>
																	<div class='col-md-6'>
																		<label>Ulang Password</label>
																		<input type='password' name='pass2' class='form-control' required='true'/>
																	</div>
																</div>
															</div>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
		if ($ac == 'edit') {
			$id = $_GET['id'];
			$value = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE id_pengawas='$id'"));
			if (isset($_POST['submit'])) {
				$nip = $_POST['nip'];
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				$username = $_POST['username'];
				$pass1 = $_POST['pass1'];
				$pass2 = $_POST['pass2'];
				if ($pass1 <> '' and $pass2 <> '') {
					if ($pass1 <> $pass2) {
						$info = info("Password tidak cocok!", "NO");
					} else {
						$password = password_hash($pass1, PASSWORD_BCRYPT);
						$exec = mysqli_query($koneksi, "UPDATE pengawas SET nip='$nip',nama='$nama',username='$username',password='$password',level='guru' WHERE id_pengawas='$id'");
					}
				} else {
					$exec = mysqli_query($koneksi, "UPDATE pengawas SET nip='$nip',nama='$nama',username='$username',level='guru' WHERE id_pengawas='$id'");
				}
				(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
			}
			echo "
										<form action='' method='post'>
									<div class='box box-solid'>
										<div class='box-header with-border'>
											<h3 class='box-title'>Edit</h3>
											<div class='box-tools pull-right '>
												<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
												<a href='?pg=<?= $pg ?>' class='btn btn-sm bg-maroon' title='Batal'><i class='fa fa-times'></i></a>
											</div>
										</div><!-- /.box-header -->
										<div class='box-body'>
											$info
											<div class='form-group'>
												<label>NIP</label>
												<input type='text' name='nip' value='$value[nip]' class='form-control' required='true' />
											</div>
											<div class='form-group'>
												<label>Nama</label>
												<input type='text' name='nama' value='$value[nama]' class='form-control' required='true' />
											</div>
											<div class='form-group'>
												<label>Username</label>
												<input type='text' name='username' value='$value[username]' class='form-control' required='true' />
											</div>
											<div class='form-group'>
												<div class='row'>
													<div class='col-md-6'>
														<label>Password</label>
														<input type='password' name='pass1' class='form-control' />
													</div>
													<div class='col-md-6'>
														<label>Ulang Password</label>
														<input type='password' name='pass2' class='form-control' />
													</div>
												</div>
											</div>
										</div><!-- /.box-body -->
									</div><!-- /.box -->
								</form>
											";
		}
		if ($ac == 'hapus') {
			$id = $_GET['id'];
			$info = info("Anda yakin akan menghapus pengawas ini?");
			if (isset($_POST['submit'])) {
				$exec = mysqli_query($koneksi, "DELETE FROM pengawas WHERE id_pengawas='$id'");
				(!$exec) ? $info = info("Gagal menghapus!", "NO") : jump("?pg=$pg");
			}
			echo "
												<form action='' method='post'>
													<div class='box box-danger'>
														<div class='box-header with-border'>
															<h3 class='box-title'>Hapus</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Hapus</button>
																<a href='?pg=$pg' class='btn btn-sm btn-default' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
		echo "
									</div>
								</div>
							";
	} elseif ($pg == 'beritaacara') {
		if ($pengawas['level'] == 'admin') {
			$idberita = $_GET['id'];
			$sqlx = mysqli_query($koneksi, "select * from berita a left join mapel b ON a.id_mapel=b.id_mapel left join mata_pelajaran c ON b.nama=c.kode_mapel where a.id_berita='$idberita'");
			$ujian = mysqli_fetch_array($sqlx);
			$kodeujian = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM jenis WHERE id_jenis='$ujian[jenis]'"));
			$nm_ujian = mysqli_query($koneksi, "SELECT * FROM ujian");
			$tampil = mysqli_fetch_array($nm_ujian);
			$jmlsemua = $ujian['ikut'] + $ujian['susulan'];
			$hari = buat_tanggal('D', $ujian['tgl_ujian']);
			$tanggal = buat_tanggal('d', $ujian['tgl_ujian']);
			$bulan = buat_tanggal('m', $ujian['tgl_ujian']);
			$tahun = buat_tanggal('Y', $ujian['tgl_ujian']);
			if (date('m') >= 7 and date('m') <= 12) {
				$ajaran = date('Y') . "/" . (date('Y') + 1);
			} elseif (date('m') >= 1 and date('m') <= 6) {
				$ajaran = (date('Y') - 1) . "/" . date('Y');
			}
			echo "
						<div class='row'>
						<div class='col-md-12' >
						<div class='box box-solid' >
						<div class='box-header' style='background-color:#A9A9A9'>
						<h3 class='box-title'>Preview Berita Acara</h3>
						<div class='box-tools pull-right btn-group'>
						<button  onclick=frames['printberita'].print() class='btn btn-sm btn-flat btn-success'><i class='fa fa-print'></i> Print</button>
						<iframe name='printberita' src='beritaacara.php?id=$idberita' style='border:none;width:1px;height:1px;'></iframe>
						</div>
						</div>
						<div class='box-body'  style='background:#c3c3c3;  height:1275px;'>
						<div class='table-responsive'>
						<div style='background:#fff; width:80%; margin:0 auto; padding:35px; height:90%;'>
						<table border='0' width='100%'>
						<tr>
						<td rowspan='4' width='120' align='center'><img src='$homeurl/$setting[foto]' width='80'></td>
						<td colspan='2'  align='center'><font size='+1'><b>BERITA ACARA PELAKSANAAN</b></font></td>
						<td rowspan='7' width='120' align='center'><img src='$homeurl/$setting[logo]' width='80'></td>
						</tr>
						 <tr>
						<td colspan='2' align='center'><font size='+1'><b>" . strtoupper($kodeujian['nama']) . "</b></font></td>
						</tr>
						<tr>
						<td colspan='2' align='center'><font size='+1'><b>TAHUN PELAJARAN $ajaran</b></font></td>
						</tr>  
						</table>
						<br>
						<table border='0' width='95%' align='center' >
						<tr height='30'>
						<td height='30' colspan='4' style='text-align: justify;'>Pada hari ini <b> $hari </b>  tanggal <b>$tanggal</b> bulan <b>$bulan</b> tahun <b>$tahun</b>
						, di $setting[sekolah] telah diselenggarakan " . ucwords(strtolower($tampil['kode_ujian'])) . " untuk Mata Pelajaran <b>$ujian[nama_mapel]</b> dari pukul <b>$ujian[mulai]</b> sampai dengan pukul <b>$ujian[selesai]</b></td>
						</tr>
						</table>
						<table border='0' width='95%' align='center'>
						<tr height='30'>
						<td height='30' width='5%'>1.</td>
						<td height='30' width='30%'>Kode Sekolah</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$setting[kode_sekolah]</td>
						</tr>
						<tr height='30'>
						<td height='30' width='10px'></td>
						<td height='30'>Sekolah/Madrasah</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$setting[sekolah]</td>  
						</tr>
						<tr height='30'>
						<td height='30' width='5%'>.</td>
						<td height='30' width='30%'>Sesi</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$ujian[sesi]</td>
						</tr>
						<tr height='30'>
						<td height='30' width='5%'>.</td>
						<td height='30' width='30%'>Ruang</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$ujian[ruang]</td>
						</tr>
						<tr height='30'>
						<td height='30' width='10px'></td>
						<td height='30'>Jumlah Peserta Seharusnya</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$jmlsemua</td>  
						</tr>
						<tr height='30'>
						<td height='30' width='5%'></td>
						<td height='30' width='30%'>Jumlah Hadir (Ikut Ujian)</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$ujian[ikut]</td>
						</tr>
						<tr height='30'>
						<td height='30' width='10px'></td>
						<td height='30'>Jumlah Tidak Hadir</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>$ujian[susulan]</td>  
						</tr>
						<tr height='30'>
						<td height='30' width='10px'></td>
						<td height='30'>Nomer Peserta</td>
						<td height='30' width='60%' style='border-bottom:thin solid #000000'>";
			$dataArray = unserialize($ujian['no_susulan']);
			foreach ($dataArray as $key => $value) {
				echo "<small class='label label-success'>$value </small>&nbsp;";
			}
			echo "
						</td>  
						</tr>
						<tr height='30'>
						<td height='30' width='10px'></td></tr>    
						<tr height='30'>
						<td height='30' width='5%'>2.</td>
						<td colspan='2' height='30' width='30%'>Catatan selama " . ucwords(strtolower($setting['nama_ujian'])) . " </td>
						</tr>
						<tr height='120px'>
						<td height='30' width='5%'></td>
						<td colspan='2' style='border:solid 1px black'>&nbsp;&nbsp;&nbsp;$ujian[catatan]</td></tr>
   
						<tr height='30'>
						<td height='30' colspan='2' width='5%'>Yang membuat berita acara : </td></tr>
						</table>
						<table border='0' width='80%' style='margin-left:50px'>  
						<tr><td colspan='4' ></td>
						<td height='30' width='30%'>TTD</td>
						<tr><td width='10%'>1. </td><td width='20%'>Proktor</td><td width='30%' style='border-bottom:thin solid #000000'>$ujian[nama_proktor]</td>
						<td height='30' width='5%'></td><td height='30' width='35%'></td>
						</tr>
						<tr><td width='10%'>   </td><td width='20%'>NIP. </td><td width='30%' style='border-bottom:thin solid #000000'>$ujian[nip_proktor]</td>
						<td height='30' width='5%'></td><td height='30' width='35%' style='border-bottom:thin solid #000000'>  1. </td>
						</tr>
						<tr><td colspan='4' ></td>
						
						<tr><td width='10%'>2. </td><td width='20%'>Pengawas</td><td width='30%' style='border-bottom:thin solid #000000'>$ujian[nama_pengawas]</td>
						<td height='30' width='5%'></td><td height='30' width='35%'></td>
						</tr>
						<tr><td width='10%'>   </td><td width='20%'>NIP. </td><td width='30%' style='border-bottom:thin solid #000000'>$ujian[nip_pengawas]</td>
						<td height='30' width='5%'></td><td height='30' width='35%' style='border-bottom:thin solid #000000'>  2. </td>
						</tr>
						<tr><td colspan='4' ></td>
						
						<tr><td width='10%'>3. </td><td width='20%'>Kepala Sekolah</td><td width='30%' style='border-bottom:thin solid #000000'>$setting[kepsek]</td>
						<td height='30' width='5%'></td><td height='30' width='35%'></td>
						</tr>
						<tr><td width='10%'>   </td><td width='20%'>NIP. </td><td width='30%' style='border-bottom:thin solid #000000'>$setting[nip]</td>
						<td height='30' width='5%'></td><td height='30' width='35%' style='border-bottom:thin solid #000000'>  3. </td>
						</tr>
						</table><br><br><br><br><br>
						
						<table width='100%' height='30'>
						<tbody><tr>
						<td width='25px' style='border:1px solid black'></td>
						<td width='5px'>&nbsp;</td>
						<td style='border:1px solid black;font-weight:bold;font-size:14px;text-align:center;'>$kodeujian[nama]</td>
						<td width='5px'>&nbsp;</td>
						<td width='25px' style='border:1px solid black'></td>
						</tr>
						</tbody>
						</table>
						</div>
						</div>
						</div>
						</div>
						</div>
						</div>
						
						";
		}
	}

	// jadwal ujian
	elseif ($pg == 'jadwal') {
		include "jadwal_ujian.php";
	} elseif ($pg == 'berita') {
		include 'berita_acara/berita_acara.php';
	} elseif ($pg == 'nilai') {
		include 'nilai.php';
	} elseif ($pg == 'nilai_xsans') {
		include 'move_nilai/nilai_xsans.php';
	} elseif ($pg == 'reset_soal') {
		include 'reset/reset_soal.php';
	} elseif ($pg == 'walikelas') {
		include 'walikelas.php';
	} elseif ($pg == 'semuanilai') {
		include 'semuanilai.php';
	} elseif ($pg == 'leger') {
		include 'leger.php';
	} elseif ($pg == 'dataserver') {
		include 'serverlokal.php';
	} elseif ($pg == 'sinkron') {
		include 'sinkronisasi.php';
	} elseif ($pg == 'sinkronset') {
		include 'sinkronsetting.php';
	} elseif ($pg == 'kirimnilai') {
		include 'kirimnilai.php';
	} elseif ($pg == 'daftar_pengumpulan_soal') {
		include 'daftar_pengumpulan_soal.php';
	} elseif ($pg == 'informasi') {
		include 'info/informasi.php';
	} elseif ($pg == 'susulan') {
		echo "
					<div class='row'>
						<div class='col-md-12'>
							<div class='box box-solid'>
								<div class='box-header with-border ' style='background-color:#A9A9A9'>
									<h3 class='box-title'><i class='fa fa-file'></i> Daftar Siswa Susulan</h3>
									<div class='box-tools pull-right '>
									</div>
								</div>
								
								<div class='box-body'>
						<div class='alert alert-warning'>	 
							Jangan Hapus Berita Acara jika ada siswa yg tidak hadir pada berita acara mapel tersebut<br><i style='color: black'>Jika dihapus maka data siswa yg tidak hadir(SUSULAN) akan Hilang!!! @wistek</i>
						</div>
									<div id='tableberita' class='table-responsive'>
										<table class='table table-bordered table-striped  table-hover'>
											<thead>
												<tr>
													<th width='5px'>#</th>
													<th>No Peserta</th>
													<th>Nama Siswa</th>
													<th>Mata Ujian</th>
												</tr>
											</thead>
											<tbody>";

		$beritaQ = mysqli_query($koneksi, "SELECT * FROM berita WHERE no_susulan <> ''");

		while ($berita = mysqli_fetch_array($beritaQ)) {

			$mapel = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM mapel a LEFT JOIN mata_pelajaran b ON a.nama=b.kode_mapel WHERE a.id_mapel='$berita[id_mapel]'"));




			if ($berita['no_susulan'] <> "") {
				$dataArray = unserialize($berita['no_susulan']);
				foreach ($dataArray as $key => $value) {

					$siswaQ = mysqli_query($koneksi, "select * from siswa where no_peserta='$value'");

					while ($siswa = mysqli_fetch_array($siswaQ)) {

						$cek = mysqli_num_rows(mysqli_query($koneksi, "select * from nilai where id_mapel='$berita[id_mapel]' and id_siswa='$siswa[id_siswa]'"));

						if ($cek == 0) {
							$no++;
							echo "
																	<tr>
																		<td>$no</td>
																		<td>$siswa[no_peserta]</td>
																		<td>$siswa[nama]</td>
																		<td>$mapel[nama_mapel]</td>
																	</tr>";
						}
					}
				}
			}
		}
		echo "
											</tbody>
										</table>
									</div>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
						</div>
					</div>";
	} elseif ($pg == 'status') {
		if ($ac == '') {


			echo "
									<div class='row'>
										<div class='col-md-12'>
										<div class='alert alert-warning alert-dismissible'>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Status peserta akan muncul saat ujian berlangsung ..
													</div>
											<div class='box box-solid'>
												<div class='box-header with-border' style='background-color:#A9A9A9'>
													<h3 class='box-title'><i class='icon fa fa-hourglass-1'>    Status Peserta </i></h3>
													<div class='box-tools pull-right btn-group'>
														
													</div>
												</div><!-- /.box-header -->
												<div class='box-body'>
												<div class='table-responsive'>
													<table id='tablestatus' class='table table-bordered table-striped'>
														<thead>
															<tr>
																<th width='5px'>#</th>
																<th>NIS</th>
																<th>Nama</th>
																<th>Kelas</th>
																<th>Mapel</th>
																<th>Lama Ujian</th>
																<th>Jawaban</th>
																<th>Nilai</th>
																<th>Ip Address</th>
																<th >Status</th>
																<th >Aksi</th>
																
															</tr>
														</thead>
														<tbody id='divstatus'>
														</tbody>
													</table>
													</div>
												</div><!-- /.box-body -->
											</div><!-- /.box -->
										</div>
									</div>
								";
		}
	} elseif ($pg == 'dokumentasi') {
		if ($ac == '') {
			$pesan = '';
			if (isset($_POST['tambah'])) {
				$allowed_ext	= array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf', 'rar', 'zip', 'apk');
				$file_name		= $_FILES['file']['name'];
				$file_ext		= strtolower(end(explode('.', $file_name)));
				$file_size		= $_FILES['file']['size'];
				$file_tmp		= $_FILES['file']['tmp_name'];

				$nama			= $_POST['nama_file'];
				$tgl			= date("Y-m-d");

				if (in_array($file_ext, $allowed_ext) === true) {
					if ($file_size < 20 * 1048576) {
						$lokasi = '../berkas/' . $nama . '.' . $file_ext;
						$namafile = $nama . '.' . $file_ext;
						move_uploaded_file($file_tmp, $lokasi);
						$in = mysqli_query($koneksi, "INSERT INTO dokumentasi (nama_file,tipe_file,ukuran_file,file,tgl_upload) VALUES ('$nama','$file_ext','$file_size','$namafile','$tgl')");
					}
				}
				(!$in) ? $info = info("Gagal Upload!", "NO") : jump("?pg=$pg");
			}
			echo "
				<div class='row'>
						<div class='col-md-12'>
							<div class='box box-solid'>
								<div class='box-header ' style='background-color:#A9A9A9'>
									<h3 class='box-title'><i class='fa fa-download'>  Daftar Berkas</i></b></h3>
									<div class='box-title pull-right'>
										<button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#tambahfile'>Tambah</button>
									</div>
								</div>
								<div class='box-body'>

									<form method='POST' class='form-horizontal' action='' enctype='multipart/form-data'>

										<div class='table-responsive'>
											<table id='example1' class='table table-condensed table-bordered table-striped'>
												<thead>
													<tr>
														<th style='width:20px'>No</th>
														<th>Nama File</th>
														<th>Ukuran</th>
														<th style='width:100px'>Aksi</th>
													</tr>
												</thead>
												<tbody>";

			$no = 0;
			$tampil = mysqli_query($koneksi, "SELECT * FROM dokumentasi");
			while ($r = mysqli_fetch_array($tampil)) {
				$no++;
				echo "
														<tr>
															<td> $no</td>
															<td>$r[file]</td>
															<td style='color:red'>$r[ukuran_file]</td>
															<input type='hidden' name='idfile' value='$r[id_file]'>
															<td width='50px'>
															<a class='btn btn-info btn-xs' title='Download' href='../berkas/$r[file]'><span class='glyphicon glyphicon-download'></span></a>
																<button class='btn btn-danger btn-xs' name='deldata' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></button></td>
														</tr>";
			}

			if (isset($_POST['deldata'])) {
				$query = mysqli_fetch_array(mysqli_query($koneksi, "select * from dokumentasi where id_file='$_POST[idfile]'"));
				$target = '../berkas/' . $query['file'];
				if (file_exists($target)) {
					unlink($target);
				}

				mysqli_query($koneksi, "DELETE FROM dokumentasi where id_file='$_POST[idfile]'");

				jump("?pg=$pg");
			}
			"
														";
			echo "
												</tbody>
											</table>
										</div>
								</div>
								</form>
							</div>";
		}
		echo "
				   <div class='modal fade' id='tambahfile' style='display: none;'>
							<div class='modal-dialog'>
								<div class='modal-content'>
									<div class='modal-header' style='background-color:#A9A9A9'>
										<h3 class='modal-title'><i class='fa fa-upload'> Tambah Berkas Dokumentasi</i></h3>
									</div>
									<div class='modal-body'>
										<form method='POST' class='form-horizontal' action='' enctype='multipart/form-data'>

											<table class='table table-condensed table-bordered'>
												<tbody>
													<tr>
														<th scope='row'>Nama file</th>
														<td><input type='text' class='form-control' name='nama_file' required></td>
													</tr>

													<tr>
														<th scope='row'>File</th>
														<td>


															<i class='fa fa-search'></i> Cari File Berkas
															<input type='file' class='files form-control' name='file' onchange='$('#upload-file-info').html($(this).val());'>
															<span class='label label-info' id='upload-file-info'></span>

														</td>
													</tr>


												</tbody>
											</table>
											<button type='submit' name='tambah' class='btn btn-info'>Tambahkan</button>
										</form>
									</div>
								</div>
							</div>
						</div>";
	} elseif ($pg == 'kartu') {
		if ($ac == '') {
			echo "
									<div class='row'>
										<div class='col-md-3'></div>
										<div class='col-md-6'>
											<div class='box box-solid'>
												<div class='box-header with-border' style='background-color:#A9A9A9'>
													<h3 class='box-title'>Kartu Peserta Ujian</h3>
													<div class='box-tools pull-right btn-group'>
														<button class='btn btn-sm btn-flat btn-success' onclick=frames['frameresult'].print()><i class='fa fa-print'></i> Print</button>
														<a href='?pg=siswa' class='btn btn-sm bg-maroon' title='Batal'><i class='fa fa-times'></i></a>
													</div>
												</div><!-- /.box-header -->
												<div class='box-body'>
													$info
													<div class='form-group'>
														<label>Header Kartu</label>";
			$nm_ujian = mysqli_query($koneksi, "SELECT * FROM ujian");
			$tampil = mysqli_fetch_array($nm_ujian);
			echo "
														<textarea  id='headerkartu' class='form-control' onchange='kirim_form();' rows='3'>KARTU PESERTA $tampil[kode_ujian]</textarea>
													</div>
													<div class='form-group'>
														
														<label>Kelas</label>
														<div class='row'>
															<div class='col-xs-4'>";
			$total = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM kelas"));
			$limit = number_format($total / 3, 0, '', '');
			$limit2 = number_format($limit * 2, 0, '', '');
			$sql_kelas = mysqli_query($koneksi, "SELECT * FROM kelas ORDER BY nama ASC LIMIT 0,$limit");
			while ($kelas = mysqli_fetch_array($sql_kelas)) {
				echo "
																		<div class='radio'>
																			<label><input type='radio' name='idk' value='$kelas[id_kelas]' onclick=printkartu('$kelas[0]') /> $kelas[nama]</label>
																		</div>
																	";
			}
			echo "
															</div>
															<div class='col-xs-4'>";
			$sql_kelas = mysqli_query($koneksi, "SELECT * FROM kelas ORDER BY nama ASC LIMIT $limit,$limit");
			while ($kelas = mysqli_fetch_array($sql_kelas)) {
				echo "
																		<div class='radio'>
																			<label><input type='radio' name='idk' value='$kelas[id_kelas]' onclick=printkartu('$kelas[0]') /> $kelas[nama]</label>
																		</div>
																	";
			}
			echo "
															</div>
															<div class='col-xs-4'>";
			$sql_kelas = mysqli_query($koneksi, "SELECT * FROM kelas ORDER BY nama ASC LIMIT $limit2,$total");
			while ($kelas = mysqli_fetch_array($sql_kelas)) {
				echo "
																		<div class='radio'>
																			<label><input type='radio' name='idk' value='$kelas[id_kelas]' onclick=printkartu('$kelas[0]') /> $kelas[nama]</label>
																		</div>
																	";
			}
			echo "
															</div>
														</div>
													</div>
												</div><!-- /.box-body -->
											</div><!-- /.box -->
										</div>
									</div>
									<iframe id='loadframe' name='frameresult' src='kartu.php' style='border:none;width:1px;height:1px;'></iframe>
								";
		}
	} elseif ($pg == 'absen') {
		if ($ac == '') {
			echo "
									<div class='row'>
										
										<div class='col-md-3'></div>
										<div class='col-md-6'>
										
											<div class='box box-solid'>
												<div class='box-header with-border' style='background-color:#A9A9A9'>
													<h3 class='box-title'>Daftar Hadir Peserta</h3>
													<div class='box-tools pull-right btn-group'>
														<button id='btnabsen' class='btn btn-sm btn-flat btn-success' onclick=frames['frameresult'].print()><i class='fa fa-print'></i> Print</button>
													</div>
												</div><!-- /.box-header -->
												<div class='box-body'>
													$info
													<div class='form-group'>
														
												<div class='form-group'>
												<label>Pilih Mapel</label>
												<select id='absenmapel' class='select2 form-control' required='true' onchange=printabsen();>";
			$sql_mapel = mysqli_query($koneksi, "SELECT * FROM ujian");
			echo "
													<option value=''>Pilih Jadwal Ujian</option>";
			while ($mapel = mysqli_fetch_array($sql_mapel)) {
				echo "
														<option value='$mapel[id_mapel]'>$mapel[nama] $mapel[level] $mapel[id_pk]</option>
													";
			}
			echo "
												</select>
											</div>

											<div class='form-group'>
												<label>Pilih Ruang</label>
												<select id='absenruang' class='form-control select2' onchange=printabsen();>";
			$sql = mysqli_query($koneksi, "SELECT * FROM siswa  GROUP BY ruang");
			echo "<option value=''>Pilih Ruang</option>";
			while ($ruang = mysqli_fetch_array($sql)) {
				echo "<option value='$ruang[ruang]'>$ruang[ruang]</option>";
			}
			echo "
											</select>
											</div>
											<div class='form-group'>
												<label>Pilih Sesi</label>
												<select id='absensesi' class='form-control select2' onchange=printabsen();>";
			$ruang = $_POST['ruang'];
			$sql = mysqli_query($koneksi, "SELECT * FROM siswa GROUP BY sesi");
			echo "<option value=''>Pilih Sesi</option>";
			while ($sesi = mysqli_fetch_array($sql)) {
				echo "<option value='$sesi[sesi]'>$sesi[sesi]</option>";
			}
			echo "
												</select>
											</div>
											<div class='form-group'>
												<label>Pilih Kelas</label>
												<select id='absenkelas' class='form-control select2' onchange=printabsen();>";

			$sql_sesi = mysqli_query($koneksi, "SELECT * FROM kelas ");
			echo "<option value=''>pilih Kelas</option>";
			while ($kelas = mysqli_fetch_array($sql_sesi)) {
				echo "<option value='$kelas[id_kelas]'>$kelas[nama]</option>";
			}
			echo "
												</select>
											</div>
													
													</div>
												</div><!-- /.box-body -->
											</div><!-- /.box -->
										</div>
									</div>
									<iframe id='loadabsen' name='frameresult' src='absen.php' style='border:none;width:0px;height:0px;'></iframe>";
		}
	} elseif ($pg == 'siswa') {
		include 'master_siswa.php';
	} elseif ($pg == 'uplfotosiswa') {
		cek_session_admin();
		if (isset($_POST["uplod"])) {
			$output = '';
			if ($_FILES['zip_file']['name'] != '') {
				$file_name = $_FILES['zip_file']['name'];
				$array = explode(".", $file_name);
				$name = $array[0];
				$ext = $array[1];
				if ($ext == 'zip') {
					$path = '../foto/fotosiswa/';
					$location = $path . $file_name;
					if (move_uploaded_file($_FILES['zip_file']['tmp_name'], $location)) {
						$zip = new ZipArchive;
						if ($zip->open($location)) {
							$zip->extractTo($path);
							$zip->close();
						}
						$files = scandir($path);
						foreach ($files as $file) {
							$file_ext = pathinfo($file, PATHINFO_EXTENSION);
							$allowed_ext = array('jpg', 'JPG');
							if (in_array($file_ext, $allowed_ext)) {
								$output .= '<div class="col-md-3"><div style="padding:16px; border:1px solid #CCC;"><img class="img img-responsive" style="height:150px;" src="../foto/fotosiswa/' . $file . '" /></div></div>';
							}
						}
						unlink($location);
						$pesan = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i> Info</h4>Upload File zip berhasil</div>";
					}
				} else {
					$pesan = "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-info'></i> Gagal Upload</h4>Mohon Upload file zip</div>";
				}
			}
		}
		if (isset($_POST['hapussemuafoto'])) {
			$files = glob('../foto/fotosiswa/*'); // Ambil semua file yang ada dalam folder

			foreach ($files as $file) { // Lakukan perulangan dari file yang kita ambil

				if (is_file($file)) // Cek apakah file tersebut benar-benar ada

					unlink($file); // Jika ada, hapus file tersebut

			}
		}
		echo "
												
													<div class='box box-danger'>
														<div class='box-header with-border' style='background-color:#A9A9A9' >
															<h3 class='box-title'>Upload Foto Peserta Ujian</h3>
															<div class='box-tools pull-right btn-group'>
																
																<a href='?pg=$pg' class='btn btn-sm bg-maroon' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
														<div class='alert alert-danger alert-dismissible'>
															<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
															<h4><i class='icon fa fa-info'></i> Info</h4>
															Upload gambar dalam berkas zip,,, Penamaan gambar sesuai dengan no peserta siswa ujian
															</div>
															<form action='' method='post' enctype='multipart/form-data'>
															<div class='col-md-6'>
															<input class='form-control' type='file' name='zip_file'  accept='.zip' />
															</div>
															<div class='col-md-6'>
															<button class='btn bg-purple' name='uplod' type='submit' >Upload Foto</button>
															</div>
															</form>
															
														</div><!-- /.box-body -->
													</div><!-- /.box -->
													<div class='box box-solid'>
														<div class='box-header with-border' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Daftar Foto Peserta</h3>
															<div class='box-tools pull-right btn-group'>
															<form action='' method='post'>
																<button class='btn btn-sm bg-red' name='hapussemuafoto'>Hapus Semua Foto</button>
																</form>
																
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>";
		$folder = "../foto/fotosiswa/"; //Sesuaikan Folder nya
		if (!($buka_folder = opendir($folder))) die("eRorr... Tidak bisa membuka Folder");

		$file_array = array();
		while ($baca_folder = readdir($buka_folder)) {
			$file_array[] = $baca_folder;
		}

		$jumlah_array = count($file_array);
		for ($i = 2; $i < $jumlah_array; $i++) {
			$nama_file = $file_array;
			$nomor = $i - 1;
			echo "
														<div class='col-md-1'>
														<img class='img-logo' src='$folder$nama_file[$i]' style='width:65px'/><br><br>
														</div>";
		}

		closedir($buka_folder);
		echo "	
														</div><!-- /.box-body -->
													</div><!-- /.box -->
											";
	} elseif ($pg == 'importmaster') {
		cek_session_admin();
		if ($setting['jenjang'] == 'SMK') {
			$format = 'importdatamaster.xls';
		}
		if ($setting['jenjang'] == 'SMP') {
			$format = 'importdatamaster.xls';
		}
		if ($setting['jenjang'] == 'SD') {
			$format = 'importdatamaster.xls';
		}
		echo "
								<div class='row'>
									
									<div class='col-md-12'>
                                        
                                            <div class='box box-solid' >
                                                <div class='box-header with-border' style='background-color:#A9A9A9'>
                                                    <h3 class='box-title' >Import Data Master</h3>
                                                    <div class='box-tools pull-right btn-group'>
                                                        <a href='$format' class='btn btn-sm btn-flat btn-info'><i class='fa fa-file-excel-o'></i> Download Format</a>
														<a href='?pg=siswa' class='btn btn-sm btn-flat btn-primary' title='Batal'><i class='fa fa-times'></i></a>
                                                    </div>
                                                </div><!-- /.box-header -->
                                                <div class='box-body'>
                                                    $info
												<div class='box box-solid'>
												<div class='box-body'>
                                                    <div class='form-group'>
														<div class='row'>
														<div class='col-md-4'>
														<form id='formsiswa' enctype='multipart/form-data'>
															<label>Pilih File</label>
															
															<input type='file'  name='file' class='form-control' required='true'/>
															
														</div>
														<div class='col-md-4'>
														<label>&nbsp;</label><br>
															<button type='submit' name='submit' class='btn btn-flat btn-warning'><i class='fa fa-check'></i> Import Data</button>
														</div>
														</form>
														</div>
                                                    </div>
													<p>Menu ini berfungsi untuk import data Master</p>
													<p><b>*Import Data Siswa, Jurusan, Kelas, Ruangan,Sesi dan Level</b>
                                                    <p>
                                                        Sebelum meng-import pastikan file yang akan anda import sudah dalam bentuk Ms. Excel 97-2003 Workbook (.xls) dan format penulisan harus sesuai dengan yang telah ditentukan. <br/>
                                                    </p>
													<div id='progressbox'></div>
													<div id='hasilimport'></div>
												</div>
												</div>
                                                </div><!-- /.box-body -->
                                                <div class='box-footer'>
                                                    
                                                </div>
                                            </div><!-- /.box -->
                                        
                                    </div>
                                </div>
                            ";
	} elseif ($pg == 'importguru') {
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$file = $_FILES['file']['name'];
			$temp = $_FILES['file']['tmp_name'];
			$ext = explode('.', $file);
			$ext = end($ext);
			if ($ext <> 'xls') {
				$info = info('Gunakan file Ms. Excel 93-2007 Workbook (.xls)', 'NO');
			} else {
				$data = new Spreadsheet_Excel_Reader($temp);
				$hasildata = $data->rowcount($sheet_index = 0);
				$sukses = $gagal = 0;
				$exec = mysqli_query($koneksi, "delete from pengawas where level='guru'");
				for ($i = 2; $i <= $hasildata; $i++) {

					$nip = $data->val($i, 2);
					$nama = $data->val($i, 3);
					$nama = addslashes($nama);
					$username = $data->val($i, 4);
					$username = str_replace("'", "", $username);
					$password = $data->val($i, 5);

					$exec = mysqli_query($koneksi, "INSERT INTO pengawas (nip,nama,username,password,level) VALUES ('$nip','$nama','$username','$password','guru')");
					($exec) ? $sukses++ : $gagal++;
				}
				$total = $hasildata - 1;

				$info = info("Berhasil: $sukses | Gagal: $gagal | Dari: $total", 'OK');
			}
		}
		echo "
								<div class='row'>
									<div class='col-md-3'></div>
									<div class='col-md-6'>
                                        <form action='' method='post' enctype='multipart/form-data'>
                                            <div class='box box-solid'>
                                                <div class='box-header with-border'>
                                                    <h3 class='box-title'>Import Guru</h3>
                                                    <div class='box-tools pull-right btn-group'>
                                                        <button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Import</button>
														<a href='?pg=guru' class='btn btn-sm btn-default' title='Batal'><i class='fa fa-times'></i></a>
                                                    </div>
                                                </div><!-- /.box-header -->
                                                <div class='box-body'>
                                                    $info
                                                    <div class='form-group'>
                                                        <label>Pilih File</label>
                                                        <input type='file' name='file' class='form-control' required='true'/>
                                                    </div>
                                                    <p>
                                                        Sebelum meng-import pastikan file yang akan anda import sudah dalam bentuk Ms. Excel 97-2003 Workbook (.xls) dan format penulisan harus sesuai dengan yang telah ditentukan. <br/>
                                                    </p>
                                                </div><!-- /.box-body -->
                                                <div class='box-footer'>
                                                    <a href='importdataguru.xls'><i class='fa fa-file-excel-o'></i> Download Format</a>
                                                </div>
                                            </div><!-- /.box -->
                                        </form>
                                    </div>
                                </div>
                            ";
	}

	//hapuswali
	elseif ($pg == 'hapuswali') {
		cek_session_admin();
		if ($ac == 'hapus') {
			$id = $_GET['id'];
			$info = info("Anda yakin akan menghapus wali kelas ini?");
			if (isset($_POST['submit'])) {
				$exec = mysqli_query($koneksi, "DELETE FROM walikls WHERE idwali='$id'");
				(!$exec) ? $info = info("Gagal menghapus!", "NO") : jump("?pg=walikelas");
			}
			echo "
												<form action='' method='post'>
													<div class='box box-danger'>
														<div class='box-header with-border'>
															<h3 class='box-title'>Hapus</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-danger'><i class='fa fa-trash-o'></i> Hapus</button>
																<a href='?pg=walikelas' class='btn btn-sm btn-default' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
	}

	//hapuswali
	elseif ($pg == 'hapuslegitimasi') {
		cek_session_admin();
		if ($ac == 'hapus') {
			$id = $_GET['id'];
			$info = info("Anda yakin akan menghapus wali kelas ini?");
			if (isset($_POST['submit'])) {
				$exec = mysqli_query($koneksi, "DELETE FROM jadwal WHERE id_kelas='$id'");
				(!$exec) ? $info = info("Gagal menghapus!", "NO") : jump("?pg=legitimasi");
			}
			echo "
												<form action='' method='post'>
													<div class='box box-danger'>
														<div class='box-header with-border'>
															<h3 class='box-title'>Hapus</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-danger'><i class='fa fa-trash-o'></i> Hapus</button>
																<a href='?pg=legitimasi' class='btn btn-sm btn-default' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
	} elseif ($pg == 'pengawas') {
		cek_session_admin();
		echo "
								<div class='row'>
									<div class='col-md-8'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Manajemen User</h3>
											</div><!-- /.box-header -->
											<div class='box-body'>
											<div class='table-responsive'>
												<table id='example1' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>NIP</th>
															<th>Nama</th>
															<th>Username</th>
															<th>Level</th>
															<th width=60px></th>
														</tr>
													</thead>
													<tbody>";
		$pengawasQ = mysqli_query($koneksi, "SELECT * FROM pengawas where level='admin' ORDER BY nama ASC");
		while ($pengawas = mysqli_fetch_array($pengawasQ)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$pengawas[nip]</td>
																<td>$pengawas[nama]</td>
																<td>$pengawas[username]</td>
																<td>$pengawas[level]</td>
																<td align='center'>
																<div class='btn-group'>
																	<a href='?pg=$pg&ac=edit&id=$pengawas[id_pengawas]'> <button class='btn btn-flat btn-xs btn-warning'><i class='fa fa-pencil-square-o'></i></button></a>
																	<a href='?pg=$pg&ac=hapus&id=$pengawas[id_pengawas]'> <button class='btn btn-flat btn-xs bg-maroon'><i class='fa fa-trash-o'></i></button></a>
																</div>
																</td>
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									<div class='col-md-4'>";
		if ($ac == '') {
			if (isset($_POST['submit'])) {
				$nip = $_POST['nip'];
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				$username = $_POST['username'];
				$pass1 = $_POST['pass1'];
				$pass2 = $_POST['pass2'];

				$cekuser = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE username='$username'"));
				if ($cekuser > 0) {
					$info = info("Username $username sudah ada!", "NO");
				} else {
					if ($pass1 <> $pass2) {
						$info = info("Password tidak cocok!", "NO");
					} else {
						$password = password_hash($pass1, PASSWORD_BCRYPT);
						$exec = mysqli_query($koneksi, "INSERT INTO pengawas (nip,nama,username,password,level) VALUES ('$nip','$nama','$username','$password','admin')");
						(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
					}
				}
			}
			echo "
												<form action='' method='post'>
													<div class='box box-solid'>
														<div class='box-header with-border' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Tambah</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
															<div class='form-group'>
																<label>NIP</label>
																<input type='text' name='nip' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Nama</label>
																<input type='text' name='nama' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Username</label>
																<input type='text' name='username' class='form-control' required='true'/>
															</div>
															
															<div class='form-group'>
																<div class='row'>
																	<div class='col-md-6'>
																		<label>Password</label>
																		<input type='password' name='pass1' class='form-control' required='true'/>
																	</div>
																	<div class='col-md-6'>
																		<label>Ulang Password</label>
																		<input type='password' name='pass2' class='form-control' required='true'/>
																	</div>
																</div>
															</div>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
		if ($ac == 'edit') {
			$id = $_GET['id'];
			$value = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE id_pengawas='$id'"));
			if (isset($_POST['submit'])) {
				$nip = $_POST['nip'];
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				$username = $_POST['username'];
				$pass1 = $_POST['pass1'];
				$pass2 = $_POST['pass2'];

				if ($pass1 <> '' and $pass2 <> '') {
					if ($pass1 <> $pass2) {
						$info = info("Password tidak cocok!", "NO");
					} else {
						$password = password_hash($pass1, PASSWORD_BCRYPT);
						$exec = mysqli_query($koneksi, "UPDATE pengawas SET nip='$nip',nama='$nama',username='$username',password='$password',level='admin' WHERE id_pengawas='$id'");
					}
				} else {
					$exec = mysqli_query($koneksi, "UPDATE pengawas SET nip='$nip',nama='$nama',username='$username',level='admin' WHERE id_pengawas='$id'");
				}
				(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
			}
			echo "
												<form action='' method='post'>
													<div class='box box-solid'>
														<div class='box-header with-border' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Edit</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<a href='?pg=$pg' class='btn btn-sm bg-maroon' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
															<div class='form-group'>
																<label>NIP</label>
																<input type='text' name='nip' value='$value[nip]' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Nama</label>
																<input type='text' name='nama' value='$value[nama]' class='form-control' required='true'/>
															</div>
															<div class='form-group'>
																<label>Username</label>
																<input type='text' name='username' value='$value[username]' class='form-control' required='true'/>
															</div>
															
															<div class='form-group'>
																<div class='row'>
																	<div class='col-md-6'>
																		<label>Password</label>
																		<input type='password' name='pass1' class='form-control'/>
																	</div>
																	<div class='col-md-6'>
																		<label>Ulang Password</label>
																		<input type='password' name='pass2' class='form-control'/>
																	</div>
																</div>
															</div>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
		if ($ac == 'hapus') {
			$id = $_GET['id'];
			$info = info("Anda yakin akan menghapus pengawas ini?");
			if (isset($_POST['submit'])) {
				$exec = mysqli_query($koneksi, "DELETE FROM pengawas WHERE id_pengawas='$id'");
				(!$exec) ? $info = info("Gagal menghapus!", "NO") : jump("?pg=$pg");
			}
			echo "
												<form action='' method='post'>
													<div class='box box-danger'>
														<div class='box-header with-border'>
															<h3 class='box-title'>Hapus</h3>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Hapus</button>
																<a href='?pg=$pg' class='btn btn-sm btn-default' title='Batal'><i class='fa fa-times'></i></a>
															</div>
														</div><!-- /.box-header -->
														<div class='box-body'>
															$info
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</form>
											";
		}
		echo "
									</div>
								</div>
							";
	} // modul jurusan

	elseif ($pg == 'pk') {
		if ($setting['jenjang'] == 'SMK') {
			cek_session_admin();
			if (isset($_POST['tambahmapel'])) {
				$idpk = str_replace(' ', '', $_POST['idpk']);
				$nama = $_POST['nama'];
				$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM pk WHERE id_pk='$idpk'"));
				if ($cek > 0) {
					$info = info("Jurusan dengan kode $idpk sudah ada!", "NO");
				} else {
					$exec = mysqli_query($koneksi, "INSERT INTO pk (id_pk,program_keahlian) VALUES ('$idpk','$nama')");
					if (!$exec) {
						$info = info("Gagal menyimpan!", "NO");
					} else {
						jump("?pg=$pg");
					}
				}
			}
			$info = '';
			echo "
								<div class='row'>
									<div class='col-md-12'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'> <i class='fa fa-bookmark'> Jurusan</i></h3>
												<div class='box-tools pull-right'>
												<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahmapel'><i class='fa fa-check'></i> Tambah Jurusan</button>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
											$info
												<table id='tablejurusan' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>Kode Jurusan</th>
															<th>Nama Jurusan</th>
															
														</tr>
													</thead>
													<tbody>";
			$adminQ = mysqli_query($koneksi, "SELECT * FROM pk ORDER BY id_pk ASC");
			while ($adm = mysqli_fetch_array($adminQ)) {
				$no++;
				echo "
															<tr>
																<td>$no</td>
																<td>$adm[id_pk]</td>
																<td>$adm[program_keahlian]</td>
																
															</tr>
														";
			}
			echo "
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									
									<div class='modal fade' id='tambahmapel' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header bg-orange'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'>Tambah Jurusan</h3>
												</div>
												<div class='modal-body'>
													<form action='' method='post'>
														<div class='form-group'>
															<label>Kode Jurusan</label>
															<input type='text' name='idpk' class='form-control'  required='true'/>
														</div>
														<div class='form-group'>
															<label>Nama Jurusan</label>
															<input type='text' name='nama'  class='form-control' required='true'/>
														</div>
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='tambahmapel' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
													</form>
												</div>
											</div>					
										</div>											
									</div>
									
									
								</div>
							";
		}
	} elseif ($pg == 'jenisujian') {
		cek_session_admin();
		if (isset($_POST['tambahujian'])) {
			$id = str_replace(' ', '', $_POST['idujian']);
			$nama = $_POST['nama'];
			$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM jenis WHERE id_jenis='$id'"));
			if ($cek > 0) {
				$info = info("Jurusan dengan kode $id sudah ada!", "NO");
			} else {
				$exec = mysqli_query($koneksi, "INSERT INTO jenis (id_jenis,nama) VALUES ('$id','$nama')");
				if (!$exec) {
					$info = info("Gagal menyimpan!", "NO");
				} else {
					jump("?pg=$pg");
				}
			}
		}
		$info = '';
		echo "
								<div class='row'>
									<div class='col-md-12'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>JENIS UJIAN</h3>
												<div class='box-tools pull-right'>
												<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahujian'><i class='fa fa-check'></i> Tambah Ujian</button>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
											$info
												<table id='tablejenis' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>Kode Ujian</th>
															<th>Nama Ujian</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>";
		$adminQ = mysqli_query($koneksi, "SELECT * FROM jenis ORDER BY id_jenis ASC");
		while ($adm = mysqli_fetch_array($adminQ)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$adm[id_jenis]</td>
																<td>$adm[nama]</td>
																<td>$adm[status]</td>
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									
									<div class='modal fade' id='tambahujian' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header' style='background-color:#A9A9A9'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'>Tambah Ujian</h3>
												</div>
												<div class='modal-body'>
													<form action='' method='post'>
														<div class='form-group'>
															<label>Kode Ujian</label>
															<input type='text' name='idujian' class='form-control'  required='true'/>
														</div>
														<div class='form-group'>
															<label>Nama Ujian</label>
															<input type='text' name='nama'  class='form-control' required='true'/>
														</div>
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='tambahujian' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
													</form>
												</div>
											</div>					
										</div>											
									</div>
								</div>
							";
	} elseif ($pg == 'ruang') {
		cek_session_admin();
		include 'master_ruang.php';
	} elseif ($pg == 'level') {
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$level = str_replace(' ', '', $_POST['level']);
			$ket = $_POST['keterangan'];

			$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM level WHERE kode_level='$level'"));
			if ($cek > 0) {
				$info = info("Level atau tingkat $level sudah ada!", "NO");
			} else {
				$exec = mysqli_query($koneksi, "INSERT INTO level (kode_level,keterangan) VALUES ('$level','$ket')");
				if (!$exec) {
					$info = info("Gagal menyimpan!", "NO");
				} else {
					jump("?pg=$pg");
				}
			}
		}
		echo "
								<div class='row'>
									<div class='col-md-12'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Level atau Tingkat</h3>
												<div class='box-tools pull-right'>
												<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahlevel'><i class='fa fa-check'></i> Tambah Level</button>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<table id='tablelevel' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															
															<th >Kode Level</th>
															<th >Nama Level</th>
															
														</tr>
													</thead>
													<tbody>";
		$adminQ = mysqli_query($koneksi, "SELECT * FROM level ");
		while ($adm = mysqli_fetch_array($adminQ)) {
			$no++;

			echo "
															<tr>
																<td>$no</td>
																
																<td>$adm[kode_level]</td>
																<td>$adm[keterangan]</td>
																																									
																
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									
									<div class='modal fade' id='tambahlevel' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header' style='background-color:#A9A9A9'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'>Tambah Level</h3>
												</div>
												<div class='modal-body'>
													<form action='' method='post'>
														<div class='form-group'>
															<label>Kode Level</label>
															<input type='text' name='level' class='form-control'  required='true'/>
														</div>
														<div class='form-group'>
															<label>Nama Level</label>
															<input type='text' name='keterangan'  class='form-control' required='true'/>
														</div>
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
													</form>
												</div>
											</div>					
										</div>											
									</div>
									
								</div>
							";
	} elseif ($pg == 'sesi') {
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$sesi = str_replace(' ', '', $_POST['sesi']);
			$nama = $_POST['nama'];

			$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM sesi WHERE kode_sesi='$sesi'"));
			if ($cek > 0) {
				$info = info("Kelompok Test atau Sesi $sesi sudah ada!", "NO");
			} else {
				$exec = mysqli_query($koneksi, "INSERT INTO sesi (kode_sesi,nama_sesi) VALUES ('$sesi','$nama')");
				if (!$exec) {
					$info = info("Gagal menyimpan!", "NO");
				} else {
					jump("?pg=$pg");
				}
			}
		}
		echo "
								<div class='row'>
									<div class='col-md-12'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Sesi atau Kelompok Test</h3>
												<div class='box-tools pull-right'>
												<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahsesi'><i class='fa fa-check'></i> Tambah Sesi</button>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<table id='tablesesi' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															
															<th >Kode Sesi</th>
															<th >Nama Sesi</th>
															
														</tr>
													</thead>
													<tbody>";
		$adminQ = mysqli_query($koneksi, "SELECT * FROM sesi ");
		while ($adm = mysqli_fetch_array($adminQ)) {
			$no++;

			echo "
															<tr>
																<td>$no</td>
																
																<td>$adm[kode_sesi]</td>
																<td>$adm[nama_sesi]</td>
																																									
																
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									
									<div class='modal fade' id='tambahsesi' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header' style='background-color:#A9A9A9'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'><i class='fa fa-user'>  Tambah Sesi</i></h3>
												</div>
												<div class='modal-body'>
													<form action='' method='post'>
														<div class='form-group'>
															<label>Kode Sesi</label>
															<input type='text' name='sesi' class='form-control'  required='true'/>
														</div>
														<div class='form-group'>
															<label>Nama Sesi</label>
															<input type='text' name='nama'  class='form-control' required='true'/>
														</div>
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
													</form>
												</div>
											</div>					
										</div>											
									</div>
									
								</div>
							";
	} elseif ($pg == 'kelas') {
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$idkelas = str_replace(' ', '', $_POST['idkelas']);
			$nama = $_POST['nama'];
			$level = $_POST['level'];
			$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM kelas WHERE id_kelas='$idkelas'"));
			if ($cek > 0) {
				$info = info("Kelas dengan kode $idkelas sudah ada!", "NO");
			} else {
				$exec = mysqli_query($koneksi, "INSERT INTO kelas (id_kelas,nama,level) VALUES ('$idkelas','$nama','$level')");
				if (!$exec) {
					$info = info("Gagal menyimpan!", "NO");
				} else {
					jump("?pg=$pg");
				}
			}
		}
		echo "
								<div class='row'>
									<div class='col-md-12'>
									<div class='alert alert-info '>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Level Kelas harus sama dengan Kode Level di data master
													</div>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'><i class='fa fa-child'>  Kelas</i></h3>
												<div class='box-tools pull-right'>
												<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahkelas'><i class='fa fa-check'></i> Tambah Kelas</button>
												</div>
											</div><!-- /.box-header -->
											<div class='box-body'>
												<table id='tablekelas' class='table table-bordered table-striped'>
													<thead>
														<tr>
															<th width='5px'>#</th>
															<th>Kode Kelas</th>
															<th>Level Kelas</th>
															<th>Nama Kelas</th>
															
														</tr>
													</thead>
													<tbody>";
		$adminQ = mysqli_query($koneksi, "SELECT * FROM kelas ORDER BY nama ASC");
		while ($adm = mysqli_fetch_array($adminQ)) {
			$no++;
			echo "
															<tr>
																<td>$no</td>
																<td>$adm[id_kelas]</td>
																<td>$adm[level]</td>
																<td>$adm[nama]</td>
																
															</tr>
														";
		}
		echo "
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
									<div class='modal fade' id='tambahkelas' style='display: none;'>
										<div class='modal-dialog'>
											<div class='modal-content'>
												<div class='modal-header bg-orange'>
												<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
													<h3 class='modal-title'>Tambah Kelas</h3>
												</div>
												<div class='modal-body'>
													<form action='' method='post'>
														<div class='form-group'>
															<label>Kode Kelas</label>
															<input type='text' name='idkelas' class='form-control'  required='true'/>
														</div>
														<div class='form-group'>
															<label>Level</label>
																<select name='level' class='form-control' required='true'>
																<option value=''></option>";
		$levelQ = mysqli_query($koneksi, "SELECT * FROM level ");
		while ($level = mysqli_fetch_array($levelQ)) {

			echo "<option value='$level[kode_level]' >$level[kode_level]</option>";
		}
		echo "
															</select>
														</div>
														<div class='form-group'>
															<label>Nama Kelas</label>
															<input type='text' name='nama'  class='form-control' required='true'/>
														</div>
													<div class='modal-footer'>
															<div class='box-tools pull-right btn-group'>
																<button type='submit' name='submit' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
															</div>
													</div>
													</form>
												</div>
											</div>					
										</div>											
									</div>
									
									
								</div>
							";
	} elseif ($pg == 'banksoal') {
		if ($ac == '') {
			$pesan = '';
			$value = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM mapel WHERE id_mapel='$id'"));
			$tgl_ujian = explode(' ', $value['tgl_ujian']);
			if (isset($_POST['editbanksoal'])) {
				$id = $_POST['idm'];
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				if ($setting['jenjang'] == "SMK") {
					$idpk = $_POST['id_pk'];
				} else {
					if ($_POST['id_pk'] == 'khusus') {
						$idpk = "khusus";
					} else {
						$idpk = "semua";
					}
				}
				$jml_soal = $_POST['jml_soal'];
				$jml_esai = $_POST['jml_esai'];
				$bobot_pg = $_POST['bobot_pg'];
				$bobot_esai = $_POST['bobot_esai'];
				$tampil_pg = $_POST['tampil_pg'];
				$tampil_esai = $_POST['tampil_esai'];
				$level = $_POST['level'];
				$status = $_POST['status'];
				$kkm = $_POST['kkm'];
				$opsi = $_POST['opsi'];
				$guru = $_POST['guru'];
				$kelas = serialize($_POST['kelas']);
				if ($_POST['siswa'] == null) {
					$siswa = null;
				} else {
					$siswa = serialize($_POST['siswa']);
				}

				if ($pengawas['level'] == 'admin') {
					$exec = mysqli_query($koneksi, "UPDATE mapel SET idpk='$idpk',nama='$nama',level='$level',jml_soal='$jml_soal',jml_esai='$jml_esai',status='$status',idguru='$guru',bobot_pg='$bobot_pg',bobot_esai='$bobot_esai',tampil_pg='$tampil_pg',tampil_esai='$tampil_esai',kelas='$kelas',opsi='$opsi',siswa='$siswa',kkm='$kkm' WHERE id_mapel='$id'");

					(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
				} elseif ($pengawas['level'] == 'guru') {
					$exec = mysqli_query($koneksi, "UPDATE mapel SET idpk='$idpk',nama='$nama',level='$level',jml_soal='$jml_soal',jml_esai='$jml_esai',status='$status',bobot_pg='$bobot_pg',bobot_esai='$bobot_esai',tampil_pg='$tampil_pg',tampil_esai='$tampil_esai',kelas='$kelas',opsi='$opsi',siswa='$siswa' WHERE id_mapel='$id'");

					(!$exec) ? $info = info("Gagal menyimpan!", "NO") : jump("?pg=$pg");
				}
			}
			if (isset($_POST['tambahbanksoal'])) {
				$nama = $_POST['nama'];
				$nama = str_replace("'", "&#39;", $nama);
				if ($setting['jenjang'] == "SMK") {
					$id_pk = $_POST['id_pk'];
				} else {
					if ($_POST['id_pk'] == 'khusus') {
						$$id_pk = "khusus";
					} else {
						$id_pk = "semua";
					}
				}
				$jml_esai = $_POST['jml_esai'];
				$jml_soal = $_POST['jml_soal'];
				$bobot_pg = $_POST['bobot_pg'];
				$bobot_esai = $_POST['bobot_esai'];
				$tampil_pg = $_POST['tampil_pg'];
				$tampil_esai = $_POST['tampil_esai'];
				$level = $_POST['level'];
				$kkm = $_POST['kkm'];
				$status = $_POST['status'];
				$opsi = $_POST['opsi'];
				$kelas = serialize($_POST['kelas']);
				if ($_POST['siswa'] == null) {
					$siswa = null;
				} else {
					$siswa = serialize($_POST['siswa']);
				}

				$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM mapel WHERE nama='$nama' and level='$level' and kelas ='$kelas'"));
				if ($pengawas['level'] == 'admin') {
					$guru = $_POST['guru'];
					if ($cek > 0) {
						$pesan = "<div class='alert alert-warning alert-dismissible'>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Maaf Kode Mapel - Level - Kelas Soal Sudah ada !
													</div>";
					} else {
						$exec = mysqli_query($koneksi, "INSERT INTO mapel (idpk, nama, jml_soal,jml_esai,level,status,idguru,bobot_pg,bobot_esai,tampil_pg,tampil_esai,kelas,opsi,kkm) VALUES ('$id_pk','$nama','$jml_soal','$jml_esai','$level','$status','$guru','$bobot_pg','$bobot_esai','$tampil_pg','$tampil_esai','$kelas','$opsi','$kkm')");
						$pesan = "<div class='alert alert-success alert-dismissible'>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Data Berhasil ditambahkan ..
													</div>";
					}
				} elseif ($pengawas['level'] == 'guru') {
					if ($cek > 0) {
						$pesan = "<div class='alert alert-warning alert-dismissible'>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Maaf Kode Mapel - Level - Kelas Sudah ada !
													</div>";
					} else {
						$exec = mysqli_query($koneksi, "INSERT INTO mapel (idpk, nama, jml_soal,jml_esai,level,status,idguru,bobot_pg,bobot_esai,tampil_pg,tampil_esai,kelas,opsi,siswa) VALUES ('$id_pk','$nama','$jml_soal','$jml_esai','$level','$status','$id_pengawas','$bobot_pg','$bobot_esai','$tampil_pg','$tampil_esai','$kelas','$opsi','$siswa')");
						$pesan = "<div class='alert alert-success alert-dismissible'>
													<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
													<i class='icon fa fa-info'></i>
													Data Berhasil ditambahkan ..
													</div>";
					}
				}
			}
			echo "
								<div class='row'>
									<div class='col-md-12'>$pesan
										<div class='box box-solid '>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'><i class='fa fa-briefcase'></i> Data Bank Soal</h3>";
			if ($setting['server'] == 'pusat') {
				echo "
												<div class='box-tools pull-right btn-group'>
													<button id='btnhapusbank' class='btn btn-sm bg-maroon'><i class='fa fa-trash'></i> Hapus</button>
													<button class='btn btn-sm btn-flat btn-success' data-toggle='modal' data-target='#tambahbanksoal'><i class='glyphicon glyphicon-plus'></i> Tambah Bank Soal</button>
												</div>";
			}
			echo "
									
											</div><!-- /.box-header -->
											<div class='box-body'>
											<div id='tablereset' class='table-responsive'>
												<table id='example1' class='table table-bordered table-striped'>
													<thead>
														<tr><th width='5px'><input type='checkbox' id='ceksemua'  ></th>
															<th width='5px'>#</th>
															<th>Mata Pelajaran</th>														
															<th width='20px'>Soal PG</th>
															<th width='20px'>Soal Esai</th>
															<th width='10px'>KKM</th>	
															<th>Kelas</th>
															<th width='10px'>Guru</th>
															<th width='10px'>Status</th>";
			if ($setting['server'] == 'pusat') {
				echo "
															<th width='90px'>Aksi</th>";
			}
			echo "
														</tr>
													</thead>
													<tbody>";
			if ($pengawas['level'] == 'admin') {
				$mapelQ = mysqli_query($koneksi, "SELECT * FROM mapel ORDER BY date ASC");
			} elseif ($pengawas['level'] == 'guru') {
				$mapelQ = mysqli_query($koneksi, "SELECT * FROM mapel where idguru='$pengawas[id_pengawas]' ORDER BY date ASC");
			}

			while ($mapel = mysqli_fetch_array($mapelQ)) {
				$cek = mysqli_num_rows(mysqli_query($koneksi, "select * from soal where id_mapel='$mapel[id_mapel]'"));
				//parsing array

				$no++;
				echo "
															<tr><td><input type='checkbox' name='cekpilih[]' class='cekpilih' id='cekpilih-$no' value='$mapel[id_mapel]' ></td>
																<td>$no</td>
																<td>
																";
				if ($mapel['idpk'] == 'semua') {
					$jur = 'Semua';
				} else {
					$jur = $mapel['idpk'];
				}
				echo "
																<b><small class='label bg-purple'>$mapel[nama]</small></b> 
																<small class='label label-primary'>$mapel[level]</small>
																<small class='label label-primary'>$jur</small>
																</td>";


				echo "
																
																
																<td><small class='label label-warning'>$mapel[tampil_pg]/$mapel[jml_soal]</small> <small class='label label-danger'>$mapel[bobot_pg] %</small>
																<small class='label label-danger'>$mapel[opsi] opsi</small></td>
																<td><small class='label label-warning'>$mapel[tampil_esai]/$mapel[jml_esai]</small><small class='label label-danger'>$mapel[bobot_esai] %</small></td>
																<td align='center'><small class='label label-warning'>$mapel[kkm]</small></td>
																<td>";
				$dataArray = unserialize($mapel['kelas']);
				foreach ($dataArray as $key => $value) {
					echo "<small class='label label-success'>$value </small>&nbsp;";
				}
				echo "</td>
																";
				if ($cek <> 0) {
					if ($mapel['status'] == '0') {
						$status = '<label class="label label-danger">non aktif</label>';
					} else {
						$status = '<label class="label label-success">  aktif  </label>';
					}
				} else {
					$status = '<label class="label label-warning">  Soal Kosong  </label>';
				}
				$guruku = mysqli_fetch_array(mysqli_query($koneksi, "select*from pengawas where id_pengawas = '$mapel[idguru]'"));
				echo "
																<td><small class='label label-primary'>$guruku[nama]</small></td>
																<td align='center'>$status</td>
																
																<td align='center'>
																	<div class='btn-group'>
																			
																			<a href='?pg=$pg&ac=lihat&id=$mapel[id_mapel]'><button class='btn btn-flat btn-success btn-flat btn-xs'><i class='fa fa-search'></i></button></a>
																			<a href='?pg=$pg&ac=importsoal&id=$mapel[id_mapel]'><button class='btn btn-info btn-flat btn-xs'><i class='fa fa-upload'></i></button></a>																			
																			<a ><button title='duplikat Soal' data-soal='$mapel[id_mapel]' href='#' class='btn btn-danger btn-flat btn-xs duplikat'><i class='fa fa-copy '></i></button></a>
																			<a ><button class='btn btn-warning btn-flat btn-xs' data-toggle='modal' data-target='#editbanksoal$mapel[id_mapel]'><i class='fa fa-pencil-square-o'></i></button></a>
																	</div>
																</td>
															</tr>
															
													<div class='modal fade' id='editbanksoal$mapel[id_mapel]' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header bg-blue'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Edit Bank Soal</h3>
													</div>
													<div class='modal-body'>
													<form action='' method='post'>	
													<input type='hidden' id='idm' name='idm' value='$mapel[id_mapel]'/>
															<div class='form-group'>
																<label>Mata Pelajaran</label>
																<select name='nama' class='form-control' required='true'>
																<option value=''></option>";
				$pkQ = mysqli_query($koneksi, "SELECT * FROM mata_pelajaran ORDER BY nama_mapel ASC");
				while ($pk = mysqli_fetch_array($pkQ)) {
					($pk['kode_mapel'] == $mapel['nama']) ? $s = 'selected' : $s = '';
					echo "<option value='$pk[kode_mapel]' $s>$pk[nama_mapel]</option>";
				}
				echo "
															</select>
															</div>";
				if ($setting['jenjang'] == 'SMK') {
					echo "
															<div class='form-group'>
															<label>Program Keahlian</label>
															<select name='id_pk' class='form-control' required='true'>
																<option value='semua'>Semua</option>";
					$pkQ = mysqli_query($koneksi, "SELECT * FROM pk ORDER BY program_keahlian ASC");
					while ($pk = mysqli_fetch_array($pkQ)) {
						($pk['id_pk'] == $mapel['idpk']) ? $s = 'selected' : $s = '';
						($pk['id_pk'] == $mapel['idpk']) ? $s = 'selected' : $s = '';
						echo "<option value='$pk[id_pk]' $s>$pk[program_keahlian]</option>";
					}
					echo "
															</select>
															</div>";
				}
				echo "
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-6'>
																<label>Pilih Level</label>
																<select name='level' class='form-control' required='true'>
																<option value='semua'>Semua Level</option>
																";
				$lev = mysqli_query($koneksi, "SELECT * FROM level");
				while ($level = mysqli_fetch_array($lev)) {
					($level['kode_level'] == $mapel['level']);
					echo "<option value='$level[kode_level] '>$level[kode_level]</option>";
				}
				echo "
																</select>
																</div>
																<div class='col-md-6'>
																<label>Pilih Kelas</label><br>
																<select name='kelas[]' class='form-control select2' multiple='multiple' style='width:100%' required='true'>
																<option value='semua'>Semua Kelas</option>
																<option value='khusus'>Khusus</option>
																";
				$lev = mysqli_query($koneksi, "SELECT * FROM kelas  ");
				while ($kelas = mysqli_fetch_array($lev)) {
					if (in_array($kelas['id_kelas'], unserialize($mapel['kelas']))) {
						echo "<option value='$kelas[id_kelas]'>$kelas[id_kelas]</option>";
					} else {
						echo "<option value='$kelas[id_kelas]'>$kelas[id_kelas]</option>";
					}
				}
				echo "
																</select>
																</div>
																</div>
															</div>
															
															<div class='form-group'>
															<div class='row'>
																<div class='col-md-12'>
																	<label>Pilih Siswa</label><br>
																	<select name='siswa[]' class='form-control select2 ' style='width:100%' multiple >
																		<option value='semua'>Semua Siswa</option>";
				$lev = mysqli_query($koneksi, "SELECT * FROM siswa");
				while ($kelas = mysqli_fetch_array($lev)) {
					if (in_array($kelas['id_siswa'], unserialize($mapel['kelas']))) {
						echo "
																				<option value='$kelas[id_siswa]' selected>$kelas[nama]</option>";
					} else {
						echo "
																			<option value='$kelas[id_siswa]'>$kelas[nama]</option>";
					}
				}
				echo "
																	</select>
																	<span style='color: blue;'>Jika Prog Keahlian,dan Level [ALL]. Lalu pilih Kelas [Khusus],dan tentukan siswanya.</span><br>
																	<span style='color: red;'>*HANYA UNTUK SISWA SUSULAN/BERMASALAH*</span>
																</div>
															</div>
														</div>
															
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-3'>
																<label>Jumlah Soal PG</label>
																<input type='number' name='jml_soal' class='form-control' value='$mapel[jml_soal]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Bobot Soal PG %</label>
																<input type='number' name='bobot_pg' class='form-control' value='$mapel[bobot_pg]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Soal Tampil</label>
																<input type='number' name='tampil_pg' class='form-control' value='$mapel[tampil_pg]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Opsi</label>
																<select name='opsi' class='form-control'>
																<option value='3' ";
				if ($mapel['opsi'] == 3) {
					echo "selected";
				}
				echo ">3</option>
																<option value='4' ";
				if ($mapel['opsi'] == 4) {
					echo "selected";
				}
				echo ">4</option>
																<option value='5' ";
				if ($mapel['opsi'] == 5) {
					echo "selected";
				}
				echo ">5</option>
																</select>
																</div>
																</div>
															</div>
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-3'>
																<label>Jumlah Soal Essai</label>
																<input type='number' name='jml_esai' class='form-control' value='$mapel[jml_esai]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Bobot Soal Essai %</label>
																<input type='number' name='bobot_esai' class='form-control' value='$mapel[bobot_esai]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Soal Tampil</label>
																<input type='number' name='tampil_esai' class='form-control' value='$mapel[tampil_esai]' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>KKM</label>
																<input type='number' id='kkm' name='kkm' class='form-control' value='$mapel[kkm]'  required='true'/>
																</div>
																</div>
															</div>
															
																<div class='form-group'>
																<div class='row'>
															";
				if ($pengawas['level'] == 'admin') {
					echo "
																<div class='col-md-6'>
																<label>Guru Pengampu</label>
																<select name='guru' class='form-control' required='true'>
																";
					$guruku = mysqli_query($koneksi, "SELECT * FROM pengawas where level='guru' order by nama asc");
					while ($guru = mysqli_fetch_array($guruku)) {
						($guru['id_pengawas'] == $mapel['idguru']) ? $s = 'selected' : $s = '';
						echo "<option value='$guru[id_pengawas]' $s>$guru[nama]</option>";
					}
					echo "
																</select>
															</div>";
				}
				echo "
															<div class='col-md-6'>
																<label>Status Soal</label>
																<select name='status' class='form-control' required='true'>
																
																	<option value='1'>Aktif</option>
																	<option value='0'>Non Aktif</option>
																</select>
															</div>	
															</div>
															</div>
													
													</div>
													<div class='modal-footer'>
													<button type='submit' name='editbanksoal' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
												
													</div>
													</form>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
													</div>
														";
			}
			echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
								</div>
								
						";

			echo "<div class='modal fade' id='tambahbanksoal' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header' style='background-color:#A9A9A9'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'><i class='fa fa-book'> Tambah Bank Soal</i></h3>
													</div>
													<div class='modal-body'>
													<form action='' method='post'>	
															<div class='form-group'>
																<label>Mata Pelajaran</label>
																<select name='nama' class='form-control' required='true'>
																<option value=''></option>";
			$pkQ = mysqli_query($koneksi, "SELECT * FROM mata_pelajaran ORDER BY nama_mapel ASC");
			while ($pk = mysqli_fetch_array($pkQ)) {
				echo "<option value='$pk[kode_mapel]'>$pk[nama_mapel]</option>";
			}
			echo "
															</select>
															</div>";
			if ($setting['jenjang'] == 'SMK') {
				echo "
															<div class='form-group'>
															<label>Program Keahlian</label>
															<select name='id_pk' class='form-control' required='true'>
																<option value='semua'>Semua</option>";
				$pkQ = mysqli_query($koneksi, "SELECT * FROM pk ORDER BY program_keahlian ASC");
				while ($pk = mysqli_fetch_array($pkQ)) {
					echo "<option value='$pk[id_pk]'>$pk[program_keahlian]</option>";
				}
				echo "
															</select>
															</div>";
			}
			echo "
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-6'>
																<label>Level Soal</label>
																<select name='level' id='soallevel' class='form-control' required='true'>
																<option value=''></option>
																<option value='semua'>Semua</option>
																";
			$lev = mysqli_query($koneksi, "SELECT * FROM level");
			while ($level = mysqli_fetch_array($lev)) {
				echo "<option value='$level[kode_level]'>$level[kode_level]</option>";
			}
			echo "
																</select>
																</div>
																<div class='col-md-6'>
															
																<label>Pilih Kelas</label><br>
																<select name='kelas[]' id='soalkelas' class='form-control select2' multiple='multiple' style='width:100%' required='true'>
																																
																</select>
																</div>
																</div>
															</div>
															
																<div class='form-group'>
																<div class='row'>
																<div class='col-md-12'>
																	<label>Pilih Siswa</label><br>
																	<select name='siswa[]' class='form-control select2 ' style='width:100%' multiple >
																		<option value='semua'>Semua Siswa</option>";
			$lev = mysqli_query($koneksi, "SELECT * FROM siswa");
			while ($kelas = mysqli_fetch_array($lev)) {
				if (in_array($kelas['id_siswa'], unserialize($mapel['kelas']))) {
					echo "
																				<option value='$kelas[id_siswa]' selected>$kelas[nama]</option>";
				} else {
					echo "
																			<option value='$kelas[id_siswa]'>$kelas[nama]</option>";
				}
			}
			echo "
																	</select>
																	<span style='color: blue;'>Jika Prog Keahlian,dan Level [ALL]. Lalu pilih Kelas [Khusus],dan tentukan siswanya.</span><br>
																	<span style='color: red;'>*HANYA UNTUK SISWA TERTENTU YANG SUSULAN/BERMASALAH*</span>
																</div>
																</div>
																</div>
															
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-3'>
																<label>Jumlah Soal PG</label>
																<input type='number' id='soalpg' name='jml_soal' class='form-control'  required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Bobot Soal PG %</label>
																<input type='number' name='bobot_pg' class='form-control'  required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Soal Tampil</label>
																<input type='number' id='tampilpg'  name='tampil_pg' class='form-control'  required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Opsi</label>
																<select name='opsi' class='form-control'>
																<option value='3'>3</option>
																<option value='4'>4</option>
																<option value='5'>5</option>
																</select>
																</div>
																</div>
															</div>
															<div class='form-group'>
																<div class='row'>
																<div class='col-md-3'>
																<label>Jumlah Soal Essai</label>
																<input type='number' id='soalesai' name='jml_esai' class='form-control'  required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Bobot Soal Essai %</label>
																<input type='number' name='bobot_esai' class='form-control' required='true'/>
																</div>
																<div class='col-md-3'>
																<label>Soal Tampil</label>
																<input type='number' id='tampilesai' name='tampil_esai' class='form-control'  required='true'/>
																</div>
																<div class='col-md-3'>
																<label>KKM</label>
																<input type='number' id='kkm' name='kkm' class='form-control'  required='true'/>
																</div>
																</div>
															</div>
															<div class='form-group'>
																<div class='row'>
															";
			if ($pengawas['level'] == 'admin') {
				echo "
																
																<div class='col-md-6'>
																<label>Guru Pengampu</label>
																<select name='guru' class='form-control' required='true'>
																";
				$guruku = mysqli_query($koneksi, "SELECT * FROM pengawas where level='guru' order by nama asc");
				while ($guru = mysqli_fetch_array($guruku)) {
					echo "<option value='$guru[id_pengawas]'>$guru[nama]</option>";
				}
				echo "
																</select>
															</div>";
			}
			echo "
															<div class='col-md-6'>
																<label>Status Soal</label>
																<select name='status' class='form-control' required='true'>
																
																	<option value='1'>Aktif</option>
																	<option value='0'>Non Aktif</option>
																</select>
																</div>
																</div>
															</div>	
													
											</div>
											<div class='modal-footer'>
												<button type='submit' name='tambahbanksoal' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Simpan</button>
												
											</div>
											</form>
												<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
											";
		} elseif ($ac == 'input') {
			include 'inputsmk.php';
		} elseif ($ac == 'hapusbank') {
			$exec = mysqli_query($koneksi, "delete from soal where id_mapel='$_GET[id]'");
			$gambar = mysqli_query($koneksi, "select * file_pendukung where id_mapel='$_GET[id]'");
			while ($file = mysqli_fetch_array($gambar)) {
				$path = $homeurl . "/files/" . $file['nama_file'];
				unlink($path);
			}
			$exec = mysqli_query($koneksi, "DELETE FROM file_pendukung WHERE id_mapel='$_GET[id]'");
			jump("?pg=$pg&ac=lihat&id=$_GET[id]");
		} elseif ($ac == 'lihat') {
			$id_mapel = $_GET['id'];

			if (isset($_REQUEST['tambah'])) {
				$sip = $_SERVER['SERVER_NAME'];
				$smax = mysqli_query($koneksi, "select max(qid) as maxi from savsoft_qbank");
				while ($hmax = mysqli_fetch_array($smax)) {
					$jumsoal = $hmax['maxi'];
				}
				$smaop = mysqli_query($koneksi, "select max(oid) as maxop from savsoft_options");
				while ($hmaop = mysqli_fetch_array($smaop)) {
					$jumop = $hmaop['maxop'];
				}
				$b_op = $jumop / $jumsoal;

				$no = 1;
				$sqlcek = mysqli_query($koneksi, "select * from savsoft_qbank");
				while ($r = mysqli_fetch_array($sqlcek)) {
					$s_soal = mysqli_fetch_array(mysqli_query($koneksi, "select * from savsoft_qbank where qid = '$no'"));
					$soal_tanya = $s_soal['question'];
					$l_soal = $s_soal['lid'];
					$c_id = $s_soal['cid'];
					$g_soal = $s_soal['description'];
					$g_soal = str_replace(" ", "", $g_soal);

					$smin = mysqli_query($koneksi, "select min(oid) as mini from savsoft_options where qid = '$no'");
					while ($hmin = mysqli_fetch_array($smin)) {
						$min_op = $hmin['mini'];
					}

					$sqlopc = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no' and oid = '$min_op'");
					$ropc = mysqli_fetch_array($sqlopc);
					$opj1 = $ropc['q_option'];
					$opj1 = str_replace("&ndash;", "-", $opj1);
					$opjs1 = $ropc['score'];
					$fileA = $ropc['q_option_match'];
					$fileA = str_replace(" ", "", $fileA);

					$dele = mysqli_query($koneksi, "delete from savsoft_options where qid = '$no' and oid = '$min_op'");

					$smin = mysqli_query($koneksi, "select min(oid) as mini from savsoft_options where qid = '$no'");
					while ($hmin = mysqli_fetch_array($smin)) {
						$min_op = $hmin['mini'];
					}

					$sqlopc = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no' and oid = '$min_op'");
					$rubah = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no'");
					$ck_jum = mysqli_num_rows($rubah);

					$ropc = mysqli_fetch_array($sqlopc);
					$opj2 = $ropc['q_option'];
					$opj2 = str_replace("&ndash;", "-", $opj2);
					$opjs2 = $ropc['score'];
					$fileB = $ropc['q_option_match'];
					$fileB = str_replace(" ", "", $fileB);


					$dele = mysqli_query($koneksi, "delete from savsoft_options where qid = '$no' and oid = '$min_op'");

					$smin = mysqli_query($koneksi, "select min(oid) as mini from savsoft_options where qid = '$no'");
					while ($hmin = mysqli_fetch_array($smin)) {
						$min_op = $hmin['mini'];
					}

					$sqlopc = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no' and oid = '$min_op'");
					$ropc = mysqli_fetch_array($sqlopc);
					$opj3 = $ropc['q_option'];
					$opj3 = str_replace("&ndash;", "-", $opj3);
					$opjs3 = $ropc['score'];
					$fileC = $ropc['q_option_match'];
					$fileC = str_replace(" ", "", $fileC);


					$dele = mysqli_query($koneksi, "delete from savsoft_options where qid = '$no' and oid = '$min_op'");

					$smin = mysqli_query($koneksi, "select min(oid) as mini from savsoft_options where qid = '$no'");
					while ($hmin = mysqli_fetch_array($smin)) {
						$min_op = $hmin['mini'];
					}

					$sqlopc = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no' and oid = '$min_op'");
					$ropc = mysqli_fetch_array($sqlopc);
					$opj4 = $ropc['q_option'];
					$opj4 = str_replace("&ndash;", "-", $opj4);
					$opjs4 = $ropc['score'];
					$fileD = $ropc['q_option_match'];
					$fileD = str_replace(" ", "", $fileD);

					$dele = mysqli_query($koneksi, "delete from savsoft_options where qid = '$no' and oid = '$min_op'");

					$smin = mysqli_query($koneksi, "select min(oid) as mini from savsoft_options where qid = '$no'");
					while ($hmin = mysqli_fetch_array($smin)) {
						$min_op = $hmin['mini'];
					}

					$sqlopc = mysqli_query($koneksi, "select * from savsoft_options where qid = '$no' and oid = '$min_op'");
					$ropc = mysqli_fetch_array($sqlopc);
					$opj5 = $ropc['q_option'];
					$opj5 = str_replace("&ndash;", "-", $opj5);
					$opjs5 = $ropc['score'];
					$fileE = $ropc['q_option_match'];
					$fileE = str_replace(" ", "", $fileE);


					$dele = mysqli_query($koneksi, "delete from savsoft_options where qid = '$no' and oid = '$min_op'");

					if ($opjs1 == 1) {
						$kunci = "A";
					}
					if ($opjs2 == 1) {
						$kunci = "B";
					}
					if ($opjs3 == 1) {
						$kunci = "C";
					}
					if ($opjs4 == 1) {
						$kunci = "D";
					}
					if ($opjs5 == 1) {
						$kunci = "E";
					}

					if ($ck_jum !== 0) {
						$jns = "1";
					}
					if ($ck_jum == 0) {
						$jns = "2";
					}

					$jwb522 = str_replace("&amp;lt;", "<", $jwb521);
					$jwb422 = str_replace("&amp;lt;", "<", $jwb421);
					$jwb322 = str_replace("&amp;lt;", "<", $jwb321);
					$jwb222 = str_replace("&amp;lt;", "<", $jwb221);
					$jwb122 = str_replace("&amp;lt;", "<", $jwb121);

					$soal_tanya2 = str_replace("&amp;lt;", "<", $soal_tanya);

					$jwb52 = str_replace("&amp;gt;", ">", $jwb522);
					$jwb42 = str_replace("&amp;gt;", ">", $jwb422);
					$jwb32 = str_replace("&amp;gt;", ">", $jwb322);
					$jwb22 = str_replace("&amp;gt;", ">", $jwb222);
					$jwb12 = str_replace("&amp;gt;", ">", $jwb122);

					$soal_tanya = str_replace("&amp;gt;", ">", $soal_tanya2);
					$exec = mysqli_query($koneksi, "INSERT INTO soal (id_mapel,nomor,soal,pilA,pilB,pilC,pilD,pilE,jawaban,jenis,file,file1,fileA,fileB,fileC,fileD,fileE) VALUES ('$id_mapel','$no','$soal_tanya','$opj1','$opj2','$opj3','$opj4','$opj5','$kunci','$jns','$g_soal','$file2','$fileA','$fileB','$fileC','$fileD','$fileE')");
					if ($g_soal <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$g_soal','$id_mapel')");
					}
					if ($fileA <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$fileA','$id_mapel')");
					}
					if ($fileB <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$fileB','$id_mapel')");
					}
					if ($fileC <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$fileC','$id_mapel')");
					}
					if ($fileD <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$fileD','$id_mapel')");
					}
					if ($fileE <> "") {
						$file = mysqli_query($koneksi, "INSERT INTO file_pendukung (nama_file,id_mapel) values ('$fileE','$id_mapel')");
					}
					$no++;
				}

				$hasil2 = mysqli_query($koneksi, "TRUNCATE TABLE savsoft_qbank");
				$hasil2 = mysqli_query($koneksi, "TRUNCATE TABLE savsoft_options");
			}

			$namamapel = mysqli_fetch_array(mysqli_query($koneksi, "select * from mapel where id_mapel='$id_mapel'"));
			if ($namamapel['jml_esai'] == 0) {
				$hide = 'hidden';
			} else {
				$hide = '';
			}
			echo "
								<div class='row'>
									<div class='col-md-12'>
										<div class='box box-solid'>
											<div class='box-header with-border' style='background-color:#A9A9A9'>
												<h3 class='box-title'>Daftar Soal $namamapel[nama]</h3>
												<div class='box-tools pull-right btn-group'>
												
													<a href='?pg=$pg&ac=input&id=$id_mapel&no=1&jenis=1' class='btn btn-sm btn-flat btn-success'><i class='fa fa-plus'></i><span class='hidden-xs'> Tambah</span> PG</a>
													<a href='?pg=$pg&ac=input&id=$id_mapel&no=1&jenis=2' class='btn btn-sm btn-flat btn-success'><i class='fa fa-plus'></i><span class='hidden-xs'> Tambah</span> Essai</a>
													<a class='btn btn-sm btn-flat btn-success' href='soal_excel.php?m=$id_mapel'><i class='fa fa-file-excel-o'></i><span class='hidden-xs'> Excel</span></a>
													<button class='btn btn-sm btn-flat btn-success' onclick=frames['frameresult'].print()><i class='fa fa-print'></i><span class='hidden-xs'> Print</span></button>
													<!-- /.Kode Hapus Soal Per Mapel -->
													&nbsp;&nbsp;&nbsp;<a><button class='btn btn-danger btn-sm' data-toggle='modal' data-target='#hapusbank$soal[id_mapel]'><i class='fa fa-trash'></i>Kosongkan</button></a>
															<iframe name='frameresult' src='cetaksoal.php?id=$id_mapel' style='border:none;width:1px;height:1px;'></iframe>
													
												</div>
									
											</div><!-- /.box-header -->
											
											";
			//kode hapus & alert
			$info = info("Anda yakin akan menghapus soal ini keseluruhan ?");
			if (isset($_POST['hapusbank'])) {
				$exec = mysqli_query($koneksi, "delete from soal where id_mapel='$_GET[id]'");
				(!$exec) ? info("Gagal menyimpan", "NO") : jump("?pg=$pg&ac=lihat&id=$_GET[id]");
			}
			echo "
													<div class='modal fade' id='hapusbank$soal[id]' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header bg-red'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Hapus Soal</h3>
															</div>
													<div class='modal-body'>
													<form action='' method='post'>
													<input type='hidden' id='idu' name='idu' value='$soal[id_soal]'/>
													<div class='callout '>
															<h4>$info</h4>
													</div>
													<div class='modal-footer'>
													<div class='box-tools pull-right btn-group'>
																<button type='submit' name='hapusbank' class='btn btn-sm btn-danger'><i class='fa fa-trash-o'></i> Hapus</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
													</div>	
													</div>
													</form>
													</div>
								
													</div>
														<!-- /.modal-content -->
													</div>
														<!-- /.modal-dialog -->
													</div>
											
									
											<div class='box-body'>
											<div class='table-responsive'>
											<b>A. Soal Pilihan Ganda</b>
												<table  class='table table-bordered table-striped'>
													
													<tbody>";

			$soalq = mysqli_query($koneksi, "SELECT * FROM soal where id_mapel='$id_mapel' and jenis='1' order by nomor ");

			while ($soal = mysqli_fetch_array($soalq)) {

				echo "
															<tr>
																<td style='width:30px'>$soal[nomor]</td>
																<td>";
				if ($soal['file'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$video = array('mp4');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['file']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[file]' style='max-width:200px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[file]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} elseif (in_array($ext, $video)) {
						echo "<video width='100%' height='300px' controls='controls'><source src='$homeurl/files/$soal[file]' type='video/$ext'/>Your browser does not support the video tag.</video>";
					} else {
						echo "File tidak didukung!";
					}
				}
				if ($soal['file1'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['file1']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[file1]' style='max-width:200px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[file1]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} elseif (in_array($ext, $video)) {
						echo "<video width='100%' height='300px' controls='controls'><source src='$homeurl/files/$soal[file1]' type='video/$ext'/>Your browser does not support the video tag.</video>";
					} else {
						echo "File tidak didukung!";
					}
				}
				echo "
																$soal[soal]
																
																<table width=100% border=0>
																<tr>											
																
																<td width=4px valign=top>A.</td>
																<td width=300px colspan=2 valign=top>";
				if ($soal['pilA'] <> '') {
					echo "$soal[pilA]<br>";
				}
				if ($soal['fileA'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['fileA']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[fileA]' style='max-width:100px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[fileA]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} else {
						echo "File tidak didukung!";
					}
				}
				echo "
																
																</td>
										
																<td width=30px valign=top>&nbsp;</td>
																<td width=4px valign=top>C.</td>
																<td width=300px colspan=2 valign=top>";
				if (!$soal['pilC'] == "") {
					echo "$soal[pilC]<br>";
				}
				if ($soal['fileC'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['fileC']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[fileC]' style='max-width:100px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[fileC]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} else {
						echo "File tidak didukung!";
					}
				}
				echo "
																</td>";
				if ($namamapel['opsi'] == 5) {
					echo "
																<td width=30px valign=top>&nbsp;</td>
																<td width=4px valign=top>E.</td>
																<td width=300px colspan=2 valign=top>";
					if (!$soal['pilE'] == "") {
						echo "$soal[pilE]<br>";
					}
					if ($soal['fileE'] <> '') {
						$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
						$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
						$ext = explode(".", $soal['fileE']);
						$ext = end($ext);
						if (in_array($ext, $image)) {
							echo "
																				
																				<img src='$homeurl/files/$soal[fileE]' style='max-width:100px;'/>
																			";
						} elseif (in_array($ext, $audio)) {
							echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[fileE]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
						} else {
							echo "File tidak didukung!";
						}
					}
					echo "
																</td>
																<td width=30px valign=top>&nbsp;</td>";
				}
				echo "
																</tr>
																<tr>
																
																<td width=4px valign=top>B.</td>
																<td width=300px colspan=2 valign=top>";
				if (!$soal['pilB'] == "") {
					echo "$soal[pilB]<br>";
				}
				if ($soal['fileB'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['fileB']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[fileB]' style='max-width:100px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[fileB]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} else {
						echo "File tidak didukung!";
					}
				}
				echo "
																</td>
																";
				if ($namamapel['opsi'] <> 3) {
					echo "
																<td width=30px>&nbsp;</td>
																<td width=4px valign=top>D.</td>
																<td width=300px colspan=2 valign=top>";
					if (!$soal['pilD'] == "") {
						echo "$soal[pilD]<br>";
					}
					if ($soal['fileD'] <> '') {
						$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
						$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
						$ext = explode(".", $soal['fileD']);
						$ext = end($ext);
						if (in_array($ext, $image)) {
							echo "
																				
																				<img src='$homeurl/files/$soal[fileD]' style='max-width:100px;'/>
																			";
						} elseif (in_array($ext, $audio)) {
							echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[fileD]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
						} else {
							echo "File tidak didukung!";
						}
					}
					echo "
																
																</td>";
				}
				echo "
																<tr>
																<td width=300px colspan=2 valign=top>
																<b>Jawaban: $soal[jawaban]</b> 
																<img src='$homeurl/dist/img/benar.png' height='20px'>
																</td>
																</tr>
																</table>
																		
																</td>
																<td style='width:30px'>
																<a href='?pg=$pg&ac=input&id=$id_mapel&no=$soal[nomor]&jenis=1' class='btn btn-sm btn-primary'><i class='fa fa-pencil-square-o'></i></a>
																<br><br>
																<a><button class='btn bg-maroon btn-sm' data-toggle='modal' data-target='#hapuspg$soal[id_soal]'><i class='fa fa-trash-o'></i></button></a>
																</td>
																</tr>
													";
				$info = info("Anda yakin akan menghapus soal ini  ?");
				if (isset($_POST['hapuspg'])) {
					$exec = mysqli_query($koneksi, "DELETE  FROM soal WHERE id_soal = '$_REQUEST[idu]'");
					(!$exec) ? info("Gagal menyimpan", "NO") : jump("?pg=$pg&ac=$ac&id=$id_mapel");
				}
				echo "
													<div class='modal fade' id='hapuspg$soal[id_soal]' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header bg-maroon'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Hapus Soal</h3>
															</div>
													<div class='modal-body'>
													<form action='' method='post'>
													<input type='hidden' id='idu' name='idu' value='$soal[id_soal]'/>
													<div class='callout '>
															<h4>$info</h4>
													</div>
													<div class='modal-footer'>
													<div class='box-tools pull-right btn-group'>
																<button type='submit' name='hapuspg' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Hapus</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
													</div>	
													</div>
													</form>
													</div>
								
													</div>
														<!-- /.modal-content -->
													</div>
														<!-- /.modal-dialog -->
													</div>
														
														";
			}
			echo "
													</tbody>
												</table>
												<b>B. Soal Essai</b>
												<table  class='table table-bordered table-striped'>
													
													<tbody>";

			$soalq = mysqli_query($koneksi, "SELECT * FROM soal where id_mapel='$id_mapel' and jenis='2' order by nomor ");

			while ($soal = mysqli_fetch_array($soalq)) {

				echo "
															<tr>
																<td style='width:30px'>$soal[nomor]</td>
																<td>";
				if ($soal['file'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$video = array('mp4');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['file']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[file]' style='max-width:200px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[file]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} elseif (in_array($ext, $video)) {
						echo "<video width='300px' height='300px' controls='controls'><source src='$homeurl/files/$soal[file]' type='video/$ext'/>Your browser does not support the video tag.</video>";
					} else {
						echo "File tidak didukung!";
					}
				}
				if ($soal['file1'] <> '') {
					$audio = array('mp3', 'wav', 'ogg', 'MP3', 'WAV', 'OGG');
					$image = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP');
					$ext = explode(".", $soal['file1']);
					$ext = end($ext);
					if (in_array($ext, $image)) {
						echo "
																				
																				<img src='$homeurl/files/$soal[file1]' style='max-width:200px;'/>
																			";
					} elseif (in_array($ext, $audio)) {
						echo "
																				
																				<audio controls><source src='$homeurl/files/$soal[file1]' type='audio/$ext'>Your browser does not support the audio tag.</audio>
																			";
					} elseif (in_array($ext, $video)) {
						echo "<video width='300px' height='300px' controls='controls'><source src='$homeurl/files/$soal[file1]' type='video/$ext'/>Your browser does not support the video tag.</video>";
					} else {
						echo "File tidak didukung!";
					}
				}
				echo "
																$soal[soal]
																
		
																</td>
																<td style='width:30px'>
																<a href='?pg=$pg&ac=input&id=$id_mapel&no=$soal[nomor]&jenis=2' class='btn btn-sm btn-primary'><i class='fa fa-pencil-square-o'></i></a>
																<br><br>
																<a><button class='btn bg-maroon btn-sm' data-toggle='modal' data-target='#hapusessay$soal[id_soal]'><i class='fa fa-trash-o'></i></button></a>
																</td>
																</tr>
														";
				$info = info("Anda yakin akan menghapus soal ini  ?");
				if (isset($_POST['hapusessay'])) {
					$exec = mysqli_query($koneksi, "DELETE  FROM soal WHERE id_soal = '$_REQUEST[idu]'");
					(!$exec) ? info("Gagal menyimpan", "NO") : jump("?pg=$pg&ac=$ac&id=$id_mapel");
				}
				echo "
													<div class='modal fade' id='hapusessay$soal[id_soal]' style='display: none;'>
													<div class='modal-dialog'>
													<div class='modal-content'>
													<div class='modal-header bg-maroon'>
													<button  class='close' data-dismiss='modal'><span aria-hidden='true'><i class='glyphicon glyphicon-remove'></i></span></button>
															<h3 class='modal-title'>Hapus Soal</h3>
															</div>
													<div class='modal-body'>
													<form action='' method='post'>
													<input type='hidden' id='idu' name='idu' value='$soal[id_soal]'/>
													<div class='callout callout-warning'>
															<h4>$info</h4>
													</div>
													<div class='modal-footer'>
													<div class='box-tools pull-right btn-group'>
																<button type='submit' name='hapusessay' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Hapus</button>
																<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Close</button>
													</div>	
													</div>
													</form>
													</div>
								
													</div>
														<!-- /.modal-content -->
													</div>
														<!-- /.modal-dialog -->
													</div>
														";
			}
			echo "
													</tbody>
												</table>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box -->
									</div>
								</div>
								
						";
		} elseif ($ac == 'hapusfile') {
			$jenis = $_GET['jenis'];
			$id = $_GET['id'];
			$file = $_GET['file'];
			$soal = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM soal WHERE id_soal='$id'"));
			(file_exists("../files/" . $soal[$file])) ? unlink("../files/" . $soal[$file]) : null;
			mysqli_query($koneksi, "UPDATE soal SET $file='' WHERE id_soal='$id'");
			jump("?pg=$pg&ac=input&paket=$soal[paket]&id=$soal[id_mapel]&no=$soal[nomor]&jenis=$jenis");
		} elseif ($ac == 'importsoal') {
			$id_mapel = $_GET['id'];
			$mapelQ = mysqli_query($koneksi, "SELECT * FROM mapel where id_mapel='$id_mapel'");
			$mapel = mysqli_fetch_array($mapelQ);
			$cekmapel = mysqli_num_rows($mapelQ);
			if (isset($_POST['submit'])) {
				$file = $_FILES['file']['name'];
				$temp = $_FILES['file']['tmp_name'];
				$ext = explode('.', $file);
				$ext = end($ext);
				if ($ext <> 'xls') {
					$info = info('Gunakan file Ms. Excel 93-2007 Workbook (.xls)', 'NO');
				} else {

					$data = new Spreadsheet_Excel_Reader($temp);
					$hasildata = $data->rowcount($sheet_index = 0);
					$sukses = $gagal = 0;
					$exec = mysqli_query($koneksi, "delete from soal where id_mapel='$id_mapel' ");
					for ($i = 2; $i <= $hasildata; $i++) {
						$no = $data->val($i, 1);
						$soal = addslashes($data->val($i, 2));
						$pilA = addslashes($data->val($i, 3));
						$pilB = addslashes($data->val($i, 4));
						$pilC = addslashes($data->val($i, 5));
						$pilD = addslashes($data->val($i, 6));
						$pilE = addslashes($data->val($i, 7));
						$jawaban = $data->val($i, 8);
						$jenis = $data->val($i, 9);
						$file1 = $data->val($i, 10);
						$file2 = $data->val($i, 11);
						$fileA = $data->val($i, 12);
						$fileB = $data->val($i, 13);
						$fileC = $data->val($i, 14);
						$fileD = $data->val($i, 15);
						$fileE = $data->val($i, 16);
						$id_mapel = $_POST['id_mapel'];

						if ($soal <> '' and $jenis <> '') {
							$exec = mysqli_query($koneksi, "INSERT INTO soal (id_mapel,nomor,soal,pilA,pilB,pilC,pilD,pilE,jawaban,jenis,file,file1,fileA,fileB, fileC,fileD,fileE) VALUES ('$id_mapel','$no','$soal','$pilA','$pilB','$pilC','$pilD','$pilE','$jawaban','$jenis','$file1','$file2','$fileA','$fileB','$fileC','$fileD','$fileE')");
							if ($file1 <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$file1','$id_mapel')");
							}
							if ($file2 <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$file2','$id_mapel')");
							}
							if ($fileA <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileA','$id_mapel')");
							}
							if ($fileB <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileB','$id_mapel')");
							}
							if ($fileC <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileC','$id_mapel')");
							}
							if ($fileD <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileD','$id_mapel')");
							}
							if ($fileE <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileE','$id_mapel')");
							}
							($exec) ? $sukses++ : $gagal++;
						} else {
							$gagal++;
						}
					}
					$total = $hasildata - 1;
					$info = info("Berhasil: $sukses | Gagal: $gagal | Dari: $total", 'OK');
				}
			}

			if (isset($_POST['importbee'])) {
				$file = $_FILES['file']['name'];
				$temp = $_FILES['file']['tmp_name'];
				$ext = explode('.', $file);
				$ext = end($ext);
				if ($ext <> 'xls') {
					$info = info('Gunakan file Ms. Excel 93-2007 Workbook (.xls)', 'NO');
				} else {

					$data = new Spreadsheet_Excel_Reader($temp);
					$hasildata = $data->rowcount($sheet_index = 0);
					$sukses = $gagal = 0;
					$exec = mysqli_query($koneksi, "delete from soal where id_mapel='$id_mapel' ");
					for ($i = 3; $i <= $hasildata; $i++) {
						$no = $data->val($i, 1);
						$soal = addslashes($data->val($i, 5));
						$pilA = addslashes($data->val($i, 6));
						$pilB = addslashes($data->val($i, 8));
						$pilC = addslashes($data->val($i, 10));
						$pilD = addslashes($data->val($i, 12));
						$pilE = addslashes($data->val($i, 14));
						$jawab = $data->val($i, 19);
						if ($jawab == '1') {
							$jawaban = 'A';
						} elseif ($jawab == '2') {
							$jawaban = 'B';
						} elseif ($jawab == '3') {
							$jawaban = 'C';
						} elseif ($jawab == '4') {
							$jawaban = 'D';
						} elseif ($jawab == '5') {
							$jawaban = 'E';
						}
						$jenis = $data->val($i, 2);
						$file1 = $data->val($i, 18);
						$file2 = $data->val($i, 17);
						$fileA = $data->val($i, 7);
						$fileB = $data->val($i, 9);
						$fileC = $data->val($i, 11);
						$fileD = $data->val($i, 13);
						$fileE = $data->val($i, 15);
						$id_mapel = $_POST['id_mapel'];

						if ($jenis <> '') {
							$exec = mysqli_query($koneksi, "INSERT INTO soal (id_mapel,nomor,soal,pilA,pilB,pilC,pilD,pilE,jawaban,jenis,file,file1,fileA,fileB, fileC,fileD,fileE) VALUES ('$id_mapel','$no','$soal','$pilA','$pilB','$pilC','$pilD','$pilE','$jawaban','$jenis','$file1','$file2','$fileA','$fileB','$fileC','$fileD','$fileE')");
							($exec) ? $sukses++ : $gagal++;
							if ($file1 <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$file1','$id_mapel')");
							}
							if ($file2 <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$file2','$id_mapel')");
							}
							if ($fileA <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileA','$id_mapel')");
							}
							if ($fileB <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileB','$id_mapel')");
							}
							if ($fileC <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileC','$id_mapel')");
							}
							if ($fileD <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileD','$id_mapel')");
							}
							if ($fileE <> '') {
								$sql = mysqli_query($koneksi, "insert into file_pendukung (nama_file,id_mapel) values ('$fileE','$id_mapel')");
							}
						} else {
							$gagal++;
						}
					}
					$total = $hasildata - 1;
					$info = info("Berhasil: $sukses", 'OK');
				}
			}



			echo "
								<div class='row'>
									<div class='col-md-6'>
                                        <form action='' method='post' enctype='multipart/form-data'>
                                            <div class='box '>
                                                <div class='box-header with-border' style='background-color:grey'>
                                                    <h3 class='box-title'>Import Soal Excel</h3>
                                                    <div class='box-tools pull-right btn-group'>
																										<a href='importdatasoal.xls' class='btn btn-sm btn-primary'> Download Format</a>
                                                        <button type='submit' name='submit' class='btn btn-sm btn-success'><i class='fa fa-check'></i> Import</button>
														<a href='?pg=$pg' class='btn btn-sm btn-danger' title='Batal'><i class='fa fa-times'></i></a>
                                                    </div>
                                                </div><!-- /.box-header -->
                                                <div class='box-body'>
												
														$info
														<div class='form-group'>
															<label>Mata Pelajaran</label>
															<input type='hidden' name='id_mapel' class='form-control' value='$mapel[id_mapel]'/>
															<input type='text' name='mapel' class='form-control' value='$mapel[nama]' disabled/>
															
														</div>
													
                                                    <div class='form-group'>
                                                        <label>Pilih File</label>
                                                        <input type='file' name='file' class='form-control' required='true'/>
                                                    </div>
                                                    <p style='margin-bottom:18px'>
                                                        Sebelum meng-import pastikan file yang akan anda import sudah dalam bentuk Ms. Excel 97-2003 Workbook (.xls) dan format penulisan harus sesuai dengan yang telah ditentukan. <br/>
                                                    </p>
                                                </div><!-- /.box-body -->
                                                <!--<div class='box-footer'>
                                                    <a href='importdatasoal.xls'><i class='fa fa-file-excel-o'></i> Download Format</a>
                                                </div>-->
                                            </div><!-- /.box -->
                                        </form>
                                    </div>
                            ";
			include 'filesoal.php';
			echo '</div>';
		} elseif ($ac == 'duplikat') {
			include "duplikasi_soal.php";
		}
	} elseif ($pg == 'editguru') {
		if (isset($_POST['submitguru'])) {
			$info1 = $info2 = $info3 = $info4 = '';
			$username = $_POST['username'];
			$nip = $_POST['nip'];
			$nama = $_POST['nama'];
			$nama = str_replace("'", "&#39;", $nama);
			$exec = mysqli_query($koneksi, "update pengawas set username='$username', nama='$nama',nip='$nip',password='$_POST[password]' where id_pengawas='$id_pengawas'");
		}

		if ($ac == '') {
			$guru = mysqli_fetch_array(mysqli_query($koneksi, "select * from pengawas where id_pengawas='$pengawas[id_pengawas]'"));
			echo "
								<div class='row'>
                                	<div class='col-md-3'>
                                	<div class='box box-solid'>
                                		<div class='box-body box-profile'>
                                			 <!--<input type='file' name='fotoguru' class='form-control'/> --!>
											 <div class='group'>
												<div class='right'>
														 <div class='panel panel-info' style='padding-top:20'>
																	<div class='panel-heading' style=' text-align:center'>
																		Upload Photo : 
																	</div>
																	<div class='panel-body'>
																	  
																	<!-- Upload Button, use any id you wish-->
																	<div id='upload' style='text-align:center'><input type='image' name='uploadguru' src='../foto/fotoguru/$guru[nama].jpg' width='100'/></div><span id='status1' ></span>
																	</div>
																	<h3 class='profile-username text-center'>$guru[nama]</h3>
																	<div class='panel-footer' style=' text-align:center'>Update Foto Anda Kemudain Refresh / Reload (F5)
																	</div>
														   
														</div>


															

												</div>	
											</div> 
                                		</div>				
                                		</div>
                                	</div>
                                	
                                	<div class='col-md-9'>
                            		<div class='nav-tabs-custom'>
                                        <ul class='nav nav-tabs'>
                                          <li class='active'><a aria-expanded='true' href='#detail' data-toggle='tab'><i class='fa fa-user'></i> Detail Profile</a></li>
                            			<!--  <li><a aria-expanded='true' href='#alamat' data-toggle='tab'><i class='fa fa-sign-in'></i> <span class='hidden-xs'>Login History</span></a></li>
                            			  <li><a aria-expanded='true' href='#kesehatan' data-toggle='tab'><i class='fa fa-download'></i> <span class='hidden-xs'>Recent Download</span></a></li>
                            			  -->
                                        </ul>
                                        <div class='tab-content'>
                                          <div class='tab-pane active' id='detail'>
										  $info1
                            						<div class='row margin-bottom'>
													<form action='' method='post'>
                            							<div class='col-sm-12'>
														
                                                      <table class='table table-striped table-bordered'>
                                                      <tbody>
                                                        
                                                        <tr><th scope='row'>Nama Lengkap</th> <td><input class='form-control' name='nama' value='$guru[nama]'/></td></tr>
                                                        <tr><th scope='row'>Nip</th> <td><input class='form-control' name='nip' value='$guru[nip]'/></td></tr>
                                                        <tr><th scope='row'>Username</th> <td><input class='form-control' name='username' value='$guru[username]' /></td></tr>
                                                        <tr><th scope='row'>Password</th> <td><input class='form-control' name='password' value='$guru[password]'/></td></tr>
														<!--<tr><th scope='row'>Upload Foto</th><td><input type='file' name='fotoguru' class='form-control'/></td>--!>
													</tbody>
                        </table>
                                         
														<button name='submitguru' class='btn btn-sm btn-flat btn-success pull-right'>Perbarui Data </button>
                                                       </div>
                            						   </form>
                            						</div>
                            				</div>
                            				 <div class='tab-pane' id='alamat'>
                            						<div class='row margin-bottom'>
                            						<div class='col-sm-12'>
                                                      <table class='table  table-striped no-margin'>
                                                      <tbody>
                            							
                                                      </tbody>
                                                      </table>
                                                    </div>
                            						</div>
                            				</div>
                            				 <div class='tab-pane' id='kesehatan'>
                            						<div class='row margin-bottom'>
                            						<div class='col-sm-12'>
                                                      <table class='table  table-striped no-margin' >
                                                      <tbody>
                            						
                                                        
                                                      </tbody>
                                                      </table>
                                                    </div>
                            						</div>
                            				</div>
                            				 
                                          
                                        </div>
                                        <!-- /.tab-content -->
                            		</div>
                                </div> <!--row-->";
		}
	} elseif ($pg == 'reset') {

		$info = '';
		echo "
								<div class='row'>
									<div class='col-md-12'>
                                        
                                            <div class='box box-solid'>
                                                <div class='box-header with-border' style='background-color:#A9A9A9'>
                                                    <h3 class='box-title'>Reset Login Peserta</h3>
                                                    <div class='box-tools pull-right btn-group'>
                                                        <button id='btnresetlogin' class='btn btn-sm btn-flat btn-success'><i class='fa fa-check'></i> Reset Login</button>
														
                                                    </div>
                                                </div><!-- /.box-header -->
                                                <div class='box-body'>
													$info
													<div id='tablereset' class='table-responsive'>
														<table id='example1' class='table table-bordered table-striped'>
															<thead>
																<tr><th width='5px'><input type='checkbox' id='ceksemua'  ></th>
																	<th width='5px'>#</th>
																	<th >No Peserta</th>
																	<th >Nama Peserta</th>
																	<th>Tanggal Login</th>
																	
																</tr>
															</thead>
															<tbody>";
		$loginQ = mysqli_query($koneksi, "SELECT * FROM login ORDER BY date DESC");
		while ($login = mysqli_fetch_array($loginQ)) {
			$siswa = mysqli_fetch_array(mysqli_query($koneksi, "select * from siswa where id_siswa='$login[id_siswa]'"));
			$no++;
			echo "
																	<tr><td><input type='checkbox' name='cekpilih[]' class='cekpilih' id='cekpilih-$no' value='$login[id_log]' ></td>
																		<td>$no</td>
																		<td>$siswa[no_peserta]</td>
																		<td>$siswa[nama]</td>
																		<td>$login[date]</td>
																		
																	</tr>
																";
		}
		echo "
															</tbody>
														</table>
													</div>
														
                                                </div><!-- /.box-body -->
                                            </div><!-- /.box -->
                                        
                                    </div>
								</div>";
	} elseif ($pg == 'pengaturan') {
		cek_session_admin();
		$info1 = $info2 = $info3 = $info4 = '';
		if (isset($_POST['submit1'])) {
			$alamat = nl2br($_POST['alamat']);
			$header = nl2br($_POST['header']);
			$exec = mysqli_query($koneksi, "UPDATE setting SET aplikasi='$_POST[aplikasi]',nama_ujian='$_POST[nama_ujian]',sekolah='$_POST[sekolah]',kode_sekolah='$_POST[kode]',jenjang='$_POST[jenjang]',kepsek='$_POST[kepsek]',nip='$_POST[nip]',proktor='$_POST[proktor]',nip_proktor='$_POST[nip_proktor]',no_proktor='$_POST[no_proktor]',
								alamat='$alamat',kecamatan='$_POST[kecamatan]',kota='$_POST[kota]',provinsi='$_POST[provinsi]',telp='$_POST[telp]',fax='$_POST[fax]',web='$_POST[web]',email='$_POST[email]',header='$header',ip_server='$_POST[ipserver]',waktu='$_POST[waktu]' WHERE id_setting='1'");
			if ($exec) {
				$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
				if ($_FILES['logo']['name'] <> '') {
					$logo = $_FILES['logo']['name'];
					$temp = $_FILES['logo']['tmp_name'];
					$ext = explode('.', $logo);
					$ext = end($ext);
					$dest = 'dist/img/logo' . rand(1, 100) . '.' . $ext;
					$upload = move_uploaded_file($temp, '../' . $dest);
					if ($upload) {
						$exec = mysqli_query($koneksi, "UPDATE setting SET logo='$dest' WHERE id_setting='1'");
						$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
					} else {
						$info1 = info('Gagal menyimpan pengaturan!', 'NO');
					}
				}
			}
			if ($exec) {
				$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
				if ($_FILES['foto']['name'] <> '') {
					$foto = $_FILES['foto']['name'];
					$temp = $_FILES['foto']['tmp_name'];
					$ext = explode('.', $foto);
					$ext = end($ext);
					$dest = 'dist/img/foto' . rand(1, 100) . '.' . $ext;
					$upload = move_uploaded_file($temp, '../' . $dest);
					if ($upload) {
						$exec = mysqli_query($koneksi, "UPDATE setting SET foto='$dest' WHERE id_setting='1'");
						$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
					} else {
						$info1 = info('Gagal menyimpan pengaturan!', 'NO');
					}
				}
			}
			if ($exec) {
				$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
				if ($_FILES['ttd']['name'] <> '') {
					$ttd = $_FILES['ttd']['name'];
					$temp = $_FILES['ttd']['tmp_name'];
					$ext = explode('.', $ttd);
					$ext = end($ext);
					$dest = 'dist/img/ttd' . rand(1, 100) . '.' . $ext;
					$upload = move_uploaded_file($temp, '../' . $dest);
					if ($upload) {
						$exec = mysqli_query($koneksi, "UPDATE setting SET ttd='$dest' WHERE id_setting='1'");
						$info1 = info('Berhasil menyimpan pengaturan!', 'OK');
					} else {
						$info1 = info('Gagal menyimpan pengaturan!', 'NO');
					}
				}
			} else {
				$info1 = info('Gagal menyimpan pengaturan!', 'NO');
			}
		}

		if (isset($_POST['submit3'])) {
			$password = $_POST['password'];
			if (!password_verify($password, $pengawas['password'])) {
				$info4 = info('Password salah!', 'NO');
			} else {
				if (!empty($_POST['data'])) {
					$data = $_POST['data'];
					if ($data <> '') {
						foreach ($data as $table) {
							if ($table <> 'pengawas') {
								mysqli_query($koneksi, "TRUNCATE $table");
							} else {
								mysqli_query($koneksi, "DELETE FROM $table WHERE level!='admin'");
							}
						}
						$info4 = info('Data terpilih telah dikosongkan!', 'OK');
					}
				}
			}
		}
		$admin = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM pengawas WHERE level='admin' AND id_pengawas='1'"));
		$setting = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM setting WHERE id_setting='1'"));
		$setting['alamat'] = str_replace('<br />', '', $setting['alamat']);
		$setting['header'] = str_replace('<br />', '', $setting['header']);

		$tokenapi = $setting['db_token'];

		echo "
								<div class='row'>
						<div class='col-md-12'>
							<div class='box box-widget widget-user-2'>
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class='widget-user-header bg-purple'>
									<div class='widget-user-image'>
										<img class='img-circle' src='$homeurl/dist/img/icon/settings.svg' alt='User Avatar'>
									</div>
									<!-- /.widget-user-image -->
									<h3 class='widget-user-username'>Pengaturan</h3>
									<h5 class='widget-user-desc'>Pengaturan Aplikasi</h5>
								</div>
								<div class='box-footer no-padding '>
									<div class='nav-tabs-custom'>
										<ul class='nav nav-tabs'>
											<li class='active'><a href='#tab_1' data-toggle='tab' aria-expanded='true'>Pengaturan Umum</a></li>
											<li class=''><a href='#tab_2' data-toggle='tab' aria-expanded='false'>Hapus Data</a></li>
											<li class=''><a href='#tab_3' data-toggle='tab' aria-expanded='false'>Backup & Restore</a></li>";
		if ($setting['server'] == 'pusat') {
			if ($pengawas['level'] == 'admin') {
				echo "
											<!-- <li class=''><a href='#tab_4' data-toggle='tab' aria-expanded='false'>Token API</a></li>-->";
			}
		}
		echo "
										</ul>
										<div class='tab-content'>
											<div class='tab-pane active' id='tab_1'>
												<form action='' method='post' enctype='multipart/form-data'>

													<div class='box-body'>
														<button type='submit' name='submit1' class='btn btn-flat pull-right btn-success' style='margin-bottom:5px'><i class='fa fa-check'></i> Simpan</button>
													$info1
														<div class='form-group'>
															<label>Nama Aplikasi</label>
														<input type='text' name='aplikasi' value='$setting[aplikasi]' class='form-control' required='true'/>
														</div>
														<div class='form-group'>
															<label>Nama Ujian</label>
														<input type='text' name='nama_ujian' value='$setting[nama_ujian]' class='form-control' required='true'/>
														</div>
														<div class='form-group'>
															<div class='row'>
																<div class='col-md-6'>
														<label>Nama Sekolah</label>
														<input type='text' name='sekolah' value='$setting[sekolah]' class='form-control' required='true'/>
														</div>
														<div class='col-md-6'>
														<label>Kode Sekolah</label>
														<input type='text' name='kode' value='$setting[kode_sekolah]' class='form-control' required='true'/>
														</div>
															</div>
														</div>
														<div class='form-group'>
															<div class='row'>
																<div class='col-md-6'>
																	<label>Alamat Server / Ip Server</label>
																	<input type='text' name='ipserver' value='$setting[ip_server]' class='form-control'/>
																</div>
																<div class='col-md-6'>
																	<label>Waktu Server</label>
																	<select name='waktu' class='form-control' required='true'>
																		<option value='$setting[waktu]'>$setting[waktu]</option>
																		<option value='Asia/Jakarta'>Asia/Jakarta</option>
																		<option value='Asia/Makassar'>Asia/Makassar</option>
																		<option value='Asia/Jayapura'>Asia/Jayapura</option>
																	</select>
																</div>
															</div>
														</div>
														<div class='form-group'>
															<label>Jenjang</label>
															<select name='jenjang' class='form-control' required='true'>
																<option value='$setting[jenjang]'>$setting[jenjang]</option>
																<option value='SD'>SD/MI</option>
																<option value='SMP'>SMP/MTS</option>
																<option value='SMK'>SMK/SMA/MA</option>
															</select>
														</div>
													<div class='form-group'>
														<label>Kepala Sekolah</label>
														<input type='text' name='kepsek' value='$setting[kepsek]' class='form-control'/>
													</div>
													<div class='form-group'>
														<label>NIP Kepala Sekolah</label>
														<input type='text' name='nip' value='$setting[nip]' class='form-control'/>
													</div>
													<div class='form-group'>
														<div class='row'>
															<div class='col-md-4'>
																<label>Nama Proktor</label>
																<input type='text' name='proktor' value='$setting[proktor]' class='form-control'/>
															</div>
															<div class='col-md-4'>
																<label>NIP Proktor</label>
																<input type='text' name='nip_proktor' value='$setting[nip_proktor]' class='form-control'/>
															</div>
															<div class='col-md-4'>
																<label>Nomor Proktor</label>
																<input type='text' name='no_proktor' value='$setting[no_proktor]' class='form-control'/>
															</div>
														</div>
													</div>
													<div class='form-group'>
														<label>Alamat</label>
														<textarea name='alamat' class='form-control' rows='3'>$setting[alamat]</textarea>
													</div>
													
													<div class='form-group'>
														<div class='row'>
															<div class='col-md-6'>
																<label>Provinsi</label>
																<input type='text' name='provinsi' value='$setting[provinsi] ' class='form-control' />
															</div>
															<div class='col-md-6'>
																<label>Kota/Kabupaten</label>
																<input type='text' name='kota' value='$setting[kota]' class='form-control' />
															</div>
														</div>
													</div>
													<div class='form-group'>
														<label>Kecamatan</label>
														<input type='text' name='kecamatan' value='$setting[kecamatan]' class='form-control' />
													</div>

													<div class='form-group'>
														<div class='row'>
															<div class='col-md-6'>
																<label>Telepon</label>
																<input type='text' name='telp' value='$setting[telp]' class='form-control'/>
															</div>
															<div class='col-md-6'>
																<label>Fax</label>
																<input type='text' name='fax' value='$setting[fax]' class='form-control'/>
															</div>
														</div>
													</div>
													<div class='form-group'>
														<div class='row'>
															<div class='col-md-6'>
																<label>Website</label>
																<input type='text' name='web' value='$setting[web]' class='form-control'/>
															</div>
															<div class='col-md-6'>
																<label>E-mail</label>
																<input type='text' name='email' value='$setting[email]' class='form-control'/>
															</div>
														</div>
													</div>
													<div class='form-group'>
														<div class='row'>
															<div class='col-md-4'>
																<label>Upload Logo</label>
																<input type='file' name='logo' class='form-control'/>
																<img class='img img-responsive' width='100px' src='$homeurl/$setting[logo]'height='50'/>
															</div>
															<div class='col-md-4'>
																<label>Upload Logo Pendidikan</label>
																<input type='file' name='foto' class='form-control'/>
																<img class='img img-responsive' width='100px' src='$homeurl/$setting[foto]'/>
															</div>
															<div class='col-md-4'>
																<label>Upload Ttd</label>
																<input type='file' name='ttd' class='form-control'/>
																<img class='img img-responsive' width='100px' src='$homeurl/$setting[ttd]'/>
															</div>
														</div>
													</div>
													<div class='form-group'>
														<label>Header Laporan</label>
														<textarea name='header' class='form-control' rows='3'>$setting[header]</textarea>
													</div>
													</div><!-- /.box-body -->
												</form>
											</div>
											<!-- /.tab-pane -->
											<div class='tab-pane' id='tab_2'>
												<form action='' method='post'>
													<div class='box-body'>
														$info4

														<div class='form-group'>
															<label>Pilih Data</label>
															<div class='row'>
																<div class='col-md-5'>
																	<div class='checkbox'>
																		<small class='label bg-purple'>Pilih Data Hasil Nilai</small><br />
																		<label><input type='checkbox' name='data[]' value='nilai' /> Data Nilai</label><br />
																		<label><input type='checkbox' name='data[]' value='hasil_nilai' /> Draf Nilai</label><br />
																		<label><input type='checkbox' name='data[]' value='hasil_jawaban' /> Data Jawaban</label><br />
																		<label><input type='checkbox' name='data[]' value='jawaban' /> Temp_Jawaban</label><br />
																		<small class='label bg-green'>Pilih Data Ujian</small><br />
																		<label><input type='checkbox' name='data[]' value='soal' /> Data Soal</label><br />
																		<label><input type='checkbox' name='data[]' value='mapel' /> Data Bank Soal</label><br />
																		<label><input type='checkbox' name='data[]' value='ujian' /> Data Jadwal Ujian</label><br />
																		<label><input type='checkbox' name='data[]' value='berita' /> Data Berita Acara</label><br />
																		<label><input type='checkbox' name='data[]' value='pengacak' /> Data Pengacak Soal</label><br />
																		<label><input type='checkbox' name='data[]' value='pengacakopsi' /> Data Pengacak Opsi</label><br />
																		<label><input type='checkbox' name='data[]' value='file_pendukung' /> Data File Pendukung</label><br />

																		<small class='label label-danger'>Pilih Data Master</small><br />
																		<label><input type='checkbox' name='data[]' value='siswa' /> Data Siswa</label><br />
																		<label><input type='checkbox' name='data[]' value='kelas' /> Data Kelas</label><br />
																		<label><input type='checkbox' name='data[]' value='mata_pelajaran' /> Data Mata Pelajaran</label><br />
																		<label><input type='checkbox' name='data[]' value='pk' /> Data Jurusan</label><br />
																		<label><input type='checkbox' name='data[]' value='level' /> Data Level</label><br />
																		<label><input type='checkbox' name='data[]' value='ruang' /> Data Ruangan</label><br />
																		<label><input type='checkbox' name='data[]' value='sesi' /> Data Sesi</label><br />

																	</div>
																</div>
																<div class='col-md-7'>
																	<button type='submit' name='submit3' class='btn btn-sm bg-maroon'><i class='fa fa-trash-o'></i> Kosongkan</button>
																	<div class='form-group'>
																		<label>Password Admin</label>
																		<input type='password' name='password' class='form-control' required='true' />
																	</div>

																	<p class='text-danger'><i class='fa fa-warning'></i> <strong>Mohon di ingat!</strong> Data yang telah dikosongkan tidak dapat dikembalikan.</p>
																</div>
															</div>
														</div>
													</div><!-- /.box-body -->
												</form>
											</div>
											<!-- /.tab-pane -->
											<div class='tab-pane' id='tab_3'>
												<div class='col-md-12 notif'></div>
												<div class='col-md-6'><BR />
													<div class='box box-solid'>
														<div class='box-header' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Backup Data</h3>
														</div><!-- /.box-header -->
														<div class='box-body'>
															<p>Klik Tombol dibawah ini untuk membackup database </p>
															<button id='btnbackup' class='btn btn-flat btn-success'><i class='fa fa-database'></i> Backup Data</button>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</div>
												<div class='col-md-6'><BR />
													<div class='box box-solid'>
														<div class='box-header' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Restore Data</h3>
														</div><!-- /.box-header -->
														<div class='box-body'>
															<form id='formrestore'>
																<p>Klik Tombol dibawah ini untuk merestore database </p>
																<p  style='color:red'>Jika Database Size nya lebih dari 1Mb lebih baik Restore menggunakan HeidiSQL/SQLYoug </p>
																<div class='col-md-8'>
																	<input class='form-control' name='datafile' type='file' />
																</div>
																<button name='restore' class='btn btn-flat btn-success'><i class='fa fa-database'></i> Restore Data</button>
															</form>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</div>
												<div class='col-md-12 notif_mapel'>
													<div class='box box-solid'>
														<div class='box-header' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Backup Mapel Yang Tersedia</h3>
														</div><!-- /.box-header -->
														<div class='box-body'>
														<div class='col-md-12'>
															<select name='mapel_id' id='mapel_id' class='form-control select2' style='width: 100%;' required>";
		//backup_mapel
		$mapelbackup = mysqli_query($koneksi, "SELECT a.id_mapel, b.kode_mapel, b.nama_mapel FROM mapel a INNER JOIN mata_pelajaran b ON a.nama = b.kode_mapel INNER JOIN soal c ON a.id_mapel = c.id_mapel GROUP BY c.id_mapel ASC");
		while ($mapelb = mysqli_fetch_array($mapelbackup)) {
			echo "
																	<option value='$mapelb[id_mapel]" . "-" . "$mapelb[kode_mapel]'>$mapelb[id_mapel]. $mapelb[nama_mapel]</option>";
		}
		echo "
															</select>
															<div class='panel-footer clearfix'>
																<div class='pull-right'>
																	<button id='mastersoal' class='btn btn-flat btn-success'><i class='fa fa-database'></i> Proses</button>
																</div>
															</div>
																			</div>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</div>
							
											</div>
										<!-- Untuk Tab4 buatAPI !--> 
										<div class='tab-pane' id='tab_4'>
											<div class='col-md-6'><BR />
													<div class='box box-solid'>
														<div class='box-header ' style='background-color:#A9A9A9'>
															<h3 class='box-title'>Token API</h3>
														</div><!-- /.box-header -->
														<div class='box-body'>
																<div class='input-group input-group-sm'>
																	<input type='text' name='token' value='$tokenapi' class='form-control' readonly/>
																
																<span class='input-group-btn'>
																<button id='buat_api' class='btn btn-flat btn-success'><i class='fa fa-refresh'></i> Rilis API</button>
																</span>
																</div>
														</div><!-- /.box-body -->
													</div><!-- /.box -->
												</div>
										</div>
						</div>
							</div><!-- /.box-body -->
							</div><!-- /.box -->
								</div>
									</div>
										</div>
											<!-- /.tab-pane -->
											</div>
											<!-- /.tab-pane -->
											
										<!-- /.tab-content -->
									</div>
								</div>
							</div>
						</div>
					</div>";
	} else {
		echo "
								<div class='error-page'>
									<h2 class='headline text-yellow'> 404</h2>
									<div class='error-content'>
										<br/>
										<h3><i class='fa fa-warning text-yellow'></i> Upss! Halaman tidak ditemukan.</h3>
										<p>
											Halaman yang anda inginkan saat ini tidak tersedia.<br/>
											Silahkan kembali ke <a href='?'><strong>dashboard</strong></a> dan coba lagi.<br/>
											Hubungi petugas <strong><i>Developer</i></strong> jika ini adalah sebuah masalah.
										</p>
									</div><!-- /.error-content -->
								</div><!-- /.error-page -->
							";
	}
	echo "
						</section><!-- /.content -->
					</div><!-- /.content-wrapper -->
					<footer class='main-footer hidden-xs'>
						<div class='container'>
							<div class='pull-left hidden-xs'>
								<strong>
									<span id='end-sidebar'>
									<img src='$homeurl/dist/img/svg/copyright.svg' width='20'> CBTWISTIN $setting[versi] SUPPORT BY TEAM TKJ
									</img></span>
								</strong>
							</div>
					</footer>
				</div><!-- ./wrapper -->

				<!-- REQUIRED JS SCRIPTS -->
				
				<script src='$homeurl/plugins/jQuery/jquery-3.1.1.min.js'></script>
				<script src='$homeurl/dist/bootstrap/js/bootstrap.min.js'></script>
				<script src='$homeurl/plugins/fastclick/fastclick.js'></script>
				<script src='$homeurl/dist/js/adminlte.min.js'></script>
				<script src='$homeurl/dist/js/app.min.js'></script>
				<script src='$homeurl/plugins/datetimepicker/build/jquery.datetimepicker.full.min.js'></script>
				
				<script src='$homeurl/plugins/slimScroll/jquery.slimscroll.min.js'></script>
				<script src='$homeurl/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'></script>
				<script src='$homeurl/plugins/datatables/jquery.dataTables.min.js'></script>
				<script src='$homeurl/plugins/datatables/dataTables.bootstrap.min.js'></script>
				<script src='$homeurl/plugins/datatables/extensions/Select/js/dataTables.select.min.js'></script>
				<script src='$homeurl/plugins/datatables/extensions/Select/js/select.bootstrap.min.js'></script>
				<script src='$homeurl/plugins/iCheck/icheck.min.js'></script>
				<script src='$homeurl/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'></script>
				<script src='$homeurl/plugins/select2/select2.min.js'></script>
				<script src='$homeurl/plugins/tableedit/jquery.tabledit.js'></script>
				<script src='$homeurl/plugins/fileinput/js/fileinput.min.js'></script>
				<script src='$homeurl/plugins/notify/js/notify.js'></script>
				<script src='$homeurl/plugins/toastr/toastr.min.js'></script>
				<script src='$homeurl/plugins/sweetalert2/dist/sweetalert2.min.js'></script>
				<script src='$homeurl/dist/js/ajax_kota.js'></script>
				<script src='$homeurl/sans/js/ajaxupload.3.5.js'></script>
				<script src='$homeurl/assets/back/vendors/izitoast/js/iziToast.min.js' type='text/javascript'></script>
				

				
				<script>
				
				$('.loader').fadeOut('slow');
				
  					$(function () {
    				
   					 $('#textarea').wysihtml5()
					 
					 
					 
  					});
					
					var autoRefresh = setInterval(
						function () {
							$('#waktu').load('$homeurl/w1s4t4/_load.php?pg=waktu');
							
							$('#log-list').load('$homeurl/w1s4t4/_load.php?pg=log');
							
							$('#pengumuman').load('$homeurl/w1s4t4/_load.php?pg=pengumuman');
							
						}, 1000
					);
					var autoRefresh = setInterval(
						function () {
							$('#divstatus').load('$homeurl/w1s4t4/statuspeserta.php');	
						}, 1000
					);
				
					var autoRefresh = setInterval(
						function () {
							
							$('#isi_token').load('$homeurl/w1s4t4/_load.php?pg=token');
						}, 100000
					);
					
					
					$('.datepicker').datetimepicker({
                    timepicker:false,format:'Y-m-d'
					});
					$('.tgl').datetimepicker();
					$('.timer').datetimepicker({
                    datepicker:false,format:'H:i'
					});
					
					$(function () {
											//Add text editor
						
						$('#jenis').change(function(){
						if($('#jenis').val() == '2') {
						$('#jawabanpg').hide();
						$('input:radio[name=jawaban]').attr('disabled',true);
						}else{
						$('#jawabanpg').show();
						$('input:radio[name=jawaban]').attr('disabled',false);
						}
						});
						
					});
					function printkartu(idkelas,judul) {
						$('#loadframe').attr('src','kartu.php?id_kelas='+idkelas);
					}
					
					function printlegitimasi(idkelas,judul) {
						$('#loadframe').attr('src','legitimasi.php?id_kelas='+idkelas);
					}
					function printabsen() {
						var idsesi = $('#absensesi option:selected').val();
						var idmapel = $('#absenmapel option:selected').val();
						var idruang = $('#absenruang option:selected').val();
						var idkelas = $('#absenkelas option:selected').val();
						$('#loadabsen').attr('src', 'absen.php?id_sesi=' + idsesi + '&id_ruang=' + idruang + '&id_mapel=' + idmapel + '&id_kelas=' + idkelas);
					}				
					
					function printlaporan() {
						var idsesi = $('#sesi option:selected').val();
						var idmapel = $('#mapel option:selected').val();
						var idruang = $('#ruang option:selected').val();
						var idkelas = $('#kelas option:selected').val();
						$('#loadlaporan').attr('src','laporan.php?id_sesi='+idsesi+'&id_ruang='+idruang+'&id_mapel='+idmapel+'&id_kelas='+idkelas);
						
						
					}
					
					function iCheckform() {
						
							$('input[type=checkbox].flat-check, input[type=radio].flat-check').iCheck({
								 checkboxClass: 'icheckbox_square-green',
									radioClass: 'iradio_square-green',
								increaseArea: '20%' // optional
							});
					}
					
					$(document).ready(function() {
					$('#example1').DataTable({
						select:true
					});
					$('#soalpg').keyup(function(){
						$('#tampilpg').val(this.value);
					});
					$('#soalesai').keyup(function(){
						$('#tampilesai').val(this.value);
					});
					$('#formsoal').submit(function(e) {
						
						 e.preventDefault();
						 var data = new FormData(this);
							$.ajax({
								type: 'POST',
								url: 'simpansoal.php',
								enctype: 'multipart/form-data',
								data: data,
								cache: false,
								contentType: false,
								processData: false,
								beforeSend: function() {
										swal({
											
											  text: 'Proses menyimpan data',
											  timer: 2000,
											  onOpen: () => {
												swal.showLoading()
											  }
										});
									},
								success: function(data) {
									
									swal({
										  position: 'top-end',
										  type: 'success',
										  title: 'Data Berhasil disimpan',
										  showConfirmButton: true
										 
										});
										
								}
							})
							return false;
					 });
					$('.input-id').fileinput({
						allowedFileExtensions: ['jpg', 'png', 'gif','mp3','ogg','wav'],
						showRemove: false,
						showUpload: false,
						showBrowse: false,
						browseOnZoneClick: true,
						
						maxFileSize: 5000,
						uploadUrl: 'upload.php' // your upload server url

						
					}).on('filebatchselected', function(event, files) {
						$('.input-id').fileinput('upload');
					});
									
							$('#ceksemua').change(function() {
								$(this).parents('#tablereset:eq(0)').
								find(':checkbox').attr('checked', this.checked);
							});
						
						$('.idkel').change(function(){
							var thisval = $(this).val();
							var txt_id=$(this).attr('id').replace('me', 'txt');
							var idm = $('#'+txt_id).val();
							var idu = $('#iduj').val();
							console.log(thisval+idm);
							$('.linknilai').attr('href','?pg=nilai&ac=lihat&idu='+idu+'&idm='+idm+'&idk='+thisval);
						});
						
						$('.idkel').change(function(){
							var thisval = $(this).val();
							var txt_id=$(this).attr('id').replace('me', 'txt');
							var idm = $('#'+txt_id).val();
							var idu = $('#iduj').val();
							console.log(thisval+idm);
							$('.linknilaixsans').attr('href','?pg=nilai_xsans&ac=lihat&idu='+idu+'&idm='+idm+'&idk='+thisval);
						});
						
						$('.idkel').change(function(){
							var thisval = $(this).val();
							var txt_id=$(this).attr('id').replace('me', 'txt');
							var idm = $('#'+txt_id).val();
							var idu = $('#iduj').val();
							console.log(thisval+idm);
							$('.linkskor').attr('href','?pg=semuanilai&ac=lihat&idu='+idu+'&idm='+idm+'&idk='+thisval);
						});
						
						$('.idkel').change(function(){
							var thisval = $(this).val();
							var txt_id=$(this).attr('id').replace('me', 'txt');
							var idm = $('#'+txt_id).val();
							var idu = $('#iduj').val();
							console.log(thisval+idm);
							$('.linkleger').attr('href','?pg=leger&ac=lihat&idu='+idu+'&idm='+idm+'&idk='+thisval);
						});
						
							$('.idkel').change(function(){
							var thisval = $(this).val();
							var txt_id=$(this).attr('id').replace('me', 'txt');
							var idm = $('#'+txt_id).val();
							var idu = $('#iduj').val();
							console.log(thisval+idm);
						});
					$('.alert-dismissible').fadeTo(2000, 500).slideUp(500, function(){
					$('.alert-dismissible').alert('close');
					});
					$('.select2').select2();
					
					$('input:checkbox[name=masuksemua]').click(function() {
						if ($(this).is(':checked'))
						$('input:radio.absensi').attr('checked', 'checked');
						else
						$('input:radio.absensi').removeAttr('checked');
						});
					iCheckform()
					$('#btnbackup').click(function(){
						
						$('.notif').load('backup.php');	
						console.log('sukses');
			
					});

					});
				</script>
				<script>
					
						function kirim_form(){
							var homeurl;
							homeurl = '$homeurl';
							var jawab = $('#headerkartu').val();
							$.ajax({
								type:'POST',
								url:'simpanheader.php',
								data:'jawab='+jawab,
								success:function(response) {
									location.reload();
								}
							});
						}	
						
								
				</script>
				
				"; ?>

	<script type="text/javascript">
		//jquery untuk proses duplikasi 
		$(document).on('click', '.duplikat', function() {
			var idmapel = $(this).data('soal');
			$.ajax({
				type: 'POST',
				url: 'duplikat_soal.php',
				data: 'id=' + idmapel,
				beforeSend: function() {
					$('.loader').css('display', 'block');
				},
				success: function(respon) {
					$('.loader').css('display', 'none');
					location.reload();
				}
			});

		});
	</script>
	<script>
		$('#formrestore').submit(function(e) {
			e.preventDefault();
			var data = new FormData(this);
			//console.log(data);
			$.ajax({
				type: 'POST',
				url: 'restore.php',
				enctype: 'multipart/form-data',
				data: data,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.loader').show();
				},
				success: function(data) {
					$('.loader').hide();
					toastr.success(data);

				}
			});
			return false;
		});
	</script>
	<script>
		$("#formedit= $mapel[id_ujian]").submit(function(e) {
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: 'jadwal/edit_jadwal.php',
				data: $(this).serialize(),
				success: function(data) {
					location.reload();
				}
			});
			return false;
		});
	</script>

	<script>
		$(document).ready(function() {
			$("#btntoken").click(function() {
				$.ajax({
					url: "_load.php?pg=token",
					type: "POST",
					success: function(respon) {
						$('#isi_token').html(respon);
						toastr.success('token berhasil diperbarui');
					}
				});
				return false;
			})
			$('#formaktivasi').submit(function(e) {
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: 'jadwalaktivasi.php?key=1616161616',
					data: $(this).serialize(),
					success: function(data) {
						if (data == 'hapus') {
							toastr.success('jadwal ujian berhasil di hapus');
						}
						if (data == 'update') {
							toastr.success('jadwal ujian berhasil diperbarui');
						}
						$('#bodyreset').load(location.href + ' #bodyreset');

					}
				});
				return false;
			});

		});
	</script>
	<script>
		$('#formtambahujian').submit(function(e) {
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: 'jadwal/tambah_jadwal.php',
				data: $(this).serialize(),
				success: function(data) {
					console.log(data);
					if (data == "OK") {
						toastr.success("jadwal berhasil dibuat");
					} else {
						toastr.error("jadwal gagal tersimpan");
					}
					$('#tambahjadwal').modal('hide');
					$('#bodyreset').load(location.href + ' #bodyreset');
				}
			});
			return false;
		});
	</script>


	<script>
		$('#mastersoal').click(function() {
			var mapel_id = $('#mapel_id').val();
			$('.notif_mapel').load('backup_excel.php?mapel_id=' + mapel_id);
			console.log('sukses');
		});
		$(function() {
			$("#btnresetlogin").click(function() {
				id_array = new Array()
				i = 0;
				$("input.cekpilih:checked").each(function() {
					id_array[i] = $(this).val();
					i++;
				})

				$.ajax({
					url: 'resetlogin.php',
					data: "kode=" + id_array,
					type: "POST",
					success: function(respon) {
						if (respon == 1) {
							$("input.cekpilih:checked").each(function() {
								$(this).parent().parent().remove('.cekpilih').animate({
									opacity: "hide"
								}, "slow");
							})
						}
					}
				})
				return false;
			})
		})
		$(function() {
			$("#btnhapusbank").click(function() {
				id_array = new Array()
				i = 0;
				$("input.cekpilih:checked").each(function() {
					id_array[i] = $(this).val();
					i++;
				})

				swal({
					title: 'Bank Soal Terpilih ' + i,
					text: 'Apakah kamu yakin akan menghapus data bank soal yang sudah dipilih  ini ??',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: 'hapusbanksoal.php',
							data: "kode=" + id_array,
							type: "POST",
							success: function(respon) {
								if (respon == 1) {
									$("input.cekpilih:checked").each(function() {
										$(this).parent().parent().remove('.cekpilih').animate({
											opacity: "hide"
										}, "slow");
									})
								}
							}
						})
					}
				})
				return false;
			})
		})

		$(function() { //hapus Jadwal
			$("#btnhapusjadwal").click(function() {
				id_array = new Array()
				i = 0;
				$("input.cekpilih:checked").each(function() {
					id_array[i] = $(this).val();
					i++;
				})

				swal({
					title: 'Jadwal Terpilih ' + i,
					text: 'Apakah kamu yakin akan menghapus data jadwal yang sudah dipilih  ini ??',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: 'hapusjadwal.php',
							data: "kode=" + id_array,
							type: "POST",
							success: function(respon) {
								if (respon == 1) {
									$("input.cekpilih:checked").each(function() {
										$(this).parent().parent().remove('.cekpilih').animate({
											opacity: "hide"
										}, "slow");
									})
								}
							}
						})
					}
				})
				return false;
			})
		})

		$(function() {
			$("#buat_api").click(function() {


				swal({
					title: 'Generate API',
					text: 'Melanjutkan Proses ??',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Buat!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: 'token_api.php',
							type: "POST",
							beforeSend: function() {
								$('.loader').css('display', 'block');
							},
							success: function(respon) {
								$('.loader').css('display', 'none');
								location.reload();
							}
						})
					}
				})
				return false;
			})
		})

		$("#btnserver").click(function() {

			swal({
				title: 'Ganti Status Server ',
				text: 'Apakah kamu yakin akan mengganti status server ini ??',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, Ganti'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: 'gantiserver.php',
						type: "POST",
						success: function(respon) {
							location.reload();
						}
					})
				}
			});
			return false;
		})

		$(document).ready(function() {
			var messages = $('#pesan').notify({
				type: 'messages',
				removeIcon: '<i class="icon icon-remove"></i>'
			});
			$('#formreset').submit(function(e) {

				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {

						if (data == "ok") {
							messages.show("Reset Login Peserta Berhasil", {
								type: 'success',
								title: 'Berhasil',
								icon: '<i class="icon icon-check-sign"></i>'
							});
						}
						if (data == "pilihdulu") {
							swal({
								position: 'top-end',
								type: 'success',
								title: 'Data Berhasil disimpan',
								showConfirmButton: true

							});
						}
					}
				})
				return false;
			});
			//Json Datasiswa

			t.on('order.dt search.dt', function() {
				t.column(0, {
					search: 'applied',
					order: 'applied'
				}).nodes().each(function(cell, i) {
					cell.innerHTML = i + 1;
				});
			}).draw();
		});
	</script>
	<script>
		//import excel siswa
		$('#formsiswa').on('submit', function(e) {

			e.preventDefault();

			$.ajax({
				type: 'post',
				url: 'import_siswa.php',
				data: new FormData(this),
				processData: false,
				contentType: false,
				cache: false,

				beforeSend: function() {
					$('#progressbox').html('<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div>');
					$('.progress-bar').animate({
						width: "30%"
					}, 100);
				},
				success: function(response) {
					setTimeout(function() {
						$('.progress-bar').css({
							width: "100%"
						});
						setTimeout(function() {
							$('#hasilimport').html(response);

						}, 100);
					}, 500);

				}
			});

		});
	</script>

	<script>
		<?php if ($pg == 'jenisujian') { ?>
			$(document).ready(function() {
				$('#tablejenis').Tabledit({
					url: 'example.php?pg=jenisujian',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namajenis'],
							[3, 'status', '{"aktif": "aktif", "tidak": "tidak aktif"}']
						]
					}
				});

			});
		<?php } ?>
		<?php if ($pg == 'pk') { ?>
			$(document).ready(function() {
				$('#tablejurusan').Tabledit({
					url: 'example.php?pg=jurusan',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namajurusan']
						]
					}
				});

			});
		<?php } ?>
		<?php if ($pg == 'level') { ?>
			$(document).ready(function() {
				$('#tablelevel').Tabledit({
					url: 'example.php?pg=level',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namalevel']
						]
					}
				});
			});
		<?php } ?>
		<?php if ($pg == 'kelas') { ?>
			$(document).ready(function() {
				$('#tablekelas').Tabledit({
					url: 'example.php?pg=kelas',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'level'],
							[3, 'namakelas']
						]
					}
				});
			});
		<?php } ?>
		<?php if ($pg == 'matapelajaran') { ?>
			$(document).ready(function() {
				$('#tablemapel').Tabledit({
					url: 'example.php?pg=mapel',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namamapel']
						]
					}
				});
			});
		<?php } ?>
		<?php if ($pg == 'ruang') { ?>
			$(document).ready(function() {
				$('#tableruang').Tabledit({
					url: 'example.php?pg=ruang',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namaruang']
						]
					}
				});
			});
		<?php } ?>
		<?php if ($pg == 'sesi') { ?>
			$(document).ready(function() {
				$('#tablesesi').Tabledit({
					url: 'example.php?pg=sesi',
					restoreButton: false,
					columns: {
						identifier: [1, 'id'],
						editable: [
							[2, 'namasesi']
						]
					}
				});
			});
		<?php } ?>
	</script>
	<script>
		$(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)

			$("#soallevel").change(function() { // Ketika user mengganti atau memilih data provinsi
				// Sembunyikan dulu combobox kota nya
				var level = $(this).val();
				console.log(level);
				$.ajax({
					type: "POST", // Method pengiriman data bisa dengan GET atau POST
					url: "datakelas.php", // Isi dengan url/path file php yang dituju
					data: "level=" + level, // data yang akan dikirim ke file yang dituju
					success: function(response) { // Ketika proses pengiriman berhasil

						$("#soalkelas").html(response);
					}
				});
			});


			$(document).on('click', '.ulang', function() {
				var id = $(this).data('id');
				console.log(id);
				$('#ujiandiulang').html('bbbbbbbbbbbbbbbbbbbbbbbbb');
				swal({
					title: 'Apa anda yakin?',
					text: "Akan Mengulang Ujian Ini ??",

					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: 'ulangujian.php',
							method: "POST",
							data: 'id=' + id,
							success: function(data) {
								$('#ujiandiulang').html('1');
								swal({
									position: 'top-end',
									type: 'success',
									title: 'Ujian berhasil di ulang!!',
									showConfirmButton: false,
									timer: 1500
								});
							}
						});
					}
				})
			});

			$(document).on('click', '.hapus', function() {
				var id = $(this).data('id');
				console.log(id);
				$('#htmlujianselesai').html('bbbbbbbbbbbbbbbbbbbbbbbbb');
				swal({
					title: 'Apa anda yakin?',
					text: "aksi ini akan menyelesaikan secara paksa ujian yang sedang berlangsung!",

					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: 'selesaikan.php',
							method: "POST",
							data: 'id=' + id,
							success: function(data) {
								$('#htmlujianselesai').html('1');
								swal({
									position: 'top-end',
									type: 'success',
									title: 'Data berhasil disimpan',
									showConfirmButton: false,
									timer: 1500
								});
							}
						});
					}
				})

			});

			$("#bcruang").change(function() { //siswa susulan
				var sesi = $('#bcsesi').val();
				var ruang = $(this).val();
				console.log(ruang + sesi);
				$.ajax({
					type: "POST", // Method pengiriman data bisa dengan GET atau POST
					url: "berita_acara/bc_siswaabsen.php", // Isi dengan url/path file php yang dituju
					data: "ruang=" + ruang + '&sesi=' + sesi, // data yang akan dikirim ke file yang dituju
					success: function(response) { // Ketika proses pengiriman berhasil
						$("#bcsiswaabsen").html(response);
						console.log(response);
					},
					error: function(xhr, status, error) {
						console.log(error);
					}
				});
			});

			$(document).on('click', '.ambiljawaban', function() {

				var idmapel = $(this).data('id');
				console.log(idmapel);
				swal({
					title: 'Are you sure?',
					text: 'Fungsi ini akan memindahkan data jawaban dari temp_jawaban ke hasil jawaban',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Ambil!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							type: 'POST',
							url: 'ambiljawaban.php',
							data: 'id=' + idmapel,
							beforeSend: function() {
								$('.loader').css('display', 'block');
							},
							success: function(response) {
								$('.loader').css('display', 'none');
								location.reload();
								$(this).attr('disabled', 'disabled');
								swal({
									position: 'top-end',
									type: 'success',
									title: 'Data Berhasil diambil',
									showConfirmButton: false,
									timer: 1500
								});

							}
						});

					}
				})
			});

			$(document).on('click', '.ambilnilai', function() {

				var idmapel = $(this).data('id');
				console.log(idmapel);
				swal({
					title: 'Are you sure?',
					text: 'Fungsi ini akan memindahkan data nilai dari temp_nilai ke hasil nilai',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Ambil!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							type: 'POST',
							url: 'ambilnilai.php',
							data: 'id=' + idmapel,
							beforeSend: function() {
								$('.loader').css('display', 'block');
							},
							success: function(response) {
								$('.loader').css('display', 'none');
								location.reload();
								$(this).attr('disabled', 'disabled');
								swal({
									position: 'top-end',
									type: 'success',
									title: 'Data Berhasil diambil',
									showConfirmButton: false,
									timer: 1500
								});

							}
						});

					}
				})
			});

			$(document).on('click', '.backnilai', function() {

				var idmapel = $(this).data('id');
				console.log(idmapel);
				swal({
					title: 'Are you sure?',
					text: 'Fungsi ini akan mengembalikan nilai dari Draft Nilai ke tabel Nilai',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Ambil!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							type: 'POST',
							url: 'backnilai.php',
							data: 'id=' + idmapel,
							beforeSend: function() {
								$('.loader').css('display', 'block');
							},
							success: function(response) {
								$('.loader').css('display', 'none');
								location.reload();
								$(this).attr('disabled', 'disabled');
								swal({
									position: 'top-end',
									type: 'success',
									title: 'Data Berhasil diambil',
									showConfirmButton: false,
									timer: 1500
								});

							}
						});

					}
				})
			});
		});
	</script>

	<script>
		$(document).on('click', '.reset', function() {
			var id = $(this).data('id');
			console.log(id);
			$('#htmlujianselesai').html('bbbbbbbbbbbbbbbbbbbbbbbbb');
			swal({
				title: 'Apa anda yakin?',
				text: "aksi ini akan menyelesaikan secara paksa ujian yang sedang berlangsung!",

				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: 'reset/resetsoal.php',
						method: "POST",
						data: 'id=' + id,
						success: function(data) {
							$('#htmlujianselesai').html('1');
							swal({
								position: 'top-end',
								type: 'success',
								title: 'Data berhasil disimpan',
								showConfirmButton: false,
								timer: 1500
							});
						}
					});
				}
			})

		});
	</script>
	<script>
		$(function() {
			var btnUpload = $('#upload');
			var status = $('#status');
			new AjaxUpload(btnUpload, {
				action: 'uploadpoto_guru.php',
				name: 'uploadfile',
				onSubmit: function(file, ext) {
					if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
						// extension is not allowed 
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					status.text('Uploading...');
				},
				onComplete: function(file, response) {
					//On completion clear the status
					status.text('');
					//Add uploaded file to list

					if (response === "success") {
						$('#upload').html('<img src="../foto/fotoguru/' + file + '"  width="200" alt="" />').addClass('success');
						//					$('<li></li>').appendTo('#files').html('<img src=".photo/'+file+'" alt="" /><br />'+file).addClass('success');
					} else {
						$('<li></li>').appendTo('#files').text(file).addClass('error');
					}
				}
			});

		});
	</script>

	</body>

	</html>