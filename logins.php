<?php
	require("config/config.default.php");
	$cekdb = mysqli_query($koneksi,'select 1 from `pengawas` LIMIT 1');
	if($cekdb==false){
		header("Location: install.php");
  }
  (isset($_GET['pg'])) ? $pg = $_GET['pg'] : $pg = '';
	$siswaQ = mysqli_query($koneksi,"SELECT * FROM siswa WHERE id_kelas='$id_kelas' group by nama");

	?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login | <?php echo $setting['aplikasi']; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<html style="height: auto; min-height: 100%;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>Login | CANDY</title>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
				<link href="<?php echo $setting['logo']; ?>" rel="shortcut icon">
				<link rel="stylesheet" href="plugins/slider/bootstrap.min.css">
				<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.css">
				<link rel="stylesheet" href='<?= $homeurl ?>/dist/css/AdminLTE.min.css' />
				<link rel="stylesheet" href='<?= $homeurl ?>/dist/css/skins/skin-purple.min.css' />
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="dist/vendor/animate/animate.css">
				
				<link rel="stylesheet" type="text/css" href="plugins/slider/style.min.css">
				<link rel="stylesheet" type="text/css" href="plugins/slider/entypo.css">
				<link rel="stylesheet" type="text/css" href="dist/fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
				<link rel="stylesheet" type="text/css" href="dist/css/util.css">
        <link rel="stylesheet" type="text/css" href="dist/css/main.css">
        <link rel="stylesheet" type="text/css" href="sans/css/logins.css">
				<link rel="stylesheet" href="<?=$homeurl?>/plugins/toastr/toastr.min.css">
	<!--===============================================================================================-->
				<link rel='stylesheet' href='<?php echo $homeurl; ?>/plugins/sweetalert2/dist/sweetalert2.min.css'>

	<style>
  .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('dist/img/loading.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
	.btn {
		background-color:#4169e1;
  }
  
  </style>
</head>
<body >
	
	
  
<style>#pbSliderWrap0 .o-slider.isAnimate{-webkit-transition: all 600ms ease-out;transition: all 600ms ease-out;}</style></head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="skin-purple layout-top-nav" >

<div class="wrapper" style="height: auto; min-height: 100%;">

  <header class="main-header">
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand" style="padding:5px 15px;">
		<img src="<?php echo $setting['logo']; ?>" height="40px"></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#" data-toggle="modal" data-target="#modalinfo"><i class="fa fa-sign-in"></i>   INFO </a></li>
           
          </ul>
         
        </div>
													
      </div>
    </nav>
  </header>
  
  <div class='modal fade' id='modalinfo' style='display: none;'>
						<div class='modal-dialog'>
							<div class='modal-content'>
								<div class='modal-header bg-green'>
									<button class='close' data-dismiss='modal'><span aria-hidden='true'><i class='fa fa-remove'></i></span></button>
									<h4 class='modal-title'>Informasi</h4>
								</div>
								
							</div>
						</div>
					</div>

  <!-- Full Width Column -->
  <div class="content-wrapper" style="background: rgb(255, 255, 255); min-height: 567px;">
  
 <div class="o-sliderContainer" id="pbSliderWrap0" style="margin-top: 0px; z-index: 100; touch-action: pan-x; height: 300px;">
  <div class="o-slider isDraggable isVisible" id="pbSlider0" style="width: 500%; perspective: 1000px; backface-visibility: hidden; transform: translateX(0%);">
    <div class="o-slider--item isActive" data-image="images/5.jpg" data-id="slide-0" style="width: 20%; margin-left: 0%; margin-right: 0%; background-image: url(&quot;images/5.jpg&quot;); height: 300px;">
      <div class="o-slider-textWrap">
        <h1 class="o-slider-title"><b>CBT</b> APP</h1>
        <span class="a-divider"></span>
        <h2 class="o-slider-subTitle">Mudahnya Simulasi Mandiri</h2>
        <span class="a-divider"></span>
        <p class="o-slider-paragraph">Sekolah lebih siap dalam mempersiapkan ujian nasional berbasis komputer dengan simulasi mandiri</p>
      </div>
	</div>
   
    <div class="o-slider--item" data-image="images/1.jpg" data-id="slide-1" style="width: 20%; margin-left: 0%; margin-right: 0%; background-image: url(&quot;images/1.jpg&quot;); height: 300px;">
      <div class="o-slider-textWrap">
        <h1 class="o-slider-title"><b>CBT</b> APP</h1>
        <span class="a-divider"></span>
        <h2 class="o-slider-subTitle">Penilaian Berbasis Komputer</h2>
        <span class="a-divider"></span>
        <p class="o-slider-paragraph">Bisa dipakai untuk keperluan PTS, PAS</p>
      </div>
	</div>
    
    <div class="o-slider--item" data-image="images/2.jpg" data-id="slide-2" style="width: 20%; margin-left: 0%; margin-right: 0%; background-image: url(&quot;images/2.jpg&quot;); height: 300px;">
      <div class="o-slider-textWrap">
        <h1 class="o-slider-title"><b>CBT</b> APP</h1>
        <span class="a-divider"></span>
        <h2 class="o-slider-subTitle">Jujur Npmer Satu</h2>
        <span class="a-divider"></span>
        <p class="o-slider-paragraph">Dengan aplikasi ini membuat ujian semakin jujur,untuk kemajuan pendidikan di INDONESIA </p>
      </div>
	</div>
	
    <!-- <div class="o-slider--item" data-image="images/3.jpg" data-id="slide-3" style="width: 20%; margin-left: 0%; margin-right: 0%; background-image: url(&quot;images/3.jpg&quot;); height: 300px;">
      <div class="o-slider-textWrap">
        <h1 class="o-slider-title"><b>CBT</b> APP</h1>
        <span class="a-divider"></span>
        <h2 class="o-slider-subTitle"></h2>
        <span class="a-divider"></span>
        <p class="o-slider-paragraph"></p>
      </div>
	  </div> -->

    <!-- <div class="o-slider--item" data-image="images/4.jpg" data-id="slide-4" style="width: 20%; margin-left: 0%; margin-right: 0%; background-image: url(&quot;images/4.jpg&quot;); height: 300px;">
      <div class="o-slider-textWrap">
        <h1 class="o-slider-title"><b>CBT</b> APPLICATION</h1>
        <span class="a-divider"></span>
        <h2 class="o-slider-subTitle"></h2>
        <span class="a-divider"></span>
        <p class="o-slider-paragraph"> </p>
      </div>
	  </div> -->
  </div>
<div class="loaderWrap" style="display: none;">
<div class="ball-scale-multiple">
</div>
</div>
<div class="o-slider-controls isVisible"></div>
</div>

<div class="col-md-3">
      <div class="animated tada wrap-login100"  style='background-color:#F8F8FF; width:100%; margin-top:-4px '>
				<form id="formlogin" action="ceklogin.php" class="login100-form validate-form">
					
					<span class="animated infinite pulse delay-5s login100-form-title p-b-15">
            <h1><b>LOGIN SISWA</b></h1>
            <p style="font-size:18px"><?php echo $setting['sekolah']; ?></p>
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter Username" required>
						<input class="input100" type="text" placeholder="Username" name="username">
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" id="pass" placeholder="Password" name="password">
					</div>
					
					<div class="checkbox icheck">
						<label>
						  <input type="checkbox" onclick="showpass()"> Show Password
						</label>
					  </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
          </div>
      
				</form>
			</div>
    <!-- /.box-body -->
</div>
<div class="col-md-9" style="margin-top:10px;">
			<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li <?php if ($pg == ''){echo 'class="active"'; }?>><a href="?" style="font-size:16px; font-weight:1000">Statistik</a></li>
			        <li <?php if ($pg == 'pengumuman'){echo 'class="active"'; }?>><a href="logins.php?pg=pengumuman" style="font-size:16px; font-weight:1000" >Pengumuman</a></li>
            </ul>
        <?php if($pg==''){
          echo"  
            <div class='row' >
              <div class='col-md-12'>
								<div class='box box-solid'>
									<div class='box-header bg-blue' style='width:100%'>
										<h3 class='box-title'>Statistik</h3>
									</div><!-- /.box-header -->
									<div class='box-body'>
										<div>
											<div class='col-md-4'>
                        <div class='box box-solid'>       
                          <!-- /.box-header -->
                          <div class='box-body'>
                            <p><b>Statistik Sekolah</b></p>
                            <p>$setting[sekolah]</p>
                            <canvas id='chart-sek' width='500' height='500' class='chartjs-render-monitor' style='display: block; width: 309px; height: 154px;'></canvas>
                          </div>
                          <!-- /.box-body -->
                        </div>
                          <!-- /.box -->
                      </div>
                    <div class='col-md-4'>
                      <div class='box box-solid'>
                        <!-- /.box-header -->
                        <div class='box-body'>
                          <p><b>Statistik Guru</b></p>
                          <p>$setting[sekolah]</p>
                          <canvas id='chart-sek2' width='200' height='200' class='chartjs-render-monitor' style='display: block; width: 109px; height: 54px;'></canvas>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                    </div>
                    <div class='col-md-4'>
                      <div class='box box-solid'>
                        <!-- /.box-header -->
                        <div class='box-body'>
                          <p><b>Statistik Penilian</b></p>
                          <p>$setting[sekolah]</p>
                          <canvas id='chart-sek3' width='200' height='200' class='chartjs-render-monitor' style='display: block; width: 109px; height: 54px;'></canvas>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                    </div>
										</div>
									</div>
								</div>
              </div>
               
              <!-- /.tab-content -->";
        }elseif($pg == 'pengumuman') {
          echo"
            <div class='row'>
												<div class='col-md-12'>
													<div class='box box-solid'>
														<div class='box-header bg-blue'>
															<h3 class='box-title'>Pengumuman</h3>
														</div><!-- /.box-header -->
														<div class='box-body'>
															<div id='pengumuman'>
															<p class='text-center'>
																<br/><i class='fa fa-spin fa-circle-o-notch'></i> Loading....
															</p>
															</div>
														</div>
													</div>
                        </div>
                        <script>
                          var autoRefresh = setInterval(
                            function () {
                              $('#waktu').load('$homeurl/admin/_load.php?pg=waktu');
                              $('#pengumuman').load('$homeurl/admin/_load.php?pg=pengumumansiswa');
                              
                              
                            }, 1000
                          );
                        </script>";
        }
        ?>
          </div>
		  </div>
		

    <div class="container">
      <!-- Content Header (Page header) -->
     

      <!-- Main content -->
      <section class="content" style="margin-top:-100px;">
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>CBT-APPLICATION | Version</b> 1.0
      </div>
     <strong></strong>
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3.1.1 -->
<script src="dist/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="<?= $homeurl ?>/plugins/toastr/toastr.min.js"></script>
<script src="dist/vendor/bootstrap/js/popper.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="plugins/slider/bootstrap.min.js.download"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="plugins/slider/fastclick.js.download"></script>
<!-- AdminLTE App -->
<script src="plugins/slider/adminlte.min.js.download"></script>
<script src="plugins/slider/hammer.min.js.download"></script>
<script src="plugins/slider/slider.js.download"></script>
<script src="<?php echo $homeurl; ?>/plugins/sweetalert2/dist/sweetalert2.min.js"></script>
 <script src="sans/js/Chart.js"></script>

	<script>
		$(document).ready(function() {
			$('#formlogin').submit(function(e) {
				var homeurl;
				homeurl = '<?php echo $homeurl; ?>';
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						if (data == "ok") {
							console.log('sukses');
							window.location = homeurl;
						}
						if (data == "nopass") {
							swal({
								position: 'top-end',
								type: 'warning',
								title: 'Password Salah',
								showConfirmButton: false,
								timer: 1500
							});
						}
						if (data == "td") {
							swal({
								position: 'top-end',
								type: 'warning',
								title: 'Siswa tidak terdaftar',
								showConfirmButton: false,
								timer: 1500
							});
						}
						if (data == "nologin") {
							swal({
								position: 'top-end',
								type: 'warning',
								title: 'Siswa sudah aktif',
								showConfirmButton: false,
								timer: 1500
							});
						}

					}
				})
				return false;
			});

		});

		function showpass() {
			var x = document.getElementById("pass");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		$('#pbSlider0').pbTouchSlider({
  slider_Wrap: '#pbSliderWrap0',
  slider_Item_Width : 100,
  slider_Threshold: 10,
  slider_Speed:100,
  slider_Ease:'ease-out',
  slider_Drag : true,
  slider_Arrows: {
    enabled : true,
  },
  slider_Dots: {
    class :'.o-slider-pagination',
    enabled : false,
    preview : false
  },
  slider_Breakpoints: {
      default: {
          height: 300
      },
      tablet: {
          height: 300,
          media: 1024
      },
      smartphone: {
          height: 250,
          media: 768
      }
  }
});

</script>
	
		<script>
	$(function () {
	    var ctx = $("#chart-sek");
      var ctx2 = $("#chart-sek2");
      var ctx3 = $("#chart-sek3");
	    var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data : {
                    datasets: [{
                        data: [<?php 
					$siswa = mysqli_query($koneksi,"select * from siswa");
					echo mysqli_num_rows($siswa);
					?>,
					<?php 
					$kelas = mysqli_query($koneksi,"select * from kelas");
					echo mysqli_num_rows($kelas);
					?>,
					<?php 
					$mapel = mysqli_query($koneksi,"select * from mata_pelajaran");
					echo mysqli_num_rows($mapel);
					?>],
                        backgroundColor: [
                            '#0abb87',
                            '#5d78ff',
                            '#ffb822',
                            
                        ],
                    }],
                
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Siswa',
                        'Kelas',
                        'Mapel'
                    ]
                },
            
        });
        
        var myPieChart = new Chart(ctx2, {
            type: 'pie',
            data : {
                    labels: ['Guru','Admin'],
                    datasets: [{
                        label:'',
                        data: [<?php 
					$pengawas = mysqli_query($koneksi,"select * from pengawas WHERE level = 'guru'");
					echo mysqli_num_rows($pengawas);
					?>,
					<?php 
					$jenis = mysqli_query($koneksi,"select * from pengawas WHERE level = 'admin'");
					echo mysqli_num_rows($jenis);
					?>],
                        backgroundColor: [
                            'lightseagreen',
                            'lightsteelblue',
                            
                        ],
                        
                    }],
                
                   
                },
         
        });
        var myPieChart = new Chart(ctx3, {
            type: 'pie',
            data : {
                    labels: ['Nilai','Draft Nilai'],
                    datasets: [{
                        label:'',
                        data: [<?php 
					$pengawas = mysqli_query($koneksi,"select * from nilai");
					echo mysqli_num_rows($pengawas);
					?>,
					<?php 
					$jenis = mysqli_query($koneksi,"select * from hasil_nilai");
					echo mysqli_num_rows($jenis);
					?>],
                        backgroundColor: [
                            'lightskyblue',
                            'lightgrey',
                            '#ffb822'
                            
                        ],
                        
                    }],
                
                   
                },
         
        });
        
   
	})
	</script>

</body>
</html>