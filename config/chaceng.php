<?php
// Ambil nama file
$phpself = isset($_SERVER['PHP_SELF']) ? basename($_SERVER['PHP_SELF']) : '';
$phpself = trim($phpself, '.php');
// blok akses jika karakter yang tidak diinginkan ditemukan di nama file
if(preg_match("/:|;|'|\(|\)/", $phpself)) $phpself = '';

// define cache file name
$cachefile = ($phpself != '') ? 'cache/'.$phpself.'.cache' : '';

if(!empty($cachefile) && file_exists($cachefile)){
    // Atur waktu dalam hitungan menit
    $cachetime = 5 * 60; 
  

    if((filesize($cachefile) > 0) &&
      ((time() - $cachetime) < filemtime($cachefile))){
        // halaman yang telah di-cache dari permintaan sebelumnya
        // output isi dari file cache
        include_once($cachefile); 
        echo '<!-- Taken from cache at: '.date('H:i', filemtime($cachefile)).' -->';
        
        exit;
    }        
}
ob_start();   
?>