-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 02:56 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujian_deyar`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `sesi` varchar(10) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `ikut` varchar(10) NOT NULL,
  `susulan` varchar(10) NOT NULL,
  `jumlahujian` varchar(50) NOT NULL,
  `no_susulan` text NOT NULL,
  `mulai` varchar(10) NOT NULL,
  `selesai` varchar(10) NOT NULL,
  `nama_proktor` varchar(50) NOT NULL,
  `nip_proktor` varchar(50) NOT NULL,
  `nama_pengawas` varchar(50) NOT NULL,
  `nip_pengawas` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  `tgl_ujian` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumentasi`
--

CREATE TABLE `dokumentasi` (
  `id_file` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `ukuran_file` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tgl_upload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumentasi`
--

INSERT INTO `dokumentasi` (`id_file`, `nama_file`, `tipe_file`, `ukuran_file`, `file`, `tgl_upload`) VALUES
(1, 'PANDUAN PENGERJAAN SOAL', 'pdf', '260747', 'PANDUAN PENGERJAAN SOAL.pdf', '2021-03-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `file_pendukung`
--

CREATE TABLE `file_pendukung` (
  `id_file` int(11) NOT NULL,
  `id_mapel` int(11) DEFAULT 0,
  `nama_file` varchar(50) DEFAULT NULL,
  `status_file` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_jawaban`
--

CREATE TABLE `hasil_jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_nilai`
--

CREATE TABLE `hasil_nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) NOT NULL,
  `ujian_selesai` varchar(20) NOT NULL,
  `jml_benar` int(10) NOT NULL,
  `jml_salah` int(10) NOT NULL,
  `nilai_esai` varchar(10) NOT NULL,
  `skor` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  `status` int(1) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `hasil` int(2) NOT NULL,
  `online` int(1) NOT NULL,
  `blok` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `id_siswa`, `id_mapel`, `id_soal`, `id_ujian`, `jawaban`, `jenis`, `esai`, `nilai_esai`, `ragu`) VALUES
(2, 1, 1, 1, 2, 'E', 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama`, `status`) VALUES
('PAS', 'PENILAIAN AKHIR SEMESTER', 'aktif'),
('PAT', 'PENILAIAN AKHIR TAHUN', 'tidak'),
('PH', 'PENILAIAN HARIAN', 'tidak'),
('USBK', 'UJIAN SEKOLAH BERBASIS KOMPUTER', 'tidak'),
('USBN', 'UJIAN SEKOLAH BERSTANDAR NASIONAL', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` varchar(11) NOT NULL,
  `level` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `level`, `nama`) VALUES
('XIITKJ', 'XII', 'XIITKJ'),
('XIITKR', 'XII', 'XIITKR'),
('XIITP', 'XII', 'XIITP');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `kode_level` varchar(5) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`kode_level`, `keterangan`) VALUES
('XII', 'XII');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `text` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `idpk` varchar(10) NOT NULL,
  `idguru` varchar(3) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `level` varchar(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `kkm` int(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `siswa` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(2) NOT NULL,
  `statusujian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `idpk`, `idguru`, `nama`, `jml_soal`, `jml_esai`, `tampil_pg`, `tampil_esai`, `bobot_pg`, `bobot_esai`, `level`, `opsi`, `kkm`, `kelas`, `siswa`, `date`, `status`, `statusujian`) VALUES
(1, 'semua', '2', 'TEST', 1, 0, 1, 0, 100, 0, 'semua', 5, 75, 'a:1:{i:0;s:5:\"semua\";}', '', '2021-03-02 13:29:03', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `mapel_id` int(5) NOT NULL,
  `kode_mapel` varchar(20) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL,
  `mata_pelajaran_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`mapel_id`, `kode_mapel`, `nama_mapel`, `mata_pelajaran_id`) VALUES
(1, 'TEST', 'TEST', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) NOT NULL,
  `ujian_selesai` varchar(20) NOT NULL,
  `jml_benar` int(10) NOT NULL,
  `jml_salah` int(10) NOT NULL,
  `nilai_esai` varchar(10) NOT NULL,
  `skor` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  `status` int(1) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `hasil` int(2) NOT NULL,
  `online` int(1) NOT NULL,
  `blok` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_ujian`, `id_mapel`, `id_siswa`, `kode_ujian`, `ujian_mulai`, `ujian_berlangsung`, `ujian_selesai`, `jml_benar`, `jml_salah`, `nilai_esai`, `skor`, `total`, `status`, `ipaddress`, `hasil`, `online`, `blok`) VALUES
(2, 2, 1, 1, 'PAS', '2021-03-02 20:29:39', '2021-03-02 20:30:40', '2021-03-02 20:30:40', 0, 1, '', '0.00', '0.00', 0, '127.0.0.1', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengacak`
--

CREATE TABLE `pengacak` (
  `id_pengacak` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` longtext NOT NULL,
  `id_esai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengacakopsi`
--

CREATE TABLE `pengacakopsi` (
  `id_pengacak` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengawas`
--

CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `fotoguru` text NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(10) NOT NULL,
  `no_ktp` varchar(16) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat_jalan` varchar(255) NOT NULL,
  `rt_rw` varchar(8) NOT NULL,
  `dusun` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kode_pos` int(6) NOT NULL,
  `nuptk` varchar(20) NOT NULL,
  `bidang_studi` varchar(50) NOT NULL,
  `jenis_ptk` varchar(50) NOT NULL,
  `tgs_tambahan` varchar(50) NOT NULL,
  `status_pegawai` varchar(50) NOT NULL,
  `status_aktif` varchar(20) NOT NULL,
  `status_nikah` varchar(20) NOT NULL,
  `sumber_gaji` varchar(30) NOT NULL,
  `ahli_lab` varchar(10) NOT NULL,
  `nama_ibu` varchar(40) NOT NULL,
  `nama_suami` varchar(50) NOT NULL,
  `nik_suami` varchar(20) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL,
  `tmt` date NOT NULL,
  `keahlian_isyarat` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(10) NOT NULL,
  `npwp` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengawas`
--

INSERT INTO `pengawas` (`id_pengawas`, `nip`, `nama`, `fotoguru`, `jabatan`, `username`, `password`, `level`, `no_ktp`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `agama`, `no_hp`, `email`, `alamat_jalan`, `rt_rw`, `dusun`, `kelurahan`, `kecamatan`, `kode_pos`, `nuptk`, `bidang_studi`, `jenis_ptk`, `tgs_tambahan`, `status_pegawai`, `status_aktif`, `status_nikah`, `sumber_gaji`, `ahli_lab`, `nama_ibu`, `nama_suami`, `nik_suami`, `pekerjaan`, `tmt`, `keahlian_isyarat`, `kewarganegaraan`, `npwp`) VALUES
(1, '-', 'Administrator', '', '', 'admin', '$2y$10$zFaLAI2osvMOJ9rIcOe75.fvDS6vEBwvd95ynKBJLJjnIXkYxy6/.', 'admin', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(2, '-', 'Abdul Munir HMA, M.Pd.', '', '', 'munir', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(3, '-', 'Abdul Sukur, S.Kom.', '', '', 'sukur', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(4, '-', 'Ifo Indriati, S.Pd.', '', '', 'ifo', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(5, '-', 'Puput Riyani, S.Pd.', '', '', 'puput', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(6, '-', 'Ina Nopiana, S.Pd.', '', '', 'ina', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(7, '-', 'Nur\'aliyah, S. Pd.', '', '', 'lia', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(8, '-', 'H. Mulyadi, S.Ag.', '', '', 'mulyadi', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(9, '-', 'Sopan Sopiana, M.Pd.', '', '', 'sopan', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(10, '-', 'Dewi Hastuti, S.Pd', '', '', 'dewi', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(11, '-', 'Jamaluddin, S.Ag.', '', '', 'jamal', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(12, '-', 'Ginanjar Sukma Wjaya,Amd', '', '', 'ginanjar', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(13, '-', 'Saidah, S.Pd', '', '', 'saidah', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(14, '-', 'Nanan Supriatna,Amd', '', '', 'nanan', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(15, '-', 'M. Fuad Usman, MM', '', '', 'fuad', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(16, '-', 'Anisa Kanti Mulyawati, S.Pd', '', '', 'anisa', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(17, '-', 'Sri Indriyani, S.Pd', '', '', 'indri', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(18, '-', 'Suida Afriyani, S.Si', '', '', 'suida', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(19, '-', 'Weniarti, S.Pd', '', '', 'weni', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(20, '-', 'Dra. Betty Ria Panjaitan', '', '', 'betty', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(21, '-', 'Laily Maghrifah, S.Pd', '', '', 'laily', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(22, '-', 'Wiwin Winarti, S.Pd', '', '', 'wiwin', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(23, '-', 'Ulfa Nurvina, S.Pd', '', '', 'ulfa', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(24, '-', 'Ferari, S.Pd', '', '', 'ferari', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(25, '-', 'Deyar  Cipta Rizky', '', '', 'deyarcr', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(26, '-', 'M. Rian Fadhillah, S.Pd', '', '', 'rian', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(27, '-', 'Pardin Nahampun, SE', '', '', 'pardin', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(28, '-', 'Maulina Septiani, S.Pd', '', '', 'maulina', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(29, '-', 'Agus Budi Setiawan', '', '', 'agus', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(30, '-', 'Gilang Frilly R, S.Pd', '', '', 'gilang', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(31, '-', 'Siti Aisyah Nurjannah, S.Pd', '', '', 'siti', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(32, '-', 'Richa Khairani, S.Pd', '', '', 'richa', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(33, '-', 'Taufiq Samsiadi', '', '', 'taufiq', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(34, '-', 'Sumardjono', '', '', 'jhon', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(35, '-', 'Lika Wulandari, S.Pd', '', '', 'lika', 'admin123', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', ''),
(36, '-', 'Ina Nopiana', '', '', 'inanopiana', '$2y$10$FL/X.1s9KwrbkhJp6rI/7OcRWvJrpV8ESJHbmlf46nW3fmcIn8Wbm', 'admin', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(5) NOT NULL,
  `type` varchar(30) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `user` int(3) NOT NULL,
  `text` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pk`
--

CREATE TABLE `pk` (
  `id_pk` varchar(10) NOT NULL,
  `program_keahlian` varchar(50) NOT NULL,
  `kode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pk`
--

INSERT INTO `pk` (`id_pk`, `program_keahlian`, `kode`) VALUES
('TKJ', 'TKJ', ''),
('TKR', 'TKR', ''),
('TP', 'TP', '');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`kode_ruang`, `keterangan`) VALUES
('R1', 'R1'),
('R2', 'R2');

-- --------------------------------------------------------

--
-- Table structure for table `savsoft_options`
--

CREATE TABLE `savsoft_options` (
  `oid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `q_option` text NOT NULL,
  `q_option_match` varchar(1000) DEFAULT NULL,
  `score` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `savsoft_qbank`
--

CREATE TABLE `savsoft_qbank` (
  `qid` int(11) NOT NULL,
  `question_type` varchar(100) NOT NULL DEFAULT 'Multiple Choice Single Answer',
  `question` text NOT NULL,
  `description` text NOT NULL,
  `cid` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  `no_time_served` int(11) NOT NULL DEFAULT 0,
  `no_time_corrected` int(11) NOT NULL DEFAULT 0,
  `no_time_incorrected` int(11) NOT NULL DEFAULT 0,
  `no_time_unattempted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `server`
--

CREATE TABLE `server` (
  `kode_server` varchar(20) NOT NULL,
  `nama_server` varchar(30) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `server`
--

INSERT INTO `server` (`kode_server`, `nama_server`, `status`) VALUES
('SRX', 'SRX', 'aktif'),
('SR-XI', 'SR-XI', 'aktif'),
('WI001', 'WI001', 'aktif'),
('SMKWI', 'SMKWI', 'aktif'),
('NIZ-001', 'NIZ-001', 'aktif'),
('', '', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `sesi`
--

CREATE TABLE `sesi` (
  `kode_sesi` varchar(10) NOT NULL,
  `nama_sesi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sesi`
--

INSERT INTO `sesi` (`kode_sesi`, `nama_sesi`) VALUES
('1', '1'),
('2', '2'),
('3', '3');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `session_time` varchar(10) NOT NULL,
  `session_hash` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `session_time`, `session_hash`) VALUES
(1, '1447610188', '$2y$10$dt9BTs7FlTXgpactflaXPOSVWrs.wurWsKBGv18JkzolJmHZOj.B.');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `aplikasi` varchar(100) NOT NULL,
  `kode_sekolah` varchar(10) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `jenjang` varchar(5) NOT NULL,
  `kepsek` varchar(50) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `proktor` varchar(50) NOT NULL,
  `nip_proktor` varchar(30) NOT NULL,
  `no_proktor` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `web` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `foto` text NOT NULL,
  `ttd` text NOT NULL,
  `header` text NOT NULL,
  `header_kartu` text NOT NULL,
  `nama_ujian` text NOT NULL,
  `versi` varchar(10) NOT NULL,
  `ip_server` varchar(50) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `server` varchar(50) NOT NULL,
  `id_server` varchar(50) NOT NULL,
  `db_folder` varchar(50) NOT NULL,
  `db_host` varchar(50) NOT NULL,
  `db_user` varchar(50) NOT NULL,
  `db_pass` varchar(50) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `db_token` varchar(50) NOT NULL,
  `db_url` varchar(50) NOT NULL,
  `reset_login` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `aplikasi`, `kode_sekolah`, `sekolah`, `jenjang`, `kepsek`, `nip`, `proktor`, `nip_proktor`, `no_proktor`, `alamat`, `kecamatan`, `kota`, `provinsi`, `telp`, `fax`, `web`, `email`, `logo`, `foto`, `ttd`, `header`, `header_kartu`, `nama_ujian`, `versi`, `ip_server`, `waktu`, `server`, `id_server`, `db_folder`, `db_host`, `db_user`, `db_pass`, `db_name`, `db_token`, `db_url`, `reset_login`) VALUES
(1, 'CBT-PAS', 'SMA-00001', 'WISTEK', 'SMK', 'Mikasa Arckerman', '-', 'Deyar Cipra Rizky', '-', '08121212112', 'JL. Raya Lenteng Agung / Jl. Langgar RT 009/03 No. 1 Kode Pos 12520', '      PASAR MINGGU         ', '      JAKARTA SELATAN         ', 'DKI JAKARTA         ', '087777639337', '', 'wistek.co.id', 'wistek@gmail.com', 'dist/img/logo51.png', 'dist/img/foto50.png', 'dist/img/ttd47.png', '', 'KARTU PESERTA PENILAIAN AKHIR TAHUN BERBASIS KOMPUTER', 'PENILAIAN AKHIR SEMESTER BERBASIS KOMPUTER', '1.5', '192.168.0.200/smkwi', 'Asia/Jakarta', 'pusat', 'WI001', '', 'localhost', 'root', 'deyar123', 'cbtcandy24php7', '$2y$10$NVtT10PBQcCcw', '192.168.1.4:1997/candynano', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sinkron`
--

CREATE TABLE `sinkron` (
  `nama_data` varchar(50) NOT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL,
  `status_sinkron` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sinkron`
--

INSERT INTO `sinkron` (`nama_data`, `jumlah`, `tanggal`, `status_sinkron`) VALUES
('DATA1', '0', '2020-05-30 00:40:39', 1),
('DATA2', '0', '2020-05-30 00:40:09', 1),
('DATA3', '0', '2020-05-30 00:40:12', 1),
('DATA4', '0', '2020-05-30 00:40:14', 1),
('DATA5', '1', '2020-05-30 00:40:15', 1),
('DATA6', '52', '2020-05-30 00:40:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` varchar(11) NOT NULL,
  `idpk` varchar(10) NOT NULL,
  `nis` varchar(30) NOT NULL,
  `no_peserta` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `level` varchar(5) NOT NULL,
  `ruang` varchar(10) NOT NULL,
  `sesi` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `server` varchar(50) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `nisn` varchar(50) NOT NULL,
  `online` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kelas`, `idpk`, `nis`, `no_peserta`, `nama`, `level`, `ruang`, `sesi`, `username`, `password`, `foto`, `server`, `agama`, `nisn`, `online`) VALUES
(1, 'XIITP', 'TP', '151610041', '12-248-001-8', 'Ade Saputra', 'XII', 'R1', 1, 'hs001', 'ps001', 'siswa.png', '', '', '', 1),
(2, 'XIITP', 'TP', '151610043', '12-248-002-7', 'Ahmad Fauzi', 'XII', 'R1', 1, 'hs002', 'ps002', '', '', '', '', 0),
(3, 'XIITP', 'TP', '151610044', '12-248-003-6', 'Ahmad Fauzi', 'XII', 'R1', 1, 'hs003', 'ps003', '', '', '', '', 0),
(4, 'XIITP', 'TP', '151610045', '12-248-004-5', 'Ahmad Juliansyah', 'XII', 'R1', 1, 'hs004', 'ps004', '', '', '', '', 0),
(5, 'XIITP', 'TP', '151610047', '12-248-005-4', 'Algi Julian', 'XII', 'R1', 1, 'hs005', 'ps005', '', '', '', '', 0),
(6, 'XIITP', 'TP', '151610048', '12-248-006-3', 'Anas Aditya', 'XII', 'R1', 1, 'hs006', 'ps006', '', '', '', '', 0),
(7, 'XIITP', 'TP', '151610049', '12-248-007-2', 'Andre Irawan', 'XII', 'R1', 1, 'hs007', 'ps007', '', '', '', '', 0),
(8, 'XIITP', 'TP', '151610042', '12-248-008-9', 'Andrian Al Viansyah', 'XII', 'R1', 1, 'hs008', 'ps008', '', '', '', '', 0),
(9, 'XIITP', 'TP', '151610050', '12-248-009-8', 'Andrian Maulana', 'XII', 'R1', 1, 'hs009', 'ps009', '', '', '', '', 0),
(10, 'XIITP', 'TP', '151610051', '12-248-010-7', 'Bambang Reza Umbara', 'XII', 'R1', 1, 'hs010', 'ps010', '', '', '', '', 0),
(11, 'XIITP', 'TP', '151610052', '12-248-011-6', 'Ferdi Hasan', 'XII', 'R1', 1, 'hs011', 'ps011', '', '', '', '', 0),
(12, 'XIITP', 'TP', '151610053', '12-248-012-5', 'Guntur Adthia Bagaskara', 'XII', 'R1', 1, 'hs012', 'ps012', '', '', '', '', 0),
(13, 'XIITP', 'TP', '151610055', '12-248-013-4', 'Harun Syahroji Iqmal', 'XII', 'R1', 2, 'hs013', 'ps013', '', '', '', '', 0),
(14, 'XIITP', 'TP', '151610054', '12-248-014-3', 'Haryadi Sajali', 'XII', 'R1', 2, 'hs014', 'ps014', '', '', '', '', 0),
(15, 'XIITP', 'TP', '151610057', '12-248-015-2', 'Ismail', 'XII', 'R1', 2, 'hs015', 'ps015', '', '', '', '', 0),
(16, 'XIITP', 'TP', '151610062', '12-248-016-9', 'Muchtar Gana', 'XII', 'R1', 2, 'hs016', 'ps016', '', '', '', '', 0),
(17, 'XIITP', 'TP', '151610058', '12-248-017-8', 'Muhamad Abdul Rahman', 'XII', 'R1', 2, 'hs017', 'ps017', '', '', '', '', 0),
(18, 'XIITP', 'TP', '151610063', '12-248-018-7', 'Muhamad Ali Hapijudin', 'XII', 'R1', 2, 'hs018', 'ps018', '', '', '', '', 0),
(19, 'XIITP', 'TP', '151610065', '12-248-019-6', 'Muhamad Rizal', 'XII', 'R1', 2, 'hs019', 'ps019', '', '', '', '', 0),
(20, 'XIITP', 'TP', '151610066', '12-248-020-5', 'Muhammad Niji Yuki Huda Sabillah', 'XII', 'R1', 2, 'hs020', 'ps020', '', '', '', '', 0),
(21, 'XIITP', 'TP', '151610059', '12-248-021-4', 'Muhammad Ogi Prayoga S.', 'XII', 'R1', 2, 'hs021', 'ps021', '', '', '', '', 0),
(22, 'XIITP', 'TP', '151610067', '12-248-022-3', 'Niko', 'XII', 'R1', 2, 'hs022', 'ps022', '', '', '', '', 0),
(23, 'XIITP', 'TP', '151610068', '12-248-023-2', 'Rahma Ahmada', 'XII', 'R1', 2, 'hs023', 'ps023', '', '', '', '', 0),
(24, 'XIITP', 'TP', '151610070', '12-248-024-9', 'Renaldi', 'XII', 'R1', 2, 'hs024', 'ps024', '', '', '', '', 0),
(25, 'XIITP', 'TP', '151610069', '12-248-025-8', 'Renaldi', 'XII', 'R1', 2, 'hs025', 'ps025', '', '', '', '', 0),
(26, 'XIITP', 'TP', '151610072', '12-248-026-7', 'Rico Dwi Addrian Fattah', 'XII', 'R1', 2, 'hs026', 'ps026', '', '', '', '', 0),
(27, 'XIITP', 'TP', '151610073', '12-248-027-6', 'Riki Riyanto', 'XII', 'R1', 2, 'hs027', 'ps027', '', '', '', '', 0),
(28, 'XIITP', 'TP', '151610074', '12-248-028-5', 'Riki S', 'XII', 'R1', 2, 'hs028', 'ps028', '', '', '', '', 0),
(29, 'XIITP', 'TP', '151610075', '12-248-029-4', 'Rudi Hartono', 'XII', 'R1', 2, 'hs029', 'ps029', '', '', '', '', 0),
(30, 'XIITP', 'TP', '151610076', '12-248-030-3', 'Saipul Anwar', 'XII', 'R1', 3, 'hs030', 'ps030', '', '', '', '', 0),
(31, 'XIITP', 'TP', '151610077', '12-248-031-2', 'Satya Pratama', 'XII', 'R1', 3, 'hs031', 'ps031', '', '', '', '', 0),
(32, 'XIITP', 'TP', '151610078', '12-248-032-9', 'Sutrisno', 'XII', 'R1', 3, 'hs032', 'ps032', '', '', '', '', 0),
(33, 'XIITP', 'TP', '151610079', '12-248-033-8', 'Syarif', 'XII', 'R1', 3, 'hs033', 'ps033', '', '', '', '', 0),
(34, 'XIITP', 'TP', '151610081', '12-248-034-7', 'Yobi Pratama', 'XII', 'R1', 3, 'hs034', 'ps034', '', '', '', '', 0),
(35, 'XIITKR', 'TKR', '151610083', '12-248-035-6', 'Adittiya', 'XII', 'R1', 3, 'hs035', 'ps035', '', '', '', '', 0),
(36, 'XIITKR', 'TKR', '151610084', '12-248-036-5', 'Aef saefullah EDK', 'XII', 'R1', 3, 'hs036', 'ps036', '', '', '', '', 0),
(37, 'XIITKR', 'TKR', '151610085', '12-248-037-4', 'Ahmad', 'XII', 'R1', 3, 'hs037', 'ps037', '', '', '', '', 0),
(38, 'XIITKR', 'TKR', '151610086', '12-248-038-3', 'Ahmad dani', 'XII', 'R1', 3, 'hs038', 'ps038', '', '', '', '', 0),
(39, 'XIITKR', 'TKR', '151610089', '12-248-039-2', 'Amar', 'XII', 'R1', 3, 'hs039', 'ps039', '', '', '', '', 0),
(40, 'XIITKR', 'TKR', '151610090', '12-248-040-9', 'Andi', 'XII', 'R1', 3, 'hs040', 'ps040', '', '', '', '', 0),
(41, 'XIITKR', 'TKR', '151610091', '12-248-041-8', 'Anggi Julian Purnama', 'XII', 'R1', 3, 'hs041', 'ps041', '', '', '', '', 0),
(42, 'XIITKR', 'TKR', '151610092', '12-248-042-7', 'Ardiansyah', 'XII', 'R1', 3, 'hs042', 'ps042', '', '', '', '', 0),
(43, 'XIITKR', 'TKR', '151610093', '12-248-043-6', 'Aryanto', 'XII', 'R1', 3, 'hs043', 'ps043', '', '', '', '', 0),
(44, 'XIITKR', 'TKR', '151610094', '12-248-044-5', 'Awaludin', 'XII', 'R1', 3, 'hs044', 'ps044', '', '', '', '', 0),
(45, 'XIITKR', 'TKR', '151610096', '12-248-045-4', 'Dede Ahmad Pauji', 'XII', 'R1', 3, 'hs045', 'ps045', '', '', '', '', 0),
(46, 'XIITKR', 'TKR', '151610099', '12-248-046-3', 'Egi Ariansyah', 'XII', 'R1', 3, 'hs046', 'ps046', '', '', '', '', 0),
(47, 'XIITKR', 'TKR', '151610100', '12-248-047-2', 'Erdin', 'XII', 'R1', 3, 'hs047', 'ps047', '', '', '', '', 0),
(48, 'XIITKR', 'TKR', '151610101', '12-248-048-9', 'Fajar Ramadhan', 'XII', 'R1', 3, 'hs048', 'ps048', '', '', '', '', 0),
(49, 'XIITKR', 'TKR', '151610102', '12-248-049-8', 'Fiky Zulfikar', 'XII', 'R1', 3, 'hs049', 'ps049', '', '', '', '', 0),
(50, 'XIITKR', 'TKR', '151610103', '12-248-050-7', 'Habibi', 'XII', 'R1', 3, 'hs050', 'ps050', '', '', '', '', 0),
(51, 'XIITKR', 'TKR', '151610104', '12-248-051-6', 'Handriyansyah Wijaya', 'XII', 'R1', 3, 'hs051', 'ps051', '', '', '', '', 0),
(52, 'XIITKR', 'TKR', '151610128', '12-248-052-5', 'Herlangga Supardi', 'XII', 'R1', 3, 'hs052', 'ps052', '', '', '', '', 0),
(53, 'XIITKR', 'TKR', '151610106', '12-248-053-4', 'Ibnu Mujahidin', 'XII', 'R1', 3, 'hs053', 'ps053', '', '', '', '', 0),
(54, 'XIITKR', 'TKR', '151610107', '12-248-054-3', 'Kasan Wijaya Kusuma', 'XII', 'R1', 3, 'hs054', 'ps054', '', '', '', '', 0),
(55, 'XIITKR', 'TKR', '151610109', '12-248-055-2', 'Muhamad Aldi Ardiansyah', 'XII', 'R1', 3, 'hs055', 'ps055', '', '', '', '', 0),
(56, 'XIITKR', 'TKR', '151610108', '12-248-056-9', 'Muhammad Sutrisno', 'XII', 'R1', 1, 'hs056', 'ps056', '', '', '', '', 0),
(57, 'XIITKR', 'TKR', '151610110', '12-248-057-8', 'Muhammad Ramdan', 'XII', 'R1', 1, 'hs057', 'ps057', '', '', '', '', 0),
(58, 'XIITKR', 'TKR', '151610111', '12-248-058-7', 'Nur Arifin', 'XII', 'R1', 1, 'hs058', 'ps058', '', '', '', '', 0),
(59, 'XIITKR', 'TKR', '151610112', '12-248-059-6', 'Riyo Wijaya', 'XII', 'R1', 1, 'hs059', 'ps059', '', '', '', '', 0),
(60, 'XIITKR', 'TKR', '151610113', '12-248-060-5', 'Rizal Maulana Aziz', 'XII', 'R1', 1, 'hs060', 'ps060', '', '', '', '', 0),
(61, 'XIITKR', 'TKR', '151610114', '12-248-061-4', 'Robi Darwis', 'XII', 'R1', 1, 'hs061', 'ps061', '', '', '', '', 0),
(62, 'XIITKR', 'TKR', '151610115', '12-248-062-3', 'Roni Sahroni', 'XII', 'R1', 1, 'hs062', 'ps062', '', '', '', '', 0),
(63, 'XIITKR', 'TKR', '151610117', '12-248-063-2', 'Saemi Al Rasyid', 'XII', 'R1', 1, 'hs063', 'ps063', '', '', '', '', 0),
(64, 'XIITKR', 'TKR', '151610118', '12-248-064-9', 'Said Abdullah', 'XII', 'R1', 1, 'hs064', 'ps064', '', '', '', '', 0),
(65, 'XIITKR', 'TKR', '151610119', '12-248-065-8', 'Saripudin', 'XII', 'R1', 1, 'hs065', 'ps065', '', '', '', '', 0),
(66, 'XIITKR', 'TKR', '151610123', '12-248-066-7', 'Ahmad Faisal', 'XII', 'R1', 1, 'hs066', 'ps066', '', '', '', '', 0),
(67, 'XIITKR', 'TKR', '151610124', '12-248-067-6', 'Aksal Sobari', 'XII', 'R1', 1, 'hs067', 'ps067', '', '', '', '', 0),
(68, 'XIITKR', 'TKR', '151610125', '12-248-068-5', 'Alfian', 'XII', 'R1', 1, 'hs068', 'ps068', '', '', '', '', 0),
(69, 'XIITKR', 'TKR', '151610126', '12-248-069-4', 'Arsad sopian', 'XII', 'R1', 1, 'hs069', 'ps069', '', '', '', '', 0),
(70, 'XIITKR', 'TKR', '151610127', '12-248-070-3', 'Dede Maulana', 'XII', 'R1', 1, 'hs070', 'ps070', '', '', '', '', 0),
(71, 'XIITKR', 'TKR', '151610129', '12-248-071-2', 'Junaedi', 'XII', 'R1', 1, 'hs071', 'ps071', '', '', '', '', 0),
(72, 'XIITKR', 'TKR', '151610168', '12-248-072-9', 'Muhamad Fikri Fahmi Kurniadi', 'XII', 'R1', 1, 'hs072', 'ps072', '', '', '', '', 0),
(73, 'XIITKR', 'TKR', '151610130', '12-248-073-8', 'Muhamad Kevin Fadli Fauzi', 'XII', 'R1', 2, 'hs073', 'ps073', '', '', '', '', 0),
(74, 'XIITKR', 'TKR', '151610132', '12-248-074-7', 'Muhamad Rifki Saputra', 'XII', 'R1', 2, 'hs074', 'ps074', '', '', '', '', 0),
(75, 'XIITKR', 'TKR', '151610133', '12-248-075-6', 'Padrul Cahyadi', 'XII', 'R1', 2, 'hs075', 'ps075', '', '', '', '', 0),
(76, 'XIITKR', 'TKR', '151610169', '12-248-076-5', 'Pentin Alamsyah', 'XII', 'R1', 2, 'hs076', 'ps076', '', '', '', '', 0),
(77, 'XIITKR', 'TKR', '151610134', '12-248-077-4', 'Sobri Saputra', 'XII', 'R1', 2, 'hs077', 'ps077', '', '', '', '', 0),
(78, 'XIITKR', 'TKR', '151610135', '12-248-078-3', 'Sukendar', 'XII', 'R1', 2, 'hs078', 'ps078', '', '', '', '', 0),
(79, 'XIITKR', 'TKR', '151610120', '12-248-079-2', 'Teguh Nur Sidik', 'XII', 'R1', 2, 'hs079', 'ps079', '', '', '', '', 0),
(80, 'XIITKR', 'TKR', '151610136', '12-248-080-9', 'Tubagus M. Al-Fajri', 'XII', 'R1', 2, 'hs080', 'ps080', '', '', '', '', 0),
(81, 'XIITKR', 'TKR', '151610166', '12-248-081-8', 'Wahyu Pratama', 'XII', 'R1', 2, 'hs081', 'ps081', '', '', '', '', 0),
(82, 'XIITKR', 'TKR', '151610172', '12-248-082-7', 'Wahyudin AZ.', 'XII', 'R1', 2, 'hs082', 'ps082', '', '', '', '', 0),
(83, 'XIITKR', 'TKR', '151610138', '12-248-083-6', 'Wiro Sugianto', 'XII', 'R1', 2, 'hs083', 'ps083', '', '', '', '', 0),
(84, 'XIITKR', 'TKR', '151610121', '12-248-084-5', 'Yogi Priyogo', 'XII', 'R1', 2, 'hs084', 'ps084', '', '', '', '', 0),
(85, 'XIITKR', 'TKR', '151610139', '12-248-085-4', 'Yuda Saputra', 'XII', 'R1', 2, 'hs085', 'ps085', '', '', '', '', 0),
(86, 'XIITKR', 'TKR', '151610140', '12-248-086-3', 'Yuwanda Musyaddir', 'XII', 'R1', 2, 'hs086', 'ps086', '', '', '', '', 0),
(87, 'XIITKJ', 'TKJ', '151610001', '12-248-087-2', 'Anggi Gian Sapitri', 'XII', 'R1', 2, 'hs087', 'ps087', '', '', '', '', 0),
(88, 'XIITKJ', 'TKJ', '151610002', '12-248-088-9', 'Cindy Apriana', 'XII', 'R1', 2, 'hs088', 'ps088', '', '', '', '', 0),
(89, 'XIITKJ', 'TKJ', '151610003', '12-248-089-8', 'Dwi Lestari', 'XII', 'R1', 2, 'hs089', 'ps089', '', '', '', '', 0),
(90, 'XIITKJ', 'TKJ', '151610004', '12-248-090-7', 'Ebih', 'XII', 'R1', 2, 'hs090', 'ps090', '', '', '', '', 0),
(91, 'XIITKJ', 'TKJ', '151610005', '12-248-091-6', 'Elis Saeti Nuraeni', 'XII', 'R1', 3, 'hs091', 'ps091', '', '', '', '', 0),
(92, 'XIITKJ', 'TKJ', '151610006', '12-248-092-5', 'Euis Susilawati', 'XII', 'R1', 3, 'hs092', 'ps092', '', '', '', '', 0),
(93, 'XIITKJ', 'TKJ', '151610007', '12-248-093-4', 'Fahmi arni', 'XII', 'R1', 3, 'hs093', 'ps093', '', '', '', '', 0),
(94, 'XIITKJ', 'TKJ', '151610008', '12-248-094-3', 'Fitri Widiasari', 'XII', 'R1', 3, 'hs094', 'ps094', '', '', '', '', 0),
(95, 'XIITKJ', 'TKJ', '151610009', '12-248-095-2', 'Gaby Cantika Oktavia', 'XII', 'R1', 3, 'hs095', 'ps095', '', '', '', '', 0),
(96, 'XIITKJ', 'TKJ', '151610010', '12-248-096-9', 'Haena Hermawati Yuningsih', 'XII', 'R1', 3, 'hs096', 'ps096', '', '', '', '', 0),
(97, 'XIITKJ', 'TKJ', '151610011', '12-248-097-8', 'Karlina', 'XII', 'R1', 3, 'hs097', 'ps097', '', '', '', '', 0),
(98, 'XIITKJ', 'TKJ', '151610012', '12-248-098-7', 'Kurniawati', 'XII', 'R1', 3, 'hs098', 'ps098', '', '', '', '', 0),
(99, 'XIITKJ', 'TKJ', '151610013', '12-248-099-6', 'Ladina al zannah chandra', 'XII', 'R1', 3, 'hs099', 'ps099', '', '', '', '', 0),
(100, 'XIITKJ', 'TKJ', '151610014', '12-248-100-5', 'Laras Ayu Asmanih', 'XII', 'R1', 3, 'hs100', 'ps100', '', '', '', '', 0),
(101, 'XIITKJ', 'TKJ', '151610015', '12-248-101-4', 'Lastri Septriani', 'XII', 'R1', 3, 'hs101', 'ps101', '', '', '', '', 0),
(102, 'XIITKJ', 'TKJ', '151610016', '12-248-102-3', 'Lisah Fitri Kurnia', 'XII', 'R1', 3, 'hs102', 'ps102', '', '', '', '', 0),
(103, 'XIITKJ', 'TKJ', '151610018', '12-248-103-2', 'Lutfi Wisti Nandasari', 'XII', 'R2', 3, 'hs103', 'ps103', '', '', '', '', 0),
(104, 'XIITKJ', 'TKJ', '151610019', '12-248-104-9', 'Maya Karmanih', 'XII', 'R2', 3, 'hs104', 'ps104', '', '', '', '', 0),
(105, 'XIITKJ', 'TKJ', '151610020', '12-248-105-8', 'Mayang Sari', 'XII', 'R2', 3, 'hs105', 'ps105', '', '', '', '', 0),
(106, 'XIITKJ', 'TKJ', '151610021', '12-248-106-7', 'Mayang Sari Wati', 'XII', 'R2', 3, 'hs106', 'ps106', '', '', '', '', 0),
(107, 'XIITKJ', 'TKJ', '151610022', '12-248-107-6', 'Megawati', 'XII', 'R2', 1, 'hs107', 'ps107', '', '', '', '', 0),
(108, 'XIITKJ', 'TKJ', '151610023', '12-248-108-5', 'Narsih Agus Priyanti', 'XII', 'R2', 1, 'hs108', 'ps108', '', '', '', '', 0),
(109, 'XIITKJ', 'TKJ', '151610024', '12-248-109-4', 'Nuraina', 'XII', 'R2', 1, 'hs109', 'ps109', '', '', '', '', 0),
(110, 'XIITKJ', 'TKJ', '151610025', '12-248-110-3', 'Pita Kaputri', 'XII', 'R2', 1, 'hs110', 'ps110', '', '', '', '', 0),
(111, 'XIITKJ', 'TKJ', '151610026', '12-248-111-2', 'Putri Ayu Lestari', 'XII', 'R2', 1, 'hs111', 'ps111', '', '', '', '', 0),
(112, 'XIITKJ', 'TKJ', '151610027', '12-248-112-9', 'Putri Hagita', 'XII', 'R2', 1, 'hs112', 'ps112', '', '', '', '', 0),
(113, 'XIITKJ', 'TKJ', '151610028', '12-248-113-8', 'Rasti', 'XII', 'R2', 1, 'hs113', 'ps113', '', '', '', '', 0),
(114, 'XIITKJ', 'TKJ', '151610029', '12-248-114-7', 'Rizky Khofifah', 'XII', 'R2', 1, 'hs114', 'ps114', '', '', '', '', 0),
(115, 'XIITKJ', 'TKJ', '151610030', '12-248-115-6', 'Sahroni', 'XII', 'R2', 1, 'hs115', 'ps115', '', '', '', '', 0),
(116, 'XIITKJ', 'TKJ', '151610031', '12-248-116-5', 'Samah Maesaroh', 'XII', 'R2', 1, 'hs116', 'ps116', '', '', '', '', 0),
(117, 'XIITKJ', 'TKJ', '151610032', '12-248-117-4', 'Sarmila Febyola Putri', 'XII', 'R2', 1, 'hs117', 'ps117', '', '', '', '', 0),
(118, 'XIITKJ', 'TKJ', '151610033', '12-248-118-3', 'Silpi Damayanti', 'XII', 'R2', 1, 'hs118', 'ps118', '', '', '', '', 0),
(119, 'XIITKJ', 'TKJ', '151610034', '12-248-119-2', 'Siti Kartini', 'XII', 'R2', 1, 'hs119', 'ps119', '', '', '', '', 0),
(120, 'XIITKJ', 'TKJ', '151610035', '12-248-120-9', 'Siti Masitoh', 'XII', 'R2', 1, 'hs120', 'ps120', '', '', '', '', 0),
(121, 'XIITKJ', 'TKJ', '151610036', '12-248-121-8', 'Suci Selawati', 'XII', 'R2', 2, 'hs121', 'ps121', '', '', '', '', 0),
(122, 'XIITKJ', 'TKJ', '151610037', '12-248-122-7', 'Tania Pratika', 'XII', 'R2', 2, 'hs122', 'ps122', '', '', '', '', 0),
(123, 'XIITKJ', 'TKJ', '151610038', '12-248-123-6', 'Tarsimah D.', 'XII', 'R2', 2, 'hs123', 'ps123', '', '', '', '', 0),
(124, 'XIITKJ', 'TKJ', '151610039', '12-248-124-5', 'Trisna Shalamshah', 'XII', 'R2', 2, 'hs124', 'ps124', '', '', '', '', 0),
(125, 'XIITKJ', 'TKJ', '151610040', '12-248-125-4', 'Yoga Maulana Atmaja', 'XII', 'R2', 2, 'hs125', 'ps125', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nomor` int(5) NOT NULL,
  `soal` longtext CHARACTER SET utf8 NOT NULL,
  `jenis` int(1) NOT NULL,
  `pilA` longtext CHARACTER SET utf8 NOT NULL,
  `pilB` longtext CHARACTER SET utf8 NOT NULL,
  `pilC` longtext CHARACTER SET utf8 NOT NULL,
  `pilD` longtext CHARACTER SET utf8 NOT NULL,
  `pilE` longtext CHARACTER SET utf8 NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `file` text DEFAULT NULL,
  `file1` text DEFAULT NULL,
  `fileA` text DEFAULT NULL,
  `fileB` text DEFAULT NULL,
  `fileC` text DEFAULT NULL,
  `fileD` text DEFAULT NULL,
  `fileE` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id_soal`, `id_mapel`, `nomor`, `soal`, `jenis`, `pilA`, `pilB`, `pilC`, `pilD`, `pilE`, `jawaban`, `file`, `file1`, `fileA`, `fileB`, `fileC`, `fileD`, `fileE`) VALUES
(1, 1, 1, '<p>aaa</p>', 1, '<p>aa</p>', '<p>aa</p>', '<p>aa</p>', '<p>aa</p>', '<p>aa</p>', 'A', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id_token` int(11) NOT NULL,
  `token` varchar(6) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `masa_berlaku` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id_token`, `token`, `time`, `masa_berlaku`) VALUES
(1, 'YASEDM', '2020-12-07 09:25:40', '00:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `id_ujian` int(5) NOT NULL,
  `id_pk` varchar(10) NOT NULL,
  `id_guru` int(5) NOT NULL,
  `id_mapel` int(5) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `lama_ujian` int(5) NOT NULL,
  `tgl_ujian` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `waktu_ujian` time NOT NULL,
  `selesai_ujian` time NOT NULL,
  `level` varchar(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `siswa` varchar(255) NOT NULL,
  `sesi` varchar(1) NOT NULL,
  `acak` int(1) NOT NULL,
  `acak_opsi` int(1) NOT NULL,
  `token` int(1) NOT NULL,
  `status` int(3) NOT NULL,
  `hasil` int(2) NOT NULL,
  `kkm` varchar(50) NOT NULL,
  `ulang` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`id_ujian`, `id_pk`, `id_guru`, `id_mapel`, `kode_ujian`, `nama`, `jml_soal`, `jml_esai`, `bobot_pg`, `opsi`, `bobot_esai`, `tampil_pg`, `tampil_esai`, `lama_ujian`, `tgl_ujian`, `tgl_selesai`, `waktu_ujian`, `selesai_ujian`, `level`, `kelas`, `siswa`, `sesi`, `acak`, `acak_opsi`, `token`, `status`, `hasil`, `kkm`, `ulang`) VALUES
(2, 'semua', 2, 1, 'PAS', 'TEST', 1, 0, 100, 5, 0, 1, 0, 1, '2021-03-02 17:00:00', '2021-03-02 23:00:00', '17:00:00', '00:00:00', 'semua', 'a:1:{i:0;s:5:\"semua\";}', '', '1', 1, 1, 0, 1, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `walikls`
--

CREATE TABLE `walikls` (
  `idwali` varchar(3) NOT NULL,
  `id_pengawas` varchar(3) NOT NULL,
  `id_kelas` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walikls`
--

INSERT INTO `walikls` (`idwali`, `id_pengawas`, `id_kelas`) VALUES
('32', '32', 'X-TKJ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `file_pendukung`
--
ALTER TABLE `file_pendukung`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `hasil_jawaban`
--
ALTER TABLE `hasil_jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `hasil_nilai`
--
ALTER TABLE `hasil_nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`kode_level`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`,`idpk`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`mapel_id`,`kode_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `pengacak`
--
ALTER TABLE `pengacak`
  ADD PRIMARY KEY (`id_pengacak`);

--
-- Indexes for table `pengacakopsi`
--
ALTER TABLE `pengacakopsi`
  ADD PRIMARY KEY (`id_pengacak`);

--
-- Indexes for table `pengawas`
--
ALTER TABLE `pengawas`
  ADD PRIMARY KEY (`id_pengawas`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `pk`
--
ALTER TABLE `pk`
  ADD PRIMARY KEY (`id_pk`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`kode_ruang`);

--
-- Indexes for table `savsoft_options`
--
ALTER TABLE `savsoft_options`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `savsoft_qbank`
--
ALTER TABLE `savsoft_qbank`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `sesi`
--
ALTER TABLE `sesi`
  ADD PRIMARY KEY (`kode_sesi`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `sinkron`
--
ALTER TABLE `sinkron`
  ADD PRIMARY KEY (`nama_data`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`id_ujian`);

--
-- Indexes for table `walikls`
--
ALTER TABLE `walikls`
  ADD PRIMARY KEY (`idwali`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `file_pendukung`
--
ALTER TABLE `file_pendukung`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hasil_jawaban`
--
ALTER TABLE `hasil_jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hasil_nilai`
--
ALTER TABLE `hasil_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `mapel_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengacak`
--
ALTER TABLE `pengacak`
  MODIFY `id_pengacak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengacakopsi`
--
ALTER TABLE `pengacakopsi`
  MODIFY `id_pengacak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengawas`
--
ALTER TABLE `pengawas`
  MODIFY `id_pengawas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `savsoft_options`
--
ALTER TABLE `savsoft_options`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `savsoft_qbank`
--
ALTER TABLE `savsoft_qbank`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `id_ujian` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
