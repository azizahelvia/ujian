-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2021 at 12:09 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uts_smkwi`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `sesi` varchar(10) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `ikut` varchar(10) NOT NULL,
  `susulan` varchar(10) NOT NULL,
  `jumlahujian` varchar(50) NOT NULL,
  `no_susulan` text NOT NULL,
  `mulai` varchar(10) NOT NULL,
  `selesai` varchar(10) NOT NULL,
  `nama_proktor` varchar(50) NOT NULL,
  `nip_proktor` varchar(50) NOT NULL,
  `nama_pengawas` varchar(50) NOT NULL,
  `nip_pengawas` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  `tgl_ujian` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumentasi`
--

CREATE TABLE `dokumentasi` (
  `id_file` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `ukuran_file` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tgl_upload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumentasi`
--

INSERT INTO `dokumentasi` (`id_file`, `nama_file`, `tipe_file`, `ukuran_file`, `file`, `tgl_upload`) VALUES
(1, 'PANDUAN PENGERJAAN SOAL', 'pdf', '260747', 'PANDUAN PENGERJAAN SOAL.pdf', '2021-03-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `file_pendukung`
--

CREATE TABLE `file_pendukung` (
  `id_file` int(11) NOT NULL,
  `id_mapel` int(11) DEFAULT 0,
  `nama_file` varchar(50) DEFAULT NULL,
  `status_file` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file_pendukung`
--

INSERT INTO `file_pendukung` (`id_file`, `id_mapel`, `nama_file`, `status_file`) VALUES
(1, 1, 'UJNO1.JPG', NULL),
(2, 1, 'UJNO4.JPG', NULL),
(3, 1, 'UJNO7.JPG', NULL),
(4, 1, 'UJNO16.jpg', NULL),
(5, 1, '1_13_1.JPG', NULL),
(6, 1, '1_8_2.JPG', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_jawaban`
--

CREATE TABLE `hasil_jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_nilai`
--

CREATE TABLE `hasil_nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) NOT NULL,
  `ujian_selesai` varchar(20) NOT NULL,
  `jml_benar` int(10) NOT NULL,
  `jml_salah` int(10) NOT NULL,
  `nilai_esai` varchar(10) NOT NULL,
  `skor` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  `status` int(1) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `hasil` int(2) NOT NULL,
  `online` int(1) NOT NULL,
  `blok` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama`, `status`) VALUES
('PAS', 'PENILAIAN AKHIR SEMESTER', 'tidak'),
('PAT', 'PENILAIAN AKHIR TAHUN', 'tidak'),
('PH', 'PENILAIAN HARIAN', 'tidak'),
('PTS', 'PENILAIAN TENGAH SEMESTER', 'aktif'),
('USBK', 'UJIAN SEKOLAH BERBASIS KOMPUTER', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` varchar(11) NOT NULL,
  `level` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `level`, `nama`) VALUES
('XBGA', 'X', 'XBGA'),
('XBGB', 'X', 'XBGB'),
('XIBGA', 'XI', 'XIBGA'),
('XIBGB', 'XI', 'XIBGB'),
('XIBGC', 'XI', 'XIBGC'),
('XIPHA', 'XI', 'XIPHA'),
('XIPHB', 'XI', 'XIPHB'),
('XITKJ', 'XI', 'XITKJ'),
('XPHA', 'X', 'XPHA'),
('XPHB', 'X', 'XPHB'),
('XTKJ', 'X', 'XTKJ');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `kode_level` varchar(5) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`kode_level`, `keterangan`) VALUES
('X', 'X'),
('XI', 'XI');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `text` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `idpk` varchar(10) NOT NULL,
  `idguru` varchar(3) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `level` varchar(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `kkm` int(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `siswa` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(2) NOT NULL,
  `statusujian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `idpk`, `idguru`, `nama`, `jml_soal`, `jml_esai`, `tampil_pg`, `tampil_esai`, `bobot_pg`, `bobot_esai`, `level`, `opsi`, `kkm`, `kelas`, `siswa`, `date`, `status`, `statusujian`) VALUES
(1, 'semua', '6', 'TO-PTS', 25, 5, 25, 5, 70, 30, 'semua', 5, 75, 'a:1:{i:0;s:5:\"semua\";}', '', '2021-03-09 04:35:52', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `mapel_id` int(5) NOT NULL,
  `kode_mapel` varchar(20) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL,
  `mata_pelajaran_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`mapel_id`, `kode_mapel`, `nama_mapel`, `mata_pelajaran_id`) VALUES
(1, 'TO-PTS', 'Try Out Penilaian Tengah Semester', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) NOT NULL,
  `ujian_selesai` varchar(20) NOT NULL,
  `jml_benar` int(10) NOT NULL,
  `jml_salah` int(10) NOT NULL,
  `nilai_esai` varchar(10) NOT NULL,
  `skor` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  `status` int(1) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `hasil` int(2) NOT NULL,
  `online` int(1) NOT NULL,
  `blok` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengacak`
--

CREATE TABLE `pengacak` (
  `id_pengacak` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` longtext NOT NULL,
  `id_esai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengacakopsi`
--

CREATE TABLE `pengacakopsi` (
  `id_pengacak` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL DEFAULT 0,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengawas`
--

CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `fotoguru` text NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengawas`
--

INSERT INTO `pengawas` (`id_pengawas`, `nip`, `nama`, `fotoguru`, `jabatan`, `username`, `password`, `level`) VALUES
(1, '-', 'Administrator', '', '', 'admin', '$2y$10$zFaLAI2osvMOJ9rIcOe75.fvDS6vEBwvd95ynKBJLJjnIXkYxy6/.', 'admin'),
(2, '-', 'Abdul Munir HMA, M.Pd.', '', '', 'munir', 'admin123', 'guru'),
(3, '-', 'Abdul Sukur, S.Kom.', '', '', 'sukur', 'admin123', 'guru'),
(4, '-', 'Ifo Indriati, S.Pd.', '', '', 'ifo', 'admin123', 'guru'),
(5, '-', 'Puput Riyani, S.Pd.', '', '', 'puput', 'admin123', 'guru'),
(6, '-', 'Ina Nopiana, S.Pd.', '', '', 'ina', 'admin123', 'guru'),
(7, '-', 'Nur\'aliyah, S. Pd.', '', '', 'lia', 'admin123', 'guru'),
(8, '-', 'H. Mulyadi, S.Ag.', '', '', 'mulyadi', 'admin123', 'guru'),
(9, '-', 'Sopan Sopiana, M.Pd.', '', '', 'sopan', 'admin123', 'guru'),
(10, '-', 'Dewi Hastuti, S.Pd', '', '', 'dewi', 'admin123', 'guru'),
(11, '-', 'Jamaluddin, S.Ag.', '', '', 'jamal', 'admin123', 'guru'),
(12, '-', 'Ginanjar Sukma Wjaya,Amd', '', '', 'ginanjar', 'admin123', 'guru'),
(13, '-', 'Saidah, S.Pd', '', '', 'saidah', 'admin123', 'guru'),
(14, '-', 'Nanan Supriatna,Amd', '', '', 'nanan', 'admin123', 'guru'),
(15, '-', 'M. Fuad Usman, MM', '', '', 'fuad', 'admin123', 'guru'),
(16, '-', 'Anisa Kanti Mulyawati, S.Pd', '', '', 'anisa', 'admin123', 'guru'),
(17, '-', 'Sri Indriyani, S.Pd', '', '', 'indri', 'admin123', 'guru'),
(18, '-', 'Suida Afriyani, S.Si', '', '', 'suida', 'admin123', 'guru'),
(19, '-', 'Weniarti, S.Pd', '', '', 'weni', 'admin123', 'guru'),
(20, '-', 'Dra. Betty Ria Panjaitan', '', '', 'betty', 'admin123', 'guru'),
(21, '-', 'Laily Maghrifah, S.Pd', '', '', 'laily', 'admin123', 'guru'),
(22, '-', 'Wiwin Winarti, S.Pd', '', '', 'wiwin', 'admin123', 'guru'),
(23, '-', 'Ulfa Nurvina, S.Pd', '', '', 'ulfa', 'admin123', 'guru'),
(24, '-', 'Ferari, S.Pd', '', '', 'ferari', 'admin123', 'guru'),
(25, '-', 'Deyar  Cipta Rizky', '', '', 'deyarcr', 'admin123', 'guru'),
(26, '-', 'M. Rian Fadhillah, S.Pd', '', '', 'rian', 'admin123', 'guru'),
(27, '-', 'Pardin Nahampun, SE', '', '', 'pardin', 'admin123', 'guru'),
(28, '-', 'Maulina Septiani, S.Pd', '', '', 'maulina', 'admin123', 'guru'),
(29, '-', 'Agus Budi Setiawan', '', '', 'agus', 'admin123', 'guru'),
(30, '-', 'Gilang Frilly R, S.Pd', '', '', 'gilang', 'admin123', 'guru'),
(31, '-', 'Siti Aisyah Nurjannah, S.Pd', '', '', 'siti', 'admin123', 'guru'),
(32, '-', 'Richa Khairani, S.Pd', '', '', 'richa', 'admin123', 'guru'),
(33, '-', 'Taufiq Samsiadi', '', '', 'taufiq', 'admin123', 'guru'),
(34, '-', 'Sumardjono', '', '', 'jhon', 'admin123', 'guru'),
(35, '-', 'Lika Wulandari, S.Pd', '', '', 'lika', 'admin123', 'guru'),
(36, '-', 'Ina Nopiana', '', '', 'inanopiana', '$2y$10$FL/X.1s9KwrbkhJp6rI/7OcRWvJrpV8ESJHbmlf46nW3fmcIn8Wbm', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(5) NOT NULL,
  `type` varchar(30) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `user` int(3) NOT NULL,
  `text` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pk`
--

CREATE TABLE `pk` (
  `id_pk` varchar(10) NOT NULL,
  `program_keahlian` varchar(50) NOT NULL,
  `kode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pk`
--

INSERT INTO `pk` (`id_pk`, `program_keahlian`, `kode`) VALUES
('BOGA', 'BOGA', ''),
('PH', 'PH', ''),
('TKJ', 'TKJ', '');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`kode_ruang`, `keterangan`) VALUES
('R1', 'R1');

-- --------------------------------------------------------

--
-- Table structure for table `savsoft_options`
--

CREATE TABLE `savsoft_options` (
  `oid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `q_option` text NOT NULL,
  `q_option_match` varchar(1000) DEFAULT NULL,
  `score` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `savsoft_qbank`
--

CREATE TABLE `savsoft_qbank` (
  `qid` int(11) NOT NULL,
  `question_type` varchar(100) NOT NULL DEFAULT 'Multiple Choice Single Answer',
  `question` text NOT NULL,
  `description` text NOT NULL,
  `cid` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  `no_time_served` int(11) NOT NULL DEFAULT 0,
  `no_time_corrected` int(11) NOT NULL DEFAULT 0,
  `no_time_incorrected` int(11) NOT NULL DEFAULT 0,
  `no_time_unattempted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `server`
--

CREATE TABLE `server` (
  `kode_server` varchar(20) NOT NULL,
  `nama_server` varchar(30) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `server`
--

INSERT INTO `server` (`kode_server`, `nama_server`, `status`) VALUES
('SMKWI', 'SMKWI', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `sesi`
--

CREATE TABLE `sesi` (
  `kode_sesi` varchar(10) NOT NULL,
  `nama_sesi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sesi`
--

INSERT INTO `sesi` (`kode_sesi`, `nama_sesi`) VALUES
('1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `session_time` varchar(10) NOT NULL,
  `session_hash` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `session_time`, `session_hash`) VALUES
(1, '1447610188', '$2y$10$dt9BTs7FlTXgpactflaXPOSVWrs.wurWsKBGv18JkzolJmHZOj.B.');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `aplikasi` varchar(100) NOT NULL,
  `kode_sekolah` varchar(10) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `jenjang` varchar(5) NOT NULL,
  `kepsek` varchar(50) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `proktor` varchar(50) NOT NULL,
  `nip_proktor` varchar(30) NOT NULL,
  `no_proktor` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `web` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `foto` text NOT NULL,
  `ttd` text NOT NULL,
  `header` text NOT NULL,
  `header_kartu` text NOT NULL,
  `nama_ujian` text NOT NULL,
  `versi` varchar(10) NOT NULL,
  `ip_server` varchar(50) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `server` varchar(50) NOT NULL,
  `id_server` varchar(50) NOT NULL,
  `db_folder` varchar(50) NOT NULL,
  `db_host` varchar(50) NOT NULL,
  `db_user` varchar(50) NOT NULL,
  `db_pass` varchar(50) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `db_token` varchar(50) NOT NULL,
  `db_url` varchar(50) NOT NULL,
  `reset_login` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `aplikasi`, `kode_sekolah`, `sekolah`, `jenjang`, `kepsek`, `nip`, `proktor`, `nip_proktor`, `no_proktor`, `alamat`, `kecamatan`, `kota`, `provinsi`, `telp`, `fax`, `web`, `email`, `logo`, `foto`, `ttd`, `header`, `header_kartu`, `nama_ujian`, `versi`, `ip_server`, `waktu`, `server`, `id_server`, `db_folder`, `db_host`, `db_user`, `db_pass`, `db_name`, `db_token`, `db_url`, `reset_login`) VALUES
(1, 'CBT-PTS', 'K01040131', 'SMK WISATA INDONESIA', 'SMK', 'Abdul Munir, H.MA, M.Pd', '-', 'Deyar Cipra Rizky', '-', '081382053328', 'JL. Raya Lenteng Agung / Jl. Langgar RT 009/03 No. 1 Kode Pos 12520', 'PASAR MINGGU             ', 'JAKARTA SELATAN             ', 'DKI JAKARTA                ', '087777639337', '-', 'smkwisataindonesia.sch.id', 'smkwisataindonesia01@gmail.com', 'dist/img/logo18.png', 'dist/img/foto50.png', 'dist/img/ttd47.png', '', 'KARTU PESERTA PENILAIAN AKHIR TAHUN BERBASIS KOMPUTER', 'PENILAIAN TENGAH SEMESTER', '1.6', '192.168.0.200/smkwi', 'Asia/Jakarta', 'pusat', 'WI001', '', 'localhost', 'root', 'deyar123', 'cbtcandy24php7', '$2y$10$NVtT10PBQcCcw', '192.168.1.4:1997/candynano', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sinkron`
--

CREATE TABLE `sinkron` (
  `nama_data` varchar(50) NOT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL,
  `status_sinkron` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sinkron`
--

INSERT INTO `sinkron` (`nama_data`, `jumlah`, `tanggal`, `status_sinkron`) VALUES
('DATA1', '0', '2020-05-30 00:40:39', 1),
('DATA2', '0', '2020-05-30 00:40:09', 1),
('DATA3', '0', '2020-05-30 00:40:12', 1),
('DATA4', '0', '2020-05-30 00:40:14', 1),
('DATA5', '1', '2020-05-30 00:40:15', 1),
('DATA6', '52', '2020-05-30 00:40:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` varchar(11) NOT NULL,
  `idpk` varchar(10) NOT NULL,
  `nis` varchar(30) NOT NULL,
  `no_peserta` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `level` varchar(5) NOT NULL,
  `ruang` varchar(10) NOT NULL,
  `sesi` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `server` varchar(50) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `nisn` varchar(50) NOT NULL,
  `online` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kelas`, `idpk`, `nis`, `no_peserta`, `nama`, `level`, `ruang`, `sesi`, `username`, `password`, `foto`, `server`, `agama`, `nisn`, `online`) VALUES
(1, 'XTKJ', 'TKJ', '0043604626', '20XTKJ001', 'Adriel Deva Darmawan', 'X', 'R1', 1, 'U001', '1370741', 'siswa.png', 'SMKWI', '', '', 0),
(2, 'XTKJ', 'TKJ', '0042771569', '20XTKJ002', 'Achmad Baihaqi', 'X', 'R1', 1, 'U002', '2465863', 'siswa.png', 'SMKWI', '', '', 0),
(3, 'XTKJ', 'TKJ', '0061278708', '20XTKJ003', 'Ahmad Alvan Fawaz', 'X', 'R1', 1, 'U003', '2486200', 'siswa.png', 'SMKWI', '', '', 0),
(4, 'XTKJ', 'TKJ', '0046915474', '20XTKJ004', 'Athallah Akmal Javier', 'X', 'R1', 1, 'U004', '4663336', 'siswa.png', 'SMKWI', '', '', 0),
(5, 'XTKJ', 'TKJ', '3039802662', '20XTKJ005', 'Athur Zibran Septianda Maskat', 'X', 'R1', 1, 'U005', '4271351', 'siswa.png', 'SMKWI', '', '', 0),
(6, 'XTKJ', 'TKJ', '0049738178', '20XTKJ006', 'Dzikky Dzulfikri', 'X', 'R1', 1, 'U006', '5524179', 'siswa.png', 'SMKWI', '', '', 0),
(7, 'XTKJ', 'TKJ', '0044597916', '20XTKJ007', 'Faiz Rizq Farhanna', 'X', 'R1', 1, 'U007', '1099077', 'siswa.png', 'SMKWI', '', '', 0),
(8, 'XTKJ', 'TKJ', '0052829157', '20XTKJ008', 'Gustof Albani Subing', 'X', 'R1', 1, 'U008', '4641559', 'siswa.png', 'SMKWI', '', '', 0),
(9, 'XTKJ', 'TKJ', '0053796596', '20XTKJ009', 'Labib Fazilla', 'X', 'R1', 1, 'U009', '9804716', 'siswa.png', 'SMKWI', '', '', 0),
(10, 'XTKJ', 'TKJ', '0053254130', '20XTKJ010', 'Muhammad Ardiansyah', 'X', 'R1', 1, 'U010', '2142018', 'siswa.png', 'SMKWI', '', '', 0),
(11, 'XTKJ', 'TKJ', '0055926236', '20XTKJ011', 'Muhammad Farhan Ariansyah', 'X', 'R1', 1, 'U011', '8270751', 'siswa.png', 'SMKWI', '', '', 0),
(12, 'XTKJ', 'TKJ', '0044858396', '20XTKJ012', 'Muhammad Revansyah Putra Listanto', 'X', 'R1', 1, 'U012', '6103750', 'siswa.png', 'SMKWI', '', '', 0),
(13, 'XTKJ', 'TKJ', '0049571070', '20XTKJ013', 'Muhammad Yugi Biham Malik', 'X', 'R1', 1, 'U013', '2999325', 'siswa.png', 'SMKWI', '', '', 0),
(14, 'XTKJ', 'TKJ', '0059914984', '20XTKJ014', 'Muhammad Zaldy Asyabian Zein ', 'X', 'R1', 1, 'U014', '2554535', 'siswa.png', 'SMKWI', '', '', 0),
(15, 'XTKJ', 'TKJ', '0051209216', '20XTKJ015', 'Ricky Santoso', 'X', 'R1', 1, 'U015', '6254313', 'siswa.png', 'SMKWI', '', '', 0),
(16, 'XTKJ', 'TKJ', '0036500859', '20XTKJ016', 'Tirta Maulana', 'X', 'R1', 1, 'U016', '9938683', 'siswa.png', 'SMKWI', '', '', 0),
(17, 'XPHA', 'PH', '0049818404', '20XPHA017', 'Abrar Zidky Muafa Ramadhan', 'X', 'R1', 1, 'U017', '1703868', 'siswa.png', 'SMKWI', '', '', 0),
(18, 'XPHA', 'PH', '0049424929', '20XPHA018', 'Adinda Putri Azahra', 'X', 'R1', 1, 'U018', '8516254', 'siswa.png', 'SMKWI', '', '', 0),
(19, 'XPHA', 'PH', '0048768779', '20XPHA019', 'Ahmad Rizky Syabani', 'X', 'R1', 1, 'U019', '6991464', 'siswa.png', 'SMKWI', '', '', 0),
(20, 'XPHA', 'PH', '0054382837', '20XPHA020', 'Azkiyah Vashti Hasnika', 'X', 'R1', 1, 'U020', '6615622', 'siswa.png', 'SMKWI', '', '', 0),
(21, 'XPHA', 'PH', '0055760870', '20XPHA021', 'Badriyatu Shalihah', 'X', 'R1', 1, 'U021', '4712338', 'siswa.png', 'SMKWI', '', '', 0),
(22, 'XPHA', 'PH', '3057694208', '20XPHA022', 'Danella Audia', 'X', 'R1', 1, 'U022', '3207465', 'siswa.png', 'SMKWI', '', '', 0),
(23, 'XPHA', 'PH', '0049412850', '20XPHA023', 'Eka Priambudi', 'X', 'R1', 1, 'U023', '2963539', 'siswa.png', 'SMKWI', '', '', 0),
(24, 'XPHA', 'PH', '0054125142', '20XPHA024', 'Lovely Az-Zahra', 'X', 'R1', 1, 'U024', '9829633', 'siswa.png', 'SMKWI', '', '', 0),
(25, 'XPHA', 'PH', '0053657658', '20XPHA025', 'Melani Purnama Indriawan', 'X', 'R1', 1, 'U025', '7274926', 'siswa.png', 'SMKWI', '', '', 0),
(26, 'XPHA', 'PH', '0045758859', '20XPHA026', 'Melati Nissya Oktaviyani', 'X', 'R1', 1, 'U026', '4240420', 'siswa.png', 'SMKWI', '', '', 0),
(27, 'XPHA', 'PH', '0044630851', '20XPHA027', 'Muhammad Chairul Albiansah', 'X', 'R1', 1, 'U027', '9257673', 'siswa.png', 'SMKWI', '', '', 0),
(28, 'XPHA', 'PH', '0059899403', '20XPHA028', 'Muhammad Faiq Al Mabrurri', 'X', 'R1', 1, 'U028', '2503785', 'siswa.png', 'SMKWI', '', '', 0),
(29, 'XPHA', 'PH', '0008790101', '20XPHA029', 'Muhammad Iqbal', 'X', 'R1', 1, 'U029', '5027069', 'siswa.png', 'SMKWI', '', '', 0),
(30, 'XPHA', 'PH', '0053898197', '20XPHA030', 'Muhammad Shafwan Akram', 'X', 'R1', 1, 'U030', '3081368', 'siswa.png', 'SMKWI', '', '', 0),
(31, 'XPHA', 'PH', '0046578778', '20XPHA031', 'Naufal Zaki Ardiansyah', 'X', 'R1', 1, 'U031', '7436721', 'siswa.png', 'SMKWI', '', '', 0),
(32, 'XPHA', 'PH', '0052847277', '20XPHA032', 'Putra Wicaksono', 'X', 'R1', 1, 'U032', '6409600', 'siswa.png', 'SMKWI', '', '', 0),
(33, 'XPHA', 'PH', '0042983231', '20XPHA033', 'Rully Ardiansyah', 'X', 'R1', 1, 'U033', '5300094', 'siswa.png', 'SMKWI', '', '', 0),
(34, 'XPHA', 'PH', '0046013814', '20XPHA034', 'Salsabilah', 'X', 'R1', 1, 'U034', '1746966', 'siswa.png', 'SMKWI', '', '', 0),
(35, 'XPHA', 'PH', '0046126130', '20XPHA035', 'Yusna Hadi Purnama', 'X', 'R1', 1, 'U035', '1607626', 'siswa.png', 'SMKWI', '', '', 0),
(36, 'XPHA', 'PH', '0051289561', '20XPHA036', 'Zoetherio Giovani A. Uneputty', 'X', 'R1', 1, 'U036', '8295401', 'siswa.png', 'SMKWI', '', '', 0),
(37, 'XPHB', 'PH', '0044799946', '20XPHB037', 'Ahmad Rizki Saputra', 'X', 'R1', 1, 'U037', '7766241', 'siswa.png', 'SMKWI', '', '', 0),
(38, 'XPHB', 'PH', '0055883433', '20XPHB038', 'Aida Farihah Nurhasanah', 'X', 'R1', 1, 'U038', '3274792', 'siswa.png', 'SMKWI', '', '', 0),
(39, 'XPHB', 'PH', '0051381755', '20XPHB039', 'Albi Asy Syauqi', 'X', 'R1', 1, 'U039', '1950769', 'siswa.png', 'SMKWI', '', '', 0),
(40, 'XPHB', 'PH', '0048638674', '20XPHB040', 'Ananda Vitto Hermansyah', 'X', 'R1', 1, 'U040', '1949807', 'siswa.png', 'SMKWI', '', '', 0),
(41, 'XPHB', 'PH', '3047410204', '20XPHB041', 'Awalludin Hakim', 'X', 'R1', 1, 'U041', '6738558', 'siswa.png', 'SMKWI', '', '', 0),
(42, 'XPHB', 'PH', '0045852041', '20XPHB042', 'Bima Nugraha', 'X', 'R1', 1, 'U042', '8759417', 'siswa.png', 'SMKWI', '', '', 0),
(43, 'XPHB', 'PH', '0047195748', '20XPHB043', 'Desi Tri Wulandari', 'X', 'R1', 1, 'U043', '7555461', 'siswa.png', 'SMKWI', '', '', 0),
(44, 'XPHB', 'PH', '0051434295', '20XPHB044', 'Farrel Ardian Nazhif', 'X', 'R1', 1, 'U044', '1749199', 'siswa.png', 'SMKWI', '', '', 0),
(45, 'XPHB', 'PH', '001338264', '20XPHB045', 'Iren Oktofiqri', 'X', 'R1', 1, 'U045', '1148426', 'siswa.png', 'SMKWI', '', '', 0),
(46, 'XPHB', 'PH', '0047519757', '20XPHB046', 'Meylanie Safitri', 'X', 'R1', 1, 'U046', '6336242', 'siswa.png', 'SMKWI', '', '', 0),
(47, 'XPHB', 'PH', '0046355356', '20XPHB047', 'Muhamad Fiqri Fadillah.', 'X', 'R1', 1, 'U047', '6691975', 'siswa.png', 'SMKWI', '', '', 0),
(48, 'XPHB', 'PH', '0052795814', '20XPHB048', 'Muhammad Rafly', 'X', 'R1', 1, 'U048', '9062845', 'siswa.png', 'SMKWI', '', '', 0),
(49, 'XPHB', 'PH', '0045791509', '20XPHB049', 'Muhammad Nazriel Arrasyid', 'X', 'R1', 1, 'U049', '3033022', 'siswa.png', 'SMKWI', '', '', 0),
(50, 'XPHB', 'PH', '0051995119', '20XPHB050', 'Muhammad Putra Fauzan', 'X', 'R1', 1, 'U050', '7605266', 'siswa.png', 'SMKWI', '', '', 0),
(51, 'XPHB', 'PH', '3057051412', '20XPHB051', 'Muhammad Ridwan Auryno', 'X', 'R1', 1, 'U051', '7480529', 'siswa.png', 'SMKWI', '', '', 0),
(52, 'XPHB', 'PH', '0055814664', '20XPHB052', 'Naufal Ferdiansyah', 'X', 'R1', 1, 'U052', '9290003', 'siswa.png', 'SMKWI', '', '', 0),
(53, 'XPHB', 'PH', '3040865207', '20XPHB053', 'Niken Hardiningrum', 'X', 'R1', 1, 'U053', '3133608', 'siswa.png', 'SMKWI', '', '', 0),
(54, 'XPHB', 'PH', '0059148589', '20XPHB054', 'Renata Aqilah Nabila', 'X', 'R1', 1, 'U054', '4206953', 'siswa.png', 'SMKWI', '', '', 0),
(55, 'XPHB', 'PH', '0059148589', '20XPHB055', 'Sastra Athoriq Riswandriana Putra', 'X', 'R1', 1, 'U055', '6047311', 'siswa.png', 'SMKWI', '', '', 0),
(56, 'XPHB', 'PH', '0051631974', '20XPHB056', 'Tazkia Febriana Yusuf', 'X', 'R1', 1, 'U056', '7012031', 'siswa.png', 'SMKWI', '', '', 0),
(57, 'XPHB', 'PH', '0058315315', '20XPHB057', 'Yessy Kurnia Pratista', 'X', 'R1', 1, 'U057', '3589792', 'siswa.png', 'SMKWI', '', '', 0),
(58, 'XPHB', 'PH', '0043165684', '20XPHB058', 'Zahwa Syahrani', 'X', 'R1', 1, 'U058', '2890823', 'siswa.png', 'SMKWI', '', '', 0),
(59, 'XBGA', 'BOGA', '0047265295', '20XBGA059', 'Adinda Zahra Khairani', 'X', 'R1', 1, 'U059', '1098544', 'siswa.png', 'SMKWI', '', '', 0),
(60, 'XBGA', 'BOGA', '3043513199', '20XBGA060', 'Adzra Fathiya', 'X', 'R1', 1, 'U060', '7684516', 'siswa.png', 'SMKWI', '', '', 0),
(61, 'XBGA', 'BOGA', '3051899036', '20XBGA061', 'Akhtar Rafif Ghifari', 'X', 'R1', 1, 'U061', '9027782', 'siswa.png', 'SMKWI', '', '', 0),
(62, 'XBGA', 'BOGA', '0050932847', '20XBGA062', 'Alda Sekar Nathania Gulo', 'X', 'R1', 1, 'U062', '4197884', 'siswa.png', 'SMKWI', '', '', 0),
(63, 'XBGA', 'BOGA', '0059123495', '20XBGA063', 'Bilansepta Taufan Putra', 'X', 'R1', 1, 'U063', '5116690', 'siswa.png', 'SMKWI', '', '', 0),
(64, 'XBGA', 'BOGA', '0050713778', '20XBGA064', 'Dwiki Kurniawan', 'X', 'R1', 1, 'U064', '7455748', 'siswa.png', 'SMKWI', '', '', 0),
(65, 'XBGA', 'BOGA', '0041240668', '20XBGA065', 'Jihan Nur Fadila', 'X', 'R1', 1, 'U065', '7319906', 'siswa.png', 'SMKWI', '', '', 0),
(66, 'XBGA', 'BOGA', '0055555704', '20XBGA066', 'Metha  Anggraini Safitri', 'X', 'R1', 1, 'U066', '3197733', 'siswa.png', 'SMKWI', '', '', 0),
(67, 'XBGA', 'BOGA', '0051028585', '20XBGA067', 'Muhammad Labib Rava Syifa', 'X', 'R1', 1, 'U067', '7668121', 'siswa.png', 'SMKWI', '', '', 0),
(68, 'XBGA', 'BOGA', '0055614903', '20XBGA068', 'Muhammad Raja Indra Maulana', 'X', 'R1', 1, 'U068', '2938620', 'siswa.png', 'SMKWI', '', '', 0),
(69, 'XBGA', 'BOGA', '0058081643', '20XBGA069', 'Muhammad Syafiq', 'X', 'R1', 1, 'U069', '7873687', 'siswa.png', 'SMKWI', '', '', 0),
(70, 'XBGA', 'BOGA', '0051858192', '20XBGA070', 'Nayla Putri Ahtari', 'X', 'R1', 1, 'U070', '6985164', 'siswa.png', 'SMKWI', '', '', 0),
(71, 'XBGA', 'BOGA', '0054803109', '20XBGA071', 'Nisrina Khuzaimah Khairunisa', 'X', 'R1', 1, 'U071', '7520127', 'siswa.png', 'SMKWI', '', '', 0),
(72, 'XBGA', 'BOGA', '0056893469', '20XBGA072', 'Razkyta Gusti Fajriansyah', 'X', 'R1', 1, 'U072', '6475600', 'siswa.png', 'SMKWI', '', '', 0),
(73, 'XBGA', 'BOGA', '0045728745', '20XBGA073', 'Saefulloh Faturahman ', 'X', 'R1', 1, 'U073', '9234657', 'siswa.png', 'SMKWI', '', '', 0),
(74, 'XBGA', 'BOGA', '0058664085', '20XBGA074', 'Sakhaa\' Faadhilah Zairin', 'X', 'R1', 1, 'U074', '1136426', 'siswa.png', 'SMKWI', '', '', 0),
(75, 'XBGA', 'BOGA', '0045560411', '20XBGA075', 'Siti Aisya Cahyani', 'X', 'R1', 1, 'U075', '2654334', 'siswa.png', 'SMKWI', '', '', 0),
(76, 'XBGA', 'BOGA', '0052111642', '20XBGA076', 'Tarissya Nur Izzati', 'X', 'R1', 1, 'U076', '9484214', 'siswa.png', 'SMKWI', '', '', 0),
(77, 'XBGA', 'BOGA', '0059539832', '20XBGA077', 'Yola Yunita', 'X', 'R1', 1, 'U077', '5656987', 'siswa.png', 'SMKWI', '', '', 0),
(78, 'XBGA', 'BOGA', '0040995228', '20XBGA078', 'Zahra Amelia ', 'X', 'R1', 1, 'U078', '6165846', 'siswa.png', 'SMKWI', '', '', 0),
(79, 'XBGB', 'BOGA', '0055823140', '20XBGB079', 'Alya Fadhilah Dwi Gunawan', 'X', 'R1', 1, 'U079', '5840011', 'siswa.png', 'SMKWI', '', '', 0),
(80, 'XBGB', 'BOGA', '0058441592', '20XBGB080', 'Ananta Meyla Yuuki', 'X', 'R1', 1, 'U080', '9645452', 'siswa.png', 'SMKWI', '', '', 0),
(81, 'XBGB', 'BOGA', '0044154547', '20XBGB081', 'Andika Ekaputra Syahdani', 'X', 'R1', 1, 'U081', '1149200', 'siswa.png', 'SMKWI', '', '', 0),
(82, 'XBGB', 'BOGA', '0035972326', '20XBGB082', 'Angel Oktaviani Tampubolon', 'X', 'R1', 1, 'U082', '6560448', 'siswa.png', 'SMKWI', '', '', 0),
(83, 'XBGB', 'BOGA', '0044990153', '20XBGB083', 'Cindy Kartika Putri', 'X', 'R1', 1, 'U083', '5171947', 'siswa.png', 'SMKWI', '', '', 0),
(84, 'XBGB', 'BOGA', '0053236825', '20XBGB084', 'Daffa Ahmad La Tahzan', 'X', 'R1', 1, 'U084', '9477442', 'siswa.png', 'SMKWI', '', '', 0),
(85, 'XBGB', 'BOGA', '3050661482', '20XBGB085', 'Diva Afrisya', 'X', 'R1', 1, 'U085', '1072888', 'siswa.png', 'SMKWI', '', '', 0),
(86, 'XBGB', 'BOGA', '0050713749', '20XBGB086', 'Handika Wirayuda Rianto', 'X', 'R1', 1, 'U086', '2382849', 'siswa.png', 'SMKWI', '', '', 0),
(87, 'XBGB', 'BOGA', '0044772726', '20XBGB087', 'Laki Buci En Fina', 'X', 'R1', 1, 'U087', '3998699', 'siswa.png', 'SMKWI', '', '', 0),
(88, 'XBGB', 'BOGA', '0050919745', '20XBGB088', 'Metana Putri Fazria', 'X', 'R1', 1, 'U088', '8144945', 'siswa.png', 'SMKWI', '', '', 0),
(89, 'XBGB', 'BOGA', '0056086848', '20XBGB089', 'Muhammad Rama', 'X', 'R1', 1, 'U089', '2922561', 'siswa.png', 'SMKWI', '', '', 0),
(90, 'XBGB', 'BOGA', '0045850315', '20XBGB090', 'Muhamad Sopian Pajar', 'X', 'R1', 1, 'U090', '3313757', 'siswa.png', 'SMKWI', '', '', 0),
(91, 'XBGB', 'BOGA', '0044621187', '20XBGB091', 'Novaldy Ramadhan', 'X', 'R1', 1, 'U091', '6414761', 'siswa.png', 'SMKWI', '', '', 0),
(92, 'XBGB', 'BOGA', '0044711507', '20XBGB092', 'Putra Ramadhoni Awal', 'X', 'R1', 1, 'U092', '2098958', 'siswa.png', 'SMKWI', '', '', 0),
(93, 'XBGB', 'BOGA', '0057088825', '20XBGB093', 'Raymond Jan Reynara', 'X', 'R1', 1, 'U093', '8052576', 'siswa.png', 'SMKWI', '', '', 0),
(94, 'XBGB', 'BOGA', '0053110878', '20XBGB094', 'Shella Dwi Utami', 'X', 'R1', 1, 'U094', '2316630', 'siswa.png', 'SMKWI', '', '', 0),
(95, 'XBGB', 'BOGA', '0044691282', '20XBGB095', 'Suci Aokta Rhamadani', 'X', 'R1', 1, 'U095', '5827892', 'siswa.png', 'SMKWI', '', '', 0),
(96, 'XBGB', 'BOGA', '0056836621', '20XBGB096', 'Syifa Aulia', 'X', 'R1', 1, 'U096', '5305612', 'siswa.png', 'SMKWI', '', '', 0),
(97, 'XBGB', 'BOGA', '0050713765', '20XBGB097', 'Winda Marta Hadz', 'X', 'R1', 1, 'U097', '9630870', 'siswa.png', 'SMKWI', '', '', 0),
(98, 'XBGB', 'BOGA', '3049464253', '20XBGB098', 'Zidan Al-Falah', 'X', 'R1', 1, 'U098', '6931819', 'siswa.png', 'SMKWI', '', '', 0),
(99, 'XBGB', 'BOGA', '0032943444', '20XBGB099', 'Zildjiana Andini', 'X', 'R1', 1, 'U099', '7358779', 'siswa.png', 'SMKWI', '', '', 0),
(100, 'XITKJ', 'TKJ', '0043778679', '20XITKJ001', 'Aditya Julian', 'XI', 'R1', 1, 'U100', '6814788', 'siswa.png', 'SMKWI', '', '', 0),
(101, 'XITKJ', 'TKJ', '0036033015', '20XITKJ002', 'Ahmad Zaky Sakbana', 'XI', 'R1', 1, 'U101', '5854908', 'siswa.png', 'SMKWI', '', '', 0),
(102, 'XITKJ', 'TKJ', '0034228950', '20XITKJ003', 'Aliycia Ramadhan', 'XI', 'R1', 1, 'U102', '3299975', 'siswa.png', 'SMKWI', '', '', 0),
(103, 'XITKJ', 'TKJ', '0035930193', '20XITKJ004', 'Andhika Karebet Dwi Santoso', 'XI', 'R1', 1, 'U103', '3458409', 'siswa.png', 'SMKWI', '', '', 0),
(104, 'XITKJ', 'TKJ', '0040377867', '20XITKJ005', 'Ardha Firrizqi', 'XI', 'R1', 1, 'U104', '9623125', 'siswa.png', 'SMKWI', '', '', 0),
(105, 'XITKJ', 'TKJ', '0042278042', '20XITKJ006', 'Arrumi Azzahra', 'XI', 'R1', 1, 'U105', '1386545', 'siswa.png', 'SMKWI', '', '', 0),
(106, 'XITKJ', 'TKJ', '0050312311', '20XITKJ007', 'Arya Pangestu Amanullah', 'XI', 'R1', 1, 'U106', '1934979', 'siswa.png', 'SMKWI', '', '', 0),
(107, 'XITKJ', 'TKJ', '0041557158', '20XITKJ008', 'Ascentio Aksyal Paralevi', 'XI', 'R1', 1, 'U107', '4924435', 'siswa.png', 'SMKWI', '', '', 0),
(108, 'XITKJ', 'TKJ', '0042071064', '20XITKJ009', 'Dafa Rizkiansah Atoriq', 'XI', 'R1', 1, 'U108', '6016731', 'siswa.png', 'SMKWI', '', '', 0),
(109, 'XITKJ', 'TKJ', '0043696150', '20XITKJ010', 'Damar Fikrie', 'XI', 'R1', 1, 'U109', '5478898', 'siswa.png', 'SMKWI', '', '', 0),
(110, 'XITKJ', 'TKJ', '0040516455', '20XITKJ011', 'Dennis Ardian', 'XI', 'R1', 1, 'U110', '7755963', 'siswa.png', 'SMKWI', '', '', 0),
(111, 'XITKJ', 'TKJ', '0043696155', '20XITKJ012', 'Dikka Arya Nugraha', 'XI', 'R1', 1, 'U111', '6711373', 'siswa.png', 'SMKWI', '', '', 0),
(112, 'XITKJ', 'TKJ', '0036092832', '20XITKJ013', 'Dimas Prasetio', 'XI', 'R1', 1, 'U112', '2230503', 'siswa.png', 'SMKWI', '', '', 0),
(113, 'XITKJ', 'TKJ', '0026338544', '20XITKJ014', 'Dzahabiyah Salma Nur Ismiliza', 'XI', 'R1', 1, 'U113', '5024023', 'siswa.png', 'SMKWI', '', '', 0),
(114, 'XITKJ', 'TKJ', '0043451939', '20XITKJ015', 'Dzaky Azizan', 'XI', 'R1', 1, 'U114', '2197807', 'siswa.png', 'SMKWI', '', '', 0),
(115, 'XITKJ', 'TKJ', '0020125739', '20XITKJ016', 'Fasugih Fati Rajawani', 'XI', 'R1', 1, 'U115', '1822786', 'siswa.png', 'SMKWI', '', '', 0),
(116, 'XITKJ', 'TKJ', '0036198600', '20XITKJ017', 'Fauzan Harits Patrianesha', 'XI', 'R1', 1, 'U116', '6348608', 'siswa.png', 'SMKWI', '', '', 0),
(117, 'XITKJ', 'TKJ', '0042772020', '20XITKJ018', 'Gabriello Prasetia', 'XI', 'R1', 1, 'U117', '2414481', 'siswa.png', 'SMKWI', '', '', 0),
(118, 'XITKJ', 'TKJ', '0022826750', '20XITKJ019', 'Gifary Ilyasa', 'XI', 'R1', 1, 'U118', '5829285', 'siswa.png', 'SMKWI', '', '', 0),
(119, 'XITKJ', 'TKJ', '0035117780', '20XITKJ020', 'Mohamad Meizy Algifari', 'XI', 'R1', 1, 'U119', '1589936', 'siswa.png', 'SMKWI', '', '', 0),
(120, 'XITKJ', 'TKJ', '0041893735', '20XITKJ021', 'Moh. Syafiqur Rahman', 'XI', 'R1', 1, 'U120', '5914763', 'siswa.png', 'SMKWI', '', '', 0),
(121, 'XITKJ', 'TKJ', '0032048351', '20XITKJ022', 'Muhamad Soleh', 'XI', 'R1', 1, 'U121', '7362929', 'siswa.png', 'SMKWI', '', '', 0),
(122, 'XITKJ', 'TKJ', '0042636121', '20XITKJ023', 'Muhammad Adil Pratama ', 'XI', 'R1', 1, 'U122', '9845272', 'siswa.png', 'SMKWI', '', '', 0),
(123, 'XITKJ', 'TKJ', '0043077187', '20XITKJ024', 'Muhammad Alief Ibrahim', 'XI', 'R1', 1, 'U123', '9615857', 'siswa.png', 'SMKWI', '', '', 0),
(124, 'XITKJ', 'TKJ', '0041235035', '20XITKJ025', 'Muhammad Raffiq Akbar', 'XI', 'R1', 1, 'U124', '8900556', 'siswa.png', 'SMKWI', '', '', 0),
(125, 'XITKJ', 'TKJ', '0042636123', '20XITKJ026', 'Rama Aditya Aprilyano', 'XI', 'R1', 1, 'U125', '3535461', 'siswa.png', 'SMKWI', '', '', 0),
(126, 'XITKJ', 'TKJ', '0042912329', '20XITKJ027', 'Ridho Yudhistira Ridwan', 'XI', 'R1', 1, 'U126', '2282545', 'siswa.png', 'SMKWI', '', '', 0),
(127, 'XITKJ', 'TKJ', '0036457380', '20XITKJ028', 'Rika Amelia', 'XI', 'R1', 1, 'U127', '9197227', 'siswa.png', 'SMKWI', '', '', 0),
(128, 'XITKJ', 'TKJ', '0041270048', '20XITKJ029', 'Rizki Januar Aditya', 'XI', 'R1', 1, 'U128', '6970406', 'siswa.png', 'SMKWI', '', '', 0),
(129, 'XITKJ', 'TKJ', '0042025124', '20XITKJ030', 'Salman Al Farizi', 'XI', 'R1', 1, 'U129', '5285885', 'siswa.png', 'SMKWI', '', '', 0),
(130, 'XITKJ', 'TKJ', '0042636125', '20XITKJ031', 'Satria Muzart Ardana', 'XI', 'R1', 1, 'U130', '4426821', 'siswa.png', 'SMKWI', '', '', 0),
(131, 'XITKJ', 'TKJ', '0042636122', '20XITKJ032', 'Taufan Hermawan', 'XI', 'R1', 1, 'U131', '1210851', 'siswa.png', 'SMKWI', '', '', 0),
(132, 'XIPHA', 'PH', '0035374706', '20XIPHA033', 'Andika', 'XI', 'R1', 1, 'U132', '5283098', 'siswa.png', 'SMKWI', '', '', 0),
(133, 'XIPHA', 'PH', '0041956997', '20XIPHA034', 'Alinza Zalianti Damara', 'XI', 'R1', 1, 'U133', '9955066', 'siswa.png', 'SMKWI', '', '', 0),
(134, 'XIPHA', 'PH', '0037233081', '20XIPHA035', 'Alya Ramdina Syifa', 'XI', 'R1', 1, 'U134', '5281362', 'siswa.png', 'SMKWI', '', '', 0),
(135, 'XIPHA', 'PH', '0045974936', '20XIPHA036', 'Annisa Tri Rizkiah', 'XI', 'R1', 1, 'U135', '8755324', 'siswa.png', 'SMKWI', '', '', 0),
(136, 'XIPHA', 'PH', '0042912352', '20XIPHA037', 'Arya Bima Husada', 'XI', 'R1', 1, 'U136', '3140749', 'siswa.png', 'SMKWI', '', '', 0),
(137, 'XIPHA', 'PH', '0042912349', '20XIPHA038', 'Bunga Anggini', 'XI', 'R1', 1, 'U137', '9644745', 'siswa.png', 'SMKWI', '', '', 0),
(138, 'XIPHA', 'PH', '9019410911', '20XIPHA039', 'Daffa Alif Putra Andika', 'XI', 'R1', 1, 'U138', '8874500', 'siswa.png', 'SMKWI', '', '', 0),
(139, 'XIPHA', 'PH', '0035280379', '20XIPHA040', 'Dinda Syadzwan Friday', 'XI', 'R1', 1, 'U139', '3584661', 'siswa.png', 'SMKWI', '', '', 0),
(140, 'XIPHA', 'PH', '0036660005', '20XIPHA041', 'Dwi Andrianto', 'XI', 'R1', 1, 'U140', '6428998', 'siswa.png', 'SMKWI', '', '', 0),
(141, 'XIPHA', 'PH', '0033147514', '20XIPHA042', 'Dwi Novitasari', 'XI', 'R1', 1, 'U141', '9183496', 'siswa.png', 'SMKWI', '', '', 0),
(142, 'XIPHA', 'PH', '0042246386', '20XIPHA043', 'Fadli Dzil Ikhram', 'XI', 'R1', 1, 'U142', '7276664', 'siswa.png', 'SMKWI', '', '', 0),
(143, 'XIPHA', 'PH', '0041651303', '20XIPHA044', 'Faiskal Khadafi', 'XI', 'R1', 1, 'U143', '1490024', 'siswa.png', 'SMKWI', '', '', 0),
(144, 'XIPHA', 'PH', '0043057910', '20XIPHA045', 'Farhah Sania', 'XI', 'R1', 1, 'U144', '9611578', 'siswa.png', 'SMKWI', '', '', 0),
(145, 'XIPHA', 'PH', '0035413364', '20XIPHA046', 'Heni Widia Ningsih', 'XI', 'R1', 1, 'U145', '8110452', 'siswa.png', 'SMKWI', '', '', 0),
(146, 'XIPHA', 'PH', '0032351613', '20XIPHA047', 'Intan Afriani', 'XI', 'R1', 1, 'U146', '5177550', 'siswa.png', 'SMKWI', '', '', 0),
(147, 'XIPHA', 'PH', '0042453821', '20XIPHA048', 'Melisa Handayani', 'XI', 'R1', 1, 'U147', '4788475', 'siswa.png', 'SMKWI', '', '', 0),
(148, 'XIPHA', 'PH', '0043451769', '20XIPHA049', 'Muhammad Andre Pujakusuma', 'XI', 'R1', 1, 'U148', '1283688', 'siswa.png', 'SMKWI', '', '', 0),
(149, 'XIPHA', 'PH', '0042771595', '20XIPHA050', 'Muhammad Rajab Yusran', 'XI', 'R1', 1, 'U149', '7087281', 'siswa.png', 'SMKWI', '', '', 0),
(150, 'XIPHA', 'PH', '0036972806', '20XIPHA051', 'Mutiara Hariyanti', 'XI', 'R1', 1, 'U150', '6900363', 'siswa.png', 'SMKWI', '', '', 0),
(151, 'XIPHA', 'PH', '0042790280', '20XIPHA052', 'Mutia Aprilia Salsabila', 'XI', 'R1', 1, 'U151', '8435446', 'siswa.png', 'SMKWI', '', '', 0),
(152, 'XIPHA', 'PH', '0037250857', '20XIPHA053', 'Rasendriya Hergiansyah Pangestu', 'XI', 'R1', 1, 'U152', '4569399', 'siswa.png', 'SMKWI', '', '', 0),
(153, 'XIPHA', 'PH', '0041657621', '20XIPHA054', 'Salsabila Azka Iskandar', 'XI', 'R1', 1, 'U153', '5665000', 'siswa.png', 'SMKWI', '', '', 0),
(154, 'XIPHA', 'PH', '9014537351', '20XIPHA055', 'Septian Prasetio', 'XI', 'R1', 1, 'U154', '8749306', 'siswa.png', 'SMKWI', '', '', 0),
(155, 'XIPHA', 'PH', '0034815855', '20XIPHA056', 'Sukma Laraswati', 'XI', 'R1', 1, 'U155', '7743092', 'siswa.png', 'SMKWI', '', '', 0),
(156, 'XIPHA', 'PH', '0036012514', '20XIPHA057', 'Vricil Rizky Septiara', 'XI', 'R1', 1, 'U156', '7676240', 'siswa.png', 'SMKWI', '', '', 0),
(157, 'XIPHB', 'PH', '0043258186', '20XIPHB058', 'Alviona Windasari', 'XI', 'R1', 1, 'U157', '2809564', 'siswa.png', 'SMKWI', '', '', 0),
(158, 'XIPHB', 'PH', '0041481999', '20XIPHB059', 'Ashifa Selma Karamy Yozzeline', 'XI', 'R1', 1, 'U158', '2039148', 'siswa.png', 'SMKWI', '', '', 0),
(159, 'XIPHB', 'PH', '0049383522', '20XIPHB060', 'Aulia Shofarina ', 'XI', 'R1', 1, 'U159', '3064820', 'siswa.png', 'SMKWI', '', '', 0),
(160, 'XIPHB', 'PH', '0023401274', '20XIPHB061', 'Belva Fatma Namira Atallie Wansi', 'XI', 'R1', 1, 'U160', '6825950', 'siswa.png', 'SMKWI', '', '', 0),
(161, 'XIPHB', 'PH', '0041070170', '20XIPHB062', 'Daffa Alfarizqi Sugitama', 'XI', 'R1', 1, 'U161', '3587162', 'siswa.png', 'SMKWI', '', '', 0),
(162, 'XIPHB', 'PH', '0036572184', '20XIPHB063', 'Farel Fachrezi', 'XI', 'R1', 1, 'U162', '6774653', 'siswa.png', 'SMKWI', '', '', 0),
(163, 'XIPHB', 'PH', '0043112483', '20XIPHB064', 'Indri Dewi Saputri', 'XI', 'R1', 1, 'U163', '9664185', 'siswa.png', 'SMKWI', '', '', 0),
(164, 'XIPHB', 'PH', '0044668899', '20XIPHB065', 'Irliansyah Syahputra Pramata', 'XI', 'R1', 1, 'U164', '5482096', 'siswa.png', 'SMKWI', '', '', 0),
(165, 'XIPHB', 'PH', '0033232050', '20XIPHB066', 'Ivana Talitha Candrarini', 'XI', 'R1', 1, 'U165', '2625134', 'siswa.png', 'SMKWI', '', '', 0),
(166, 'XIPHB', 'PH', '0045653510', '20XIPHB067', 'Januar Eka Nugroho', 'XI', 'R1', 1, 'U166', '8677401', 'siswa.png', 'SMKWI', '', '', 0),
(167, 'XIPHB', 'PH', '0042362798', '20XIPHB068', 'Kemala Sari', 'XI', 'R1', 1, 'U167', '8711529', 'siswa.png', 'SMKWI', '', '', 0),
(168, 'XIPHB', 'PH', '0020415254', '20XIPHB069', 'Khalif Rangga Nurrahmat', 'XI', 'R1', 1, 'U168', '8382097', 'siswa.png', 'SMKWI', '', '', 0),
(169, 'XIPHB', 'PH', '0043459705', '20XIPHB070', 'Muhammad Arya Yuda Ramadhan', 'XI', 'R1', 1, 'U169', '4910974', 'siswa.png', 'SMKWI', '', '', 0),
(170, 'XIPHB', 'PH', '0043778670', '20XIPHB071', 'Muhammad Dhanu Satrio', 'XI', 'R1', 1, 'U170', '4206632', 'siswa.png', 'SMKWI', '', '', 0),
(171, 'XIPHB', 'PH', '0046091499', '20XIPHB072', 'Muhammad Fadly Alfarizky', 'XI', 'R1', 1, 'U171', '9375800', 'siswa.png', 'SMKWI', '', '', 0),
(172, 'XIPHB', 'PH', '0042094824', '20XIPHB073', 'Muhammad Fadly Ilham', 'XI', 'R1', 1, 'U172', '2739475', 'siswa.png', 'SMKWI', '', '', 0),
(173, 'XIPHB', 'PH', '0032378580', '20XIPHB074', 'Muhammad Syahrul Ramadhan', 'XI', 'R1', 1, 'U173', '6783452', 'siswa.png', 'SMKWI', '', '', 0),
(174, 'XIPHB', 'PH', '0042636133', '20XIPHB075', 'Nadira Herliani Putri', 'XI', 'R1', 1, 'U174', '6216783', 'siswa.png', 'SMKWI', '', '', 0),
(175, 'XIPHB', 'PH', '0034996387', '20XIPHB076', 'Rafli Septiannur', 'XI', 'R1', 1, 'U175', '3911711', 'siswa.png', 'SMKWI', '', '', 0),
(176, 'XIPHB', 'PH', '0041040053', '20XIPHB077', 'Syifa Amalia', 'XI', 'R1', 1, 'U176', '5517451', 'siswa.png', 'SMKWI', '', '', 0),
(177, 'XIPHB', 'PH', '0049481935', '20XIPHB078', 'Topik Gunawan', 'XI', 'R1', 1, 'U177', '8497289', 'siswa.png', 'SMKWI', '', '', 0),
(178, 'XIPHB', 'PH', '0041259408', '20XIPHB079', 'Winandra Febrizki', 'XI', 'R1', 1, 'U178', '6399091', 'siswa.png', 'SMKWI', '', '', 0),
(179, 'XIPHB', 'PH', '0040432564', '20XIPHB080', 'Yesa Putrilia Inaya', 'XI', 'R1', 1, 'U179', '2714129', 'siswa.png', 'SMKWI', '', '', 0),
(180, 'XIPHB', 'PH', '0025877807', '20XIPHB081', 'Zahra Awlia', 'XI', 'R1', 1, 'U180', '5416762', 'siswa.png', 'SMKWI', '', '', 0),
(181, 'XIBGA', 'BOGA', '0044688369', '20XIBGA082', 'Afifah Azzahra', 'XI', 'R1', 1, 'U181', '7183594', 'siswa.png', 'SMKWI', '', '', 0),
(182, 'XIBGA', 'BOGA', '0026933822', '20XIBGA083', 'Alivia Destiana', 'XI', 'R1', 1, 'U182', '4992723', 'siswa.png', 'SMKWI', '', '', 0),
(183, 'XIBGA', 'BOGA', '0442075188', '20XIBGA084', 'Alya Syaqina Zahra', 'XI', 'R1', 1, 'U183', '5261460', 'siswa.png', 'SMKWI', '', '', 0),
(184, 'XIBGA', 'BOGA', '0040917111', '20XIBGA085', 'Bastian Pamungkas Kaian', 'XI', 'R1', 1, 'U184', '6746655', 'siswa.png', 'SMKWI', '', '', 0),
(185, 'XIBGA', 'BOGA', '0043695128', '20XIBGA086', 'Devina Adillah Fahrezi', 'XI', 'R1', 1, 'U185', '4557460', 'siswa.png', 'SMKWI', '', '', 0),
(186, 'XIBGA', 'BOGA', '0043451926', '20XIBGA087', 'Dika Aditya Mahardhika', 'XI', 'R1', 1, 'U186', '2643764', 'siswa.png', 'SMKWI', '', '', 0),
(187, 'XIBGA', 'BOGA', '0047141502', '20XIBGA088', 'Fiki Firmansyah', 'XI', 'R1', 1, 'U187', '7844335', 'siswa.png', 'SMKWI', '', '', 0),
(188, 'XIBGA', 'BOGA', '0049954242', '20XIBGA089', 'Marcela Dwi Amanda', 'XI', 'R1', 1, 'U188', '3037961', 'siswa.png', 'SMKWI', '', '', 0),
(189, 'XIBGA', 'BOGA', '0047632503', '20XIBGA090', 'Muhammad Adriansyah', 'XI', 'R1', 1, 'U189', '8460188', 'siswa.png', 'SMKWI', '', '', 0),
(190, 'XIBGA', 'BOGA', '0045184536', '20XIBGA091', 'Muhammad Yogi Afandi ', 'XI', 'R1', 1, 'U190', '1549240', 'siswa.png', 'SMKWI', '', '', 0),
(191, 'XIBGA', 'BOGA', '0038046438', '20XIBGA092', 'Muhammad Zikri Ramadhan', 'XI', 'R1', 1, 'U191', '1702138', 'siswa.png', 'SMKWI', '', '', 0),
(192, 'XIBGA', 'BOGA', '0043930104', '20XIBGA093', 'Nadya Putri Hanifah', 'XI', 'R1', 1, 'U192', '4360798', 'siswa.png', 'SMKWI', '', '', 0),
(193, 'XIBGA', 'BOGA', '0046100246', '20XIBGA094', 'Naurah Amalia Salsabila', 'XI', 'R1', 1, 'U193', '2847731', 'siswa.png', 'SMKWI', '', '', 0),
(194, 'XIBGA', 'BOGA', '0035090203', '20XIBGA095', 'Nindya Naidu', 'XI', 'R1', 1, 'U194', '1379078', 'siswa.png', 'SMKWI', '', '', 0),
(195, 'XIBGA', 'BOGA', '0044466515', '20XIBGA096', 'Nurhikma Agustin', 'XI', 'R1', 1, 'U195', '7995433', 'siswa.png', 'SMKWI', '', '', 0),
(196, 'XIBGA', 'BOGA', '0040817421', '20XIBGA097', 'Ranaa Azzahra', 'XI', 'R1', 1, 'U196', '9346800', 'siswa.png', 'SMKWI', '', '', 0),
(197, 'XIBGA', 'BOGA', '0040518935', '20XIBGA098', 'Ridzi Arya Satya', 'XI', 'R1', 1, 'U197', '5182952', 'siswa.png', 'SMKWI', '', '', 0),
(198, 'XIBGA', 'BOGA', '0039709194', '20XIBGA099', 'Rifki Shaquille Muhtarom Azhar', 'XI', 'R1', 1, 'U198', '4091862', 'siswa.png', 'SMKWI', '', '', 0),
(199, 'XIBGA', 'BOGA', '0042070075', '20XIBGA100', 'Rizki Ilham Maulana', 'XI', 'R1', 1, 'U199', '7801652', 'siswa.png', 'SMKWI', '', '', 0),
(200, 'XIBGA', 'BOGA', '0047608790', '20XIBGA101', 'Triyana Dewi ', 'XI', 'R1', 1, 'U200', '4111377', 'siswa.png', 'SMKWI', '', '', 0),
(201, 'XIBGA', 'BOGA', '9013849954', '20XIBGA102', 'Vidia Alfida Salsabila', 'XI', 'R1', 1, 'U201', '3858180', 'siswa.png', 'SMKWI', '', '', 0),
(202, 'XIBGA', 'BOGA', '0036370265', '20XIBGA103', 'Winda Septiani Sanusi', 'XI', 'R1', 1, 'U202', '8362574', 'siswa.png', 'SMKWI', '', '', 0),
(203, 'XIBGA', 'BOGA', '0043830937', '20XIBGA104', 'Zacky Gempar Perdana', 'XI', 'R1', 1, 'U203', '1791130', 'siswa.png', 'SMKWI', '', '', 0),
(204, 'XIBGA', 'BOGA', '0042059894', '20XIBGA105', 'Zuriel Jasson Indrastara', 'XI', 'R1', 1, 'U204', '9330003', 'siswa.png', 'SMKWI', '', '', 0),
(205, 'XIBGB', 'BOGA', '0031802690', '20XIBGB106', 'Achmad Faisal Akbar', 'XI', 'R1', 1, 'U205', '6569044', 'siswa.png', 'SMKWI', '', '', 0),
(206, 'XIBGB', 'BOGA', '0032638070', '20XIBGB107', 'Adellia Nafilah Nugraha', 'XI', 'R1', 1, 'U206', '8466055', 'siswa.png', 'SMKWI', '', '', 0),
(207, 'XIBGB', 'BOGA', '0043599303', '20XIBGB108', 'Adias Ricanda', 'XI', 'R1', 1, 'U207', '8357222', 'siswa.png', 'SMKWI', '', '', 0),
(208, 'XIBGB', 'BOGA', '0038091503', '20XIBGB109', 'Aisyah Gusti Riyani', 'XI', 'R1', 1, 'U208', '3540960', 'siswa.png', 'SMKWI', '', '', 0),
(209, 'XIBGB', 'BOGA', '0041790735', '20XIBGB110', 'Alvira Nuraini Sasongko ', 'XI', 'R1', 1, 'U209', '3634981', 'siswa.png', 'SMKWI', '', '', 0),
(210, 'XIBGB', 'BOGA', '0016940073', '20XIBGB111', 'Amanda Moerdiani', 'XI', 'R1', 1, 'U210', '4730554', 'siswa.png', 'SMKWI', '', '', 0),
(211, 'XIBGB', 'BOGA', '0042130529', '20XIBGB112', 'Aprillia Indah Astuti', 'XI', 'R1', 1, 'U211', '8876099', 'siswa.png', 'SMKWI', '', '', 0),
(212, 'XIBGB', 'BOGA', '0035270722', '20XIBGB113', 'Arini Saskia Ramadhanti', 'XI', 'R1', 1, 'U212', '8345933', 'siswa.png', 'SMKWI', '', '', 0),
(213, 'XIBGB', 'BOGA', '0042559006', '20XIBGB114', 'Bulan Sekar Wangi', 'XI', 'R1', 1, 'U213', '8368286', 'siswa.png', 'SMKWI', '', '', 0),
(214, 'XIBGB', 'BOGA', '0017995307', '20XIBGB115', 'Christian Andrew Satrio Wowor', 'XI', 'R1', 1, 'U214', '8617482', 'siswa.png', 'SMKWI', '', '', 0),
(215, 'XIBGB', 'BOGA', '0035633217', '20XIBGB116', 'Ernyssa Putri Lesmana', 'XI', 'R1', 1, 'U215', '3911684', 'siswa.png', 'SMKWI', '', '', 0),
(216, 'XIBGB', 'BOGA', '0043233677', '20XIBGB117', 'Fahriza Laita Taslim', 'XI', 'R1', 1, 'U216', '8255556', 'siswa.png', 'SMKWI', '', '', 0),
(217, 'XIBGB', 'BOGA', '0043848343', '20XIBGB118', 'Gyas Thalita Alea', 'XI', 'R1', 1, 'U217', '1226863', 'siswa.png', 'SMKWI', '', '', 0),
(218, 'XIBGB', 'BOGA', '0040918293', '20XIBGB119', 'Intan Nur Azzahra', 'XI', 'R1', 1, 'U218', '6883376', 'siswa.png', 'SMKWI', '', '', 0),
(219, 'XIBGB', 'BOGA', '0028289239', '20XIBGB120', 'Muhammad Daffa Chalik', 'XI', 'R1', 1, 'U219', '6857566', 'siswa.png', 'SMKWI', '', '', 0),
(220, 'XIBGB', 'BOGA', '0021800831', '20XIBGB121', 'Nabila Istiqfany', 'XI', 'R1', 1, 'U220', '1208785', 'siswa.png', 'SMKWI', '', '', 0),
(221, 'XIBGB', 'BOGA', '0042959404', '20XIBGB122', 'Naila Sofia Ramadhani', 'XI', 'R1', 1, 'U221', '2501473', 'siswa.png', 'SMKWI', '', '', 0),
(222, 'XIBGB', 'BOGA', '0036059623', '20XIBGB123', 'Naomi Yuki Kahala', 'XI', 'R1', 1, 'U222', '7318042', 'siswa.png', 'SMKWI', '', '', 0),
(223, 'XIBGB', 'BOGA', '0035695050', '20XIBGB124', 'Redondo Putra Mossad Nirahua', 'XI', 'R1', 1, 'U223', '6694204', 'siswa.png', 'SMKWI', '', '', 0),
(224, 'XIBGB', 'BOGA', '0036834455', '20XIBGB125', 'Rufus Yefta Simeon', 'XI', 'R1', 1, 'U224', '9620141', 'siswa.png', 'SMKWI', '', '', 0),
(225, 'XIBGB', 'BOGA', '0047541457', '20XIBGB126', 'Sabrina Nurul Maulidya', 'XI', 'R1', 1, 'U225', '9374926', 'siswa.png', 'SMKWI', '', '', 0),
(226, 'XIBGB', 'BOGA', '0032044488', '20XIBGB127', 'Shaskia Kharinina Kana Gusti', 'XI', 'R1', 1, 'U226', '7157637', 'siswa.png', 'SMKWI', '', '', 0),
(227, 'XIBGB', 'BOGA', '0035713173', '20XIBGB128', 'Shintya Puspita Sari', 'XI', 'R1', 1, 'U227', '9515579', 'siswa.png', 'SMKWI', '', '', 0),
(228, 'XIBGB', 'BOGA', '0027312146', '20XIBGB129', 'Sutra Satria Praja', 'XI', 'R1', 1, 'U228', '2111401', 'siswa.png', 'SMKWI', '', '', 0),
(229, 'XIBGB', 'BOGA', '0029386275', '20XIBGB130', 'Syahril Ramadhan', 'XI', 'R1', 1, 'U229', '7169547', 'siswa.png', 'SMKWI', '', '', 0),
(230, 'XIBGB', 'BOGA', '0035930199', '20XIBGB131', 'Zhara kaylannisa', 'XI', 'R1', 1, 'U230', '7678156', 'siswa.png', 'SMKWI', '', '', 0),
(231, 'XIBGC', 'BOGA', '0040619280', '20XIBGB132', 'Adinda Putri Heriani', 'XI', 'R1', 1, 'U231', '4537130', 'siswa.png', 'SMKWI', '', '', 0),
(232, 'XIBGC', 'BOGA', '0042772024', '20XIBGC133', 'Anggita Riyanih', 'XI', 'R1', 1, 'U232', '7881447', 'siswa.png', 'SMKWI', '', '', 0),
(233, 'XIBGC', 'BOGA', '0043452163', '20XIBGC134', 'Anindya Winy', 'XI', 'R1', 1, 'U233', '8932264', 'siswa.png', 'SMKWI', '', '', 0),
(234, 'XIBGC', 'BOGA', '0026933837', '20XIBGC135', 'Arfian Ramadhan Dewanto', 'XI', 'R1', 1, 'U234', '2563125', 'siswa.png', 'SMKWI', '', '', 0),
(235, 'XIBGC', 'BOGA', '0037250830', '20XIBGC136', 'Audytia Putri Syahrani', 'XI', 'R1', 1, 'U235', '5805162', 'siswa.png', 'SMKWI', '', '', 0),
(236, 'XIBGC', 'BOGA', '0037825098', '20XIBGC137', 'Bagaskara Rajendra', 'XI', 'R1', 1, 'U236', '5972340', 'siswa.png', 'SMKWI', '', '', 0),
(237, 'XIBGC', 'BOGA', '0036975406', '20XIBGC138', 'Buffon Oktavian Ramadhani', 'XI', 'R1', 1, 'U237', '1897524', 'siswa.png', 'SMKWI', '', '', 0),
(238, 'XIBGC', 'BOGA', '0037499335', '20XIBGC139', 'Faiz Wafa Naufal', 'XI', 'R1', 1, 'U238', '9424360', 'siswa.png', 'SMKWI', '', '', 0),
(239, 'XIBGC', 'BOGA', '0042912628', '20XIBGC140', 'Fara Dila Nanda Sandi', 'XI', 'R1', 1, 'U239', '8238333', 'siswa.png', 'SMKWI', '', '', 0),
(240, 'XIBGC', 'BOGA', '0037076551', '20XIBGC141', 'Farlya', 'XI', 'R1', 1, 'U240', '6192194', 'siswa.png', 'SMKWI', '', '', 0),
(241, 'XIBGC', 'BOGA', '0035237365', '20XIBGC142', 'Marisa Fitria Ramadanti', 'XI', 'R1', 1, 'U241', '9806161', 'siswa.png', 'SMKWI', '', '', 0),
(242, 'XIBGC', 'BOGA', '0032712706', '20XIBGC143', 'Marulitua Manurung', 'XI', 'R1', 1, 'U242', '2538175', 'siswa.png', 'SMKWI', '', '', 0),
(243, 'XIBGC', 'BOGA', '0044232238', '20XIBGC144', 'Mira Mariyam', 'XI', 'R1', 1, 'U243', '6840654', 'siswa.png', 'SMKWI', '', '', 0),
(244, 'XIBGC', 'BOGA', '0037132168', '20XIBGC145', 'Muhamad Nur Fauzan Sulistiyo', 'XI', 'R1', 1, 'U244', '6961233', 'siswa.png', 'SMKWI', '', '', 0),
(245, 'XIBGC', 'BOGA', '0043740564', '20XIBGC146', 'Muhammad Zharfan  Aftara', 'XI', 'R1', 1, 'U245', '4997187', 'siswa.png', 'SMKWI', '', '', 0),
(246, 'XIBGC', 'BOGA', '0042765156', '20XIBGC147', 'Nadya Putri Marshanda', 'XI', 'R1', 1, 'U246', '9144373', 'siswa.png', 'SMKWI', '', '', 0),
(247, 'XIBGC', 'BOGA', '0042636140', '20XIBGC148', 'Najwa Alifa Salsabila', 'XI', 'R1', 1, 'U247', '2946621', 'siswa.png', 'SMKWI', '', '', 0),
(248, 'XIBGC', 'BOGA', '0040875034', '20XIBGC149', 'Naura Juanita Thamrin', 'XI', 'R1', 1, 'U248', '6581266', 'siswa.png', 'SMKWI', '', '', 0),
(249, 'XIBGC', 'BOGA', '0036198582', '20XIBGC150', 'Putri Syabila Amnifa', 'XI', 'R1', 1, 'U249', '6088015', 'siswa.png', 'SMKWI', '', '', 0),
(250, 'XIBGC', 'BOGA', '0040390482', '20XIBGC151', 'Raditya Erickadewa', 'XI', 'R1', 1, 'U250', '8926309', 'siswa.png', 'SMKWI', '', '', 0),
(251, 'XIBGC', 'BOGA', '0037117916', '20XIBGC152', 'Rafa Mardhiah', 'XI', 'R1', 1, 'U251', '3268106', 'siswa.png', 'SMKWI', '', '', 0),
(252, 'XIBGC', 'BOGA', '0050297302', '20XIBGC153', 'Regina Raihana Salsabila', 'XI', 'R1', 1, 'U252', '3664024', 'siswa.png', 'SMKWI', '', '', 0),
(253, 'XIBGC', 'BOGA', '0047625621', '20XIBGC154', 'Shenia Agustin Nanda Widadari', 'XI', 'R1', 1, 'U253', '2086814', 'siswa.png', 'SMKWI', '', '', 0),
(254, 'XIBGC', 'BOGA', '0036013012', '20XIBGC155', 'Shufaira Zahra Zettira Bilqais', 'XI', 'R1', 1, 'U254', '7902502', 'siswa.png', 'SMKWI', '', '', 0),
(255, 'XIBGC', 'BOGA', '0029150035', '20XIBGC156', 'Siti Nuraisyah ', 'XI', 'R1', 1, 'U255', '8827566', 'siswa.png', 'SMKWI', '', '', 0),
(256, 'XIBGC', 'BOGA', '0034066630', '20XIBGC157', 'Syahfira Zahra Aulia', 'XI', 'R1', 1, 'U256', '1014687', 'siswa.png', 'SMKWI', '', '', 0),
(257, 'XIBGC', 'BOGA', '0047944896', '20XIBGC158', 'Zahra Fitriyani', 'XI', 'R1', 1, 'U257', '9904512', 'siswa.png', 'SMKWI', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nomor` int(5) NOT NULL,
  `soal` longtext CHARACTER SET utf8 NOT NULL,
  `jenis` int(1) NOT NULL,
  `pilA` longtext CHARACTER SET utf8 NOT NULL,
  `pilB` longtext CHARACTER SET utf8 NOT NULL,
  `pilC` longtext CHARACTER SET utf8 NOT NULL,
  `pilD` longtext CHARACTER SET utf8 NOT NULL,
  `pilE` longtext CHARACTER SET utf8 NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `file` text DEFAULT NULL,
  `file1` text DEFAULT NULL,
  `fileA` text DEFAULT NULL,
  `fileB` text DEFAULT NULL,
  `fileC` text DEFAULT NULL,
  `fileD` text DEFAULT NULL,
  `fileE` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id_soal`, `id_mapel`, `nomor`, `soal`, `jenis`, `pilA`, `pilB`, `pilC`, `pilD`, `pilE`, `jawaban`, `file`, `file1`, `fileA`, `fileB`, `fileC`, `fileD`, `fileE`) VALUES
(1, 1, 1, '     ', 1, '9', '12', '18', '24', '36', 'C', '', 'UJNO1.JPG', '', '', '', '', ''),
(2, 1, 2, 'Hasil dari 6√2 +4√8 - 3√32 adalah ...', 1, '2√2', '3√2', '5√2', '7√2', '9√2', 'A', '', '', '', '', '', '', ''),
(3, 1, 3, 'Jika x1 dan x2 adalah akar dari persamaan x2 – 6x + 5 = 0, maka nilai x1 + x2 =', 1, '6', '4', '-4', '-6', '10', 'D', '', '', '', '', '', '', ''),
(4, 1, 4, 'Fungsi-fungsi ini, yang merupakan fungsi kuadrat.', 1, '(i) dan (iii)', '(ii) dan (iv)', '(ii) dan (iii)', '(i) dan (iv)', '(iii) dan (iv)', 'A', '', 'UJNO4.JPG', '', '', '', '', ''),
(5, 1, 5, 'Sumbu simetri grafik fungsi y = x2 – 6x + 8 adalah.', 1, 'x = 4', 'x = 3', 'x = 2', 'x = 1', 'x = 5', 'C', '', '', '', '', '', '', ''),
(6, 1, 6, 'Pada layar televisi, panjang sebuah mobil adalah 14 cm dan tingginya 4 cm. Jika tinggi sebenarnya adalah 1 m, maka panjang mobil sebenarnya adalah.…', 1, '3 m', '3,5 m', '4 m', '4,5 m', '5 m', 'B', '', '', '', '', '', '', ''),
(7, 1, 7, '   ', 1, 'orientasi', 'Komplikasi', 'Konflik', 'Pembubaran', 'Permusuhan', 'A', '', 'UJNO7.JPG', '', '', '', '', ''),
(8, 1, 8, '<p>Bukti bahwa kutipan cerita pendek ditetapkan pada malam hari adalah &hellip;</p>', 1, '<p>saat matahari gelap dan bersinar</p>', '<p>matahari disebelah barat rumah</p>', '<p>seperti yang kamu katakan kemarin</p>', '<p>Kisah bohong pertama</p>', '<p>Kerumunan tidak lagi bersama anda</p>', 'A', '', '1_8_2.JPG', '', '', '', '', ''),
(9, 1, 9, 'Manajer pariwisata Taman Seroja memperkirakan bahwa sekitar 27.000 pelancong akan datang ke lokasi ini pada akhir liburan semester tahun ini. Prediksi ini tentu tidak berlebihan karena mencerminkan jumlah pelancong yang bepergian selama musim liburan tahun lalu. Untuk meningkatkan minat dan daya tarik para pelancong, administrator akan menyiapkan berbagai barang hiburan baru seperti wahana air, pertunjukan musik, sulap, dan banyak lagi.<br>Gagasan utama dalam paragraf di atas adalah …', 1, 'Ada wahana air tambahan di taman objek wisata', 'Memprediksi peningkatan jumlah wisatawan di taman tahun ini', 'Taman Seroja adalah tempat wisata terbaik di Indonesia', 'Beberapa taman baru taman ini akan menarik minat para pelancong', 'Pertunjukan sulap adalah hal yang paling menarik', 'B', '', '', '', '', '', '', ''),
(10, 1, 10, 'Manajer pariwisata Taman Seroja memperkirakan bahwa sekitar 27.000 pelancong akan datang ke lokasi ini pada akhir liburan semester tahun ini. Prediksi ini tentu tidak berlebihan karena mencerminkan jumlah pelancong yang bepergian selama musim liburan tahun lalu. Untuk meningkatkan minat dan daya tarik para pelancong, administrator akan menyiapkan berbagai barang hiburan baru seperti wahana air, pertunjukan musik, sulap, dan banyak lagi.<br>Serangkaian pertanyaan berikut, jawaban yang ada dalam paragraf di atas, adalah …', 1, 'Dari mana datangnya wisatawan yang mengunjungi taman?', 'Berapa banyak wisatawan yang diprediksi oleh Manajer Taman Lotus Park?', 'Hiburan macam apa yang paling populer di antara berbagai wahana bagi wisatawan di taman?', 'Siapakah tokoh nasional yang hadir pada pembukaan Wisata Taman Lotus?', 'Apakah prediksi yang dibuat oleh manajemen taman dianggap terlalu berlebihan?', 'B', '', '', '', '', '', '', ''),
(11, 1, 11, 'Berita kematian Sofia, seorang gadis berusia 7 tahun, tidak diragukan lagi membuat marah orang Indonesia. Gadis kecil yang imut itu meninggal di tangan ibu angkatnya dan dimakamkan di halaman belakang rumah, tepat di bawah kandang kelinci peliharaan keluarga. Sofia hanyalah anak angkat keluarga Stephen. Dia sering diperlakukan seperti budak di rumah.<br>Pesan berisi tentang …', 1, 'Kasus pelecehan anak biologis oleh orang tua', 'kekerasan orangtua oleh anak kandung', 'pembunuhan seorang anak oleh ibu angkatnya', 'Pengobatan seperti budak ibu angkat', 'peristiwa kematian seorang anak', 'C', '', '', '', '', '', '', ''),
(12, 1, 12, 'Berita kematian Sofia, seorang gadis berusia 7 tahun, tidak diragukan lagi membuat marah orang Indonesia. Gadis kecil yang imut itu meninggal di tangan ibu angkatnya dan dimakamkan di halaman belakang rumah, tepat di bawah kandang kelinci peliharaan keluarga. Sofia hanyalah anak angkat keluarga Stephen. Dia sering diperlakukan seperti budak di rumah.<br>Urutan pengiriman informasi di bagian pesan di atas adalah …', 1, 'Apa, mengapa, bagaimana?', 'Siapa, mengapa, apa?', 'Apa, apa, apa', 'Kenapa, siapa, apa?', 'Siapa, mengapa, apa?', 'A', '', '', '', '', '', '', ''),
(13, 1, 13, '<p>Berikut ini adalah kalimat yang tepat untuk mengisi celah dalam memo di atas:</p>', 1, '<p>Untuk Pak Sungkono, kami membutuhkan beberapa peralatan untuk mendukung administrasi bisnis. Jadi dengan ini &hellip;</p>', '<p>Sungkono, bahwa &hellip; kelengkapan kita tidak lengkap. Bagaimana Anda ingin bekerja?</p>', '<p>Yth. Pak Sungkono, silakan periksa ke ruang administrasi sekarang!</p>', '<p>YTH. Pak Sungkono, staf administrasi sangat membutuhkan peralatan tambahan. Jadi dengan ini &hellip;</p>', '<p>Tidak masalah, kami membutuhkan alat tambahan ini. Bagaimana Sungkono?</p>', 'A', '1_13_1.JPG', '', '', '', '', '', ''),
(14, 1, 14, 'Kabel UTP, yang biasanya digunakan dalam LAN dengan koneksi.', 1, 'RJ 11', 'RJ 45', 'RG 6', 'RG 58', 'RG 5', 'B', '', '', '', '', '', '', ''),
(15, 1, 15, 'Bandung is located is ...', 1, 'West Java', 'East Java', 'Central Java', 'Banten', 'Jakarta', 'A', '', '', '', '', '', '', ''),
(16, 1, 16, 'We are not allowed to turn ...', 1, 'left', 'right', 'forward', 'around', 'center', 'B', '', 'UJNO16.jpg', '', '', '', '', ''),
(17, 1, 17, 'Devi is clever. Ayu is clever. Devi is ... Ayu', 1, 'as clever as', 'as clever', 'more clever', 'cleverest', 'clever', 'A', '', '', '', '', '', '', ''),
(18, 1, 18, 'Dodi is twelve years old. Andi is ten years old. Andi is ... than Dodi.', 1, 'old', 'young', 'older', 'younger', 'man', 'D', '', '', '', '', '', '', ''),
(19, 1, 19, '....... the baby is sleeping !', 1, 'Don’t make noise', 'Be happy', 'Be careful', 'Be diligent', 'Be strong', 'A', '', '', '', '', '', '', ''),
(20, 1, 20, 'You have a motorcycle. The motorcycle is ...', 1, 'you', 'your', 'yours', 'you are', 'they are', 'C', '', '', '', '', '', '', ''),
(21, 1, 21, 'Where are they studying ? They are in the...', 1, 'classroom', 'canteen', 'library', 'office', 'cafetaria', 'A', '', '', '', '', '', '', ''),
(22, 1, 22, 'He is ... in Kelud mountain.', 1, 'fishing', 'climbing', 'cleaning', 'traveling', 'walking', 'B', '', '', '', '', '', '', ''),
(23, 1, 23, 'Sampah hijau dapat menghasilkan sampah sebagai akibat ….', 1, 'launiu', 'plastik', 'Kristal', 'kardus', 'Perusakan Alam', 'A', '', '', '', '', '', '', ''),
(24, 1, 24, 'Demam berdarah disebabkan oleh ….', 1, 'tikus', 'lago', 'Nyamuk Aedes aegypti', 'nyamuk anopelesi', 'Marmut', 'C', '', '', '', '', '', '', ''),
(25, 1, 25, 'Peralatan berkemah, antara lain …', 1, 'Cangkul', 'tenda, pasak dan tali', 'Shotgun', 'linggis', 'Laptop', 'B', '', '', '', '', '', '', ''),
(26, 1, 26, 'Sebutkan negara - negara di asia tenggara …', 2, '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, 1, 27, 'Buatlah cerpen tentang diri anda sendiri…', 2, '', '', '', '', '', '', '', '', '', '', '', '', ''),
(28, 1, 28, 'Tuliskan yang kalian ketahui mengenai steve jobs …', 2, '', '', '', '', '', '', '', '', '', '', '', '', ''),
(29, 1, 29, 'Tuliskan 5 film motivasi yang kalian ketahui…', 2, '', '', '', '', '', '', '', '', '', '', '', '', ''),
(30, 1, 30, 'Tuliskan negara - negara yang pernah menjajah indonesia…', 2, '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id_token` int(11) NOT NULL,
  `token` varchar(6) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `masa_berlaku` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `id_ujian` int(5) NOT NULL,
  `id_pk` varchar(10) NOT NULL,
  `id_guru` int(5) NOT NULL,
  `id_mapel` int(5) NOT NULL,
  `kode_ujian` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `lama_ujian` int(5) NOT NULL,
  `tgl_ujian` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `waktu_ujian` time NOT NULL,
  `selesai_ujian` time NOT NULL,
  `level` varchar(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `siswa` varchar(255) NOT NULL,
  `sesi` varchar(1) NOT NULL,
  `acak` int(1) NOT NULL,
  `acak_opsi` int(1) NOT NULL,
  `token` int(1) NOT NULL,
  `status` int(3) NOT NULL,
  `hasil` int(2) NOT NULL,
  `kkm` varchar(50) NOT NULL,
  `ulang` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `file_pendukung`
--
ALTER TABLE `file_pendukung`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `hasil_jawaban`
--
ALTER TABLE `hasil_jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `hasil_nilai`
--
ALTER TABLE `hasil_nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`kode_level`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`,`idpk`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`mapel_id`,`kode_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `pengacak`
--
ALTER TABLE `pengacak`
  ADD PRIMARY KEY (`id_pengacak`);

--
-- Indexes for table `pengacakopsi`
--
ALTER TABLE `pengacakopsi`
  ADD PRIMARY KEY (`id_pengacak`);

--
-- Indexes for table `pengawas`
--
ALTER TABLE `pengawas`
  ADD PRIMARY KEY (`id_pengawas`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `pk`
--
ALTER TABLE `pk`
  ADD PRIMARY KEY (`id_pk`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`kode_ruang`);

--
-- Indexes for table `savsoft_options`
--
ALTER TABLE `savsoft_options`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `savsoft_qbank`
--
ALTER TABLE `savsoft_qbank`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `sesi`
--
ALTER TABLE `sesi`
  ADD PRIMARY KEY (`kode_sesi`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `sinkron`
--
ALTER TABLE `sinkron`
  ADD PRIMARY KEY (`nama_data`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`id_ujian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumentasi`
--
ALTER TABLE `dokumentasi`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `file_pendukung`
--
ALTER TABLE `file_pendukung`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hasil_jawaban`
--
ALTER TABLE `hasil_jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hasil_nilai`
--
ALTER TABLE `hasil_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `mapel_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengacak`
--
ALTER TABLE `pengacak`
  MODIFY `id_pengacak` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengacakopsi`
--
ALTER TABLE `pengacakopsi`
  MODIFY `id_pengacak` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengawas`
--
ALTER TABLE `pengawas`
  MODIFY `id_pengawas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `savsoft_options`
--
ALTER TABLE `savsoft_options`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `savsoft_qbank`
--
ALTER TABLE `savsoft_qbank`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `id_ujian` int(5) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
